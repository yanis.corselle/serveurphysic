<?php
session_start();
if(isset($_SESSION['attribut']) == FALSE OR $_SESSION['attribut'] == 0)
{
  header("Location:../../profil.php");
}

$host = 'localhost';
$dbname = 'physic';
$username = 'admin';
$password = 'root';
  
$dsn = "mysql:host=$host;dbname=$dbname"; 
// récupérer toutes les interventions
$sql = "SELECT * FROM intervention";
 
try{
 $pdo = new PDO($dsn, $username, $password);
 $stmt = $pdo->query($sql);
 
 if($stmt === false){
  die("Erreur");
 }
 
}catch (PDOException $e){
  echo $e->getMessage();
}
//include("RequeteAJAX.php");
?>

<html lang="fr">
<html>
<head>
	<meta charset="utf-8"/>
	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>
	<link rel="icon" type="image/png" href="favicon.png" />
	<link type="text/css" rel="stylesheet" href="../../../css/style.css" />
	<link type="image/jpg" rel="icon" href="../../../img/favicon.jpg"/>
	<link rel="stylesheet" href="../../../css/jquery.mCustomScrollbar.css" />
	<script type="text/javascript" src="../../../js/jquery.js"></script>
</head>
	<body>
	<!--Entete-->
		<?php include("../../../include/page_upacc.php"); ?>

	<!--Image logo-->

	<!--Zone du tableau-->
	<div class="gest_user">
		<form action="" name="gest_acc" method="post" enctype="multipart/form-data" charset="utf-8" accept-charset="utf-8">
		<center>
	        <?php
	             try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
	        ?>
	        <table class="tableacc" name="" border="1px">
	            <tr id="tablecata">
                    <th id="id">ID</th>
                    <th id="nom_societe">Nom société</th>
                    <th id="nom_contact">Nom du contact</th>
                    <th id="adresse">Adresse</th>
                    <th id="tel"> Téléphone</th>
                    <th id="fax">Fax</th>
                    <th id="portable">Portable</th>
                    <th id="date_intervention">Date de l'intervention</th>
                    <th id="Modifier" name="Modifier">Modifier</th>
                    <th id="suppr" name="suppr">Supprimer</th>
	            </tr>
   <tbody>
     <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)) : ?>
                <tr>
                    <td id="id"><?php echo htmlspecialchars($row['id']); ?></td>
                    <td id="nom_societe"><?php echo htmlspecialchars($row['nom_societe']); ?></td>
                    <td id="nom_contact"><?php echo htmlspecialchars($row['nom_contact']); ?></td>
                    <td id="adresse"><?php echo htmlspecialchars($row['adresse']); ?></td>
                    <td id="tel"><?php echo htmlspecialchars($row['tel']); ?></td>
                    <td id="fax"><?php echo htmlspecialchars($row['fax']); ?></td>
                    <td id="portable"><?php echo htmlspecialchars($row['portable']); ?></td>
                    <td id="date_intervention"><?php echo htmlspecialchars($row['date_intervention']); ?></td>
                    <td id="Modifier"><form action='editer_user.php' method='POST'><input type="hidden" type='submit' name="edit" value="<?php echo $row['id']; ?>"><input type='submit' name="edit" value="<?php echo Modifier; ?>"></form>
                    <td id="suppr"><form action='suppr_user.php' method='POST'><input type="hidden" type='submit' name="sup" value="<?php echo $row['id']; ?>"><input type='submit' name="sup2" value="<?php echo X; ?>"></form>
                </tr>
     <?php endwhile; ?>
   </tbody>
 </table>

		<!--Zone du footer-->
		<?php include("../../../include/footerupacc.php"); ?>
		<!--<script type="text/javascript" src="../../js/Remise.js" charset="utf-8"></script>-->
		<script language="javascript">
      function confirme( identifiant )
      {
        var confirmation = confirm( "Voulez vous vraiment supprimer cet utilisateur ?" ) ;
	if( confirmation )
	{
	  document.location.href = "suppr_user.php?idligne="+identifiant ;
	}
      }
    </script>
</body>
</html>
