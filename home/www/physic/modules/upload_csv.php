<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="fr-FR">
<head>
	<meta http-equiv="Content-Type" content="text/html" charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>
	<link rel="icon" type="image/png" href="favicon.png"/>
	<link type="text/css" rel="stylesheet" href="../css/style.css"/>
	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>
	<link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css"/>
</head>
	<body>
	<!--Entete-->
        <?php include("../include/page_up.php"); 
        
// Vérifier si le formulaire a été soumis
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Vérifie si le fichier a été uploadé sans erreur.
    if(isset($_FILES["csv"]) && $_FILES["csv"]["error"] == 0){
        $allowed = array( "csv" => "file/csv");
        $filename = $_FILES["csv"]["name"];
        $filetype = $_FILES["csv"]["type"];
        $filesize = $_FILES["csv"]["size"];

        // Vérifie l'extension du fichier
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die("Erreur : Veuillez sélectionner un format de fichier valide.");

        // Vérifie la taille du fichier - 5Mo maximum
        $maxsize = 5 * 1024 * 1024;
        if($filesize > $maxsize) die("Error: La taille du fichier est supérieure à la limite autorisée.");

            // Vérifie si le fichier existe avant de le télécharger.
            if(file_exists("upload/" . $_FILES["csv"]["name"])){
                echo $_FILES["csv"]["name"] . " existe déjà.";
            } else{
                move_uploaded_file($_FILES["csv"]["tmp_name"], "upload/" . $_FILES["csv"]["name"]);
                echo "Votre fichier a été téléchargé avec succès.";
            } 
    } else{
        echo "Error: " . $_FILES["csv"]["error"];
    }



	$csvFileToLoad = "upload/" . $_FILES["csv"]["name"]; 

}

try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

$file = fopen($csvFileToLoad, "r");
        while (($emapData = fgetcsv($file, 10000, ";")) !== FALSE)
        {
            $Reference = $emapData[0];
            $Marque = $emapData[4];
            $Designation = $emapData[3];
            $Prix = $emapData[5];

            $sql = $bdd->prepare("INSERT into catalogue(Reference, Marque, Designation, Prix) values(:Reference, :Marque, :Designation, :Prix)");
            $sql -> execute(array(
                'Reference' => $Reference, 
                'Marque'  =>$Marque, 
                'Designation' => $Designation, 
                'Prix' => $Prix
            ));
        }
        fclose($file);
        echo "Le fichier CSV a été correctement importé..";
  

?>

	<!--Zone du footer-->
	<?php include("../include/footer.php"); ?>
</body>
</html>
