﻿<html lang="fr">
<head>
	<meta charset="utf-8"/>
	<title>Method Not Allowed</title>
	<link rel="icon" type="image/png" href="/favicon.png" />
	<link type="text/css" rel="stylesheet" href="/css/style.css" />
	<link type="image/jpg" rel="icon" href="/img/favicon.jpg"/>
	<link rel="stylesheet" href="/css/jquery.mCustomScrollbar.css" />
</head>
	<body>
	<!--Entete-->
		<?php include("/include/page_up.php"); ?>
	<!---------->
		<div style="padding:50">
			<center><label><h2>Méthode de requête non autorisée.</h2></label></center>
		</div>
	<!--Footer-->
		<?php include("/include/footer.php"); ?>
</body>
</html>
