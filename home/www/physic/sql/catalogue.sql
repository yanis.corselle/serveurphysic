-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Client: 127.0.0.1
-- Généré le : Lun 11 Février 2019 à 18:14
-- Version du serveur: 5.5.20
-- Version de PHP: 5.3.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `physic`
--

-- --------------------------------------------------------

--
-- Structure de la table `catalogue`
--

CREATE TABLE IF NOT EXISTS `catalogue` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `Reference` varchar(255) NOT NULL,
  `Marque` varchar(255) NOT NULL,
  `Designation` varchar(255) NOT NULL,
  `Prix` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=129 ;

--
-- Contenu de la table `catalogue`
--

INSERT INTO `catalogue` (`id`, `Reference`, `Marque`, `Designation`, `Prix`) VALUES
(117, 'Reference', 'Marque', 'Designation', 'Prix'),
(118, '4095914', 'EPSON', 'Epson EB-2155W - Projecteur 3LCD - 5000 lumens (blanc) - 5000 lumens (couleur) - WXGA (1280 x 800) - 16:10 - HD 720p - 802.11b/g/n wireless / LAN / Miracast', '920'),
(119, '4095917', 'EPSON', 'Epson EB-2250U - Projecteur LCD - 5000 lumens (blanc) - 5000 lumens (couleur) - WUXGA (1920 x 1200) - 16:10 - HD 1080p - LAN', '1 234,96'),
(120, '3601413', 'BENQ', 'BenQ SW921 - Projecteur DLP - 3D - 5000 ANSI lumens - WXGA (1280 x 800) - 16:10 - HD 720p', '899,36'),
(121, '4095913', 'EPSON', 'Epson EB-2055 - Projecteur 3LCD - 5000 lumens (blanc) - 5000 lumens (couleur) - XGA (1024 x 768) - 4:3 - 802.11 b/g/n sans fil/ LAN', '909,38'),
(122, '3601412', 'BENQ', 'BenQ SX920 - Projecteur DLP - 3D - 5000 ANSI lumens - XGA (1024 x 768) - 4:3', '994,49'),
(123, '4386743', 'BENQ', 'BenQ MH760 - Projecteur DLP - 3D - 5000 ANSI lumens - Full HD (1920 x 1080) - 16:9 - HD 1080p', '995,61'),
(124, '3604772', 'BENQ', 'BenQ SU922 - Projecteur DLP - 3D - 5000 ANSI lumens - WUXGA (1920 x 1200) - 16:10 - HD 1080p', '1 240,08'),
(125, '2079124', 'BENQ', 'BenQ SP870 - Projecteur DLP - 5000 lumens - XGA (1024 x 768) - 4:3', '1 470,69'),
(126, '4095916', 'EPSON', 'Epson EB-2255U - Projecteur LCD - 5000 lumens (blanc) - 5000 lumens (couleur) - WUXGA (1920 x 1200) - 16:10 - HD 1080p - sans fil 802.11n/LAN/Miracast', '1 514,08'),
(127, '3331030', 'BENQ', 'BenQ SU917 - Projecteur DLP - 3D - 5000 ANSI lumens - WUXGA (1920 x 1200) - 16:10 - HD 1080p - LAN', '2 755,05'),
(128, '4495825', 'EPSON', 'Epson EB-L1000U - Projecteur LCD - 5000 lumens (blanc) - 5000 lumens (couleur) - WUXGA (1920 x 1200) - 16:10 - HD 1080p - LAN', '5 968,04');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
