﻿<?php

$host = 'localhost';

$dbname = 'physic';

$username = 'admin';

$password = 'root';


$dsn = "mysql:host=$host;dbname=$dbname"; 

// récupérer tous les utilisateurs
if(isset($_POST['tri']))
{
	if($_POST['tri'] == 'prix_croissant') 
	{
		$sql = "SELECT * FROM catalogue ORDER BY Prix ASC";
	}
	elseif($_POST['tri'] == 'prix_decroissant') 
	{
		$sql = "SELECT * FROM catalogue ORDER BY Prix DESC";
	}
}
else
{
	$sql = "SELECT * FROM catalogue";
}

$sql2 = "SELECT DISTINCT Categorie FROM catalogue";

try{

 $pdo = new PDO($dsn, $username, $password);

 $stmt = $pdo->query($sql);
 $stmt2 = $pdo->query($sql2);

 

 if($stmt === false || $stmt2 == false){

  die("Erreur");

 }

 

}catch (PDOException $e){

  echo $e->getMessage();

}



//include("RequeteAJAX.php");

?>



<html lang="fr">

<head>

	<meta charset="utf-8"/>

	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>

	<link rel="icon" type="image/png" href="../img/favicon.png" />

	<link type="text/css" rel="stylesheet" href="../css/style.css" />
	<link type="text/css" rel="stylesheet" href="../css/simple-grid.css" />

	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>



</head>

	<body>

	<!--Entete-->

		<?php include("../include/page_upcatalogue.php"); ?>



	<!--Image logo-->

<?php

	//--------------------------------- TRAITEMENTS PHP ---------------------------------//
//--- AFFICHAGE DES CATEGORIES ---//
$contenu .= '<div class="boutique-gauche">';
$contenu .= "<ul>";
while($row = $stmt2->fetch(PDO::FETCH_ASSOC))
{
    $contenu .= "<li><a href='?categorie=" . $row['Categorie'] . "'>" . $row['Categorie'] . "</a></li>";
}
$contenu .= "</ul>";
$contenu .= "</div>";
//--- AFFICHAGE DES PRODUITS ---//
$contenu .= '<div class="boutique-droite">';	
$contenu .= "<h1> Nos produits : </h1>";
?>
<div class="zones_boutique">
	<div class="gauche">
		<form action = "" method = "POST" >
		<input type = "text" name = "terme">
		<input type = "submit" name = "recherche" value = "Rechercher">
		</form>
	</div>
	
	<div class="droite">
		<form method="POST" action="" id="zone5">
			<label for="tri-select">Trier par:</label>

			<select name="tri" id="tri-select">
				<option value="">Affichage par défaut</option>
				<option value="prix_croissant">Prix croissant</option>
				<option value="prix_decroissant">Prix décroissant</option>
			</select>
			<input type="submit" value="Trier">

		</form>
	</div>
</div>

<?php
if(isset($_POST['recherche']))
{
	$_POST['terme'] = htmlspecialchars($_POST['terme']);
	if($_POST['recherche'] == 'Rechercher')
	{
		$contenu .= '<div class="container">';
		$emplacement_row=0;
		while($row = $stmt->fetch(PDO::FETCH_ASSOC))
		{
			
			if ((strstr($row['Designation'], $_POST['terme']) != FALSE) || (strstr($row['Reference'], $_POST['terme']) != FALSE))
			{
				if($emplacement_row == 4)
				{
					$emplacement_row = 0;
				}

				if($emplacement_row == 0)
				{
					$contenu .= '<div class="row">';
				}

				$contenu .= '<div class="col-3">';
				$titre = stristr($row['Designation'], ' - ', true);
				$id = htmlspecialchars($row['id']);
				if(file_exists("upload/img_cata/" . $id . ".png"))
				{
					$image = "upload/img_cata/" . $id . ".png";
				}  

				elseif(file_exists("upload/img_cata/" . $id . ".jpg"))
				{
					$image = "upload/img_cata/" . $id . ".jpg";
				}  

				elseif(file_exists("upload/img_cata/" . $id . ".jpeg"))
				{
					$image = "upload/img_cata/" . $id . ".jpeg";
				}  

				else{
					$image = "upload/img_cata/null.png";
				}
				$contenu .= '<div class="boutique-produit">';
				$contenu .= "<div class=\"visible_boutique\"><h2>$titre</h2> </div>";
				$contenu .= "<div class=\"invisible_boutique\"><p>$row[Designation]</p> </div>";
				$contenu .= "<div class=\"photos_boutique\"><img src=$image width=\"130\" height=\"100\"> </div>";
				$contenu .= "<p>$row[Prix] €</p>";
				$contenu .= "<p>Référence du produit : $row[Reference] </p>";
				$contenu .= "<a href=\"client/panier.php?action=ajout&amp;l=" . $row['Designation'] . "&amp;q=1&amp;p=" . $row['Prix'] . "&amp;r=" . $row['Reference'] ." \"onclick=\"window.open(this.href, '';\">Ajouter au panier</a>";
				$contenu .= '</div>';
				$contenu .= '</div>';
				if($emplacement_row == 3)
				{
					$contenu .= '</div>';
				}
				$emplacement_row += 1;
				
				
			}
			
		}
		$contenu .= '</div>';
	}
	
}
else
{
	$emplacement_row = 0;
	$contenu .= '<div class="container">';
	while($row = $stmt->fetch(PDO::FETCH_ASSOC))
	{

		if($emplacement_row == 4)
			{
				$emplacement_row = 0;
			}

			if($emplacement_row == 0)
			{
				$contenu .= '<div class="row">';
			}

			$contenu .= '<div class="col-3">';
			$titre = stristr($row['Designation'], ' - ', true);
			$id = htmlspecialchars($row['id']);
			if(file_exists("upload/img_cata/" . $id . ".png"))
			{
				$image = "upload/img_cata/" . $id . ".png";
			}  

			elseif(file_exists("upload/img_cata/" . $id . ".jpg"))
			{
				$image = "upload/img_cata/" . $id . ".jpg";
			}  

			elseif(file_exists("upload/img_cata/" . $id . ".jpeg"))
			{
				$image = "upload/img_cata/" . $id . ".jpeg";
			}  

			else{
				$image = "upload/img_cata/null.png";
			}
			$contenu .= '<div class="boutique-produit">';
			$contenu .= "<div class=\"visible_boutique\"><h2>$titre</h2> </div>";
			$contenu .= "<div class=\"invisible_boutique\"><p>$row[Designation]</p> </div>";
			$contenu .= "<div class=\"photos_boutique\"><img src=$image width=\"130\" height=\"100\"> </div>";
			$contenu .= "<p>$row[Prix] €</p>";
			$contenu .= "<p>Référence du produit : $row[Reference] </p>";
			$contenu .= "<a href=\"client/panier.php?action=ajout&amp;l=" . $row['Designation'] . "&amp;q=1&amp;p=" . $row['Prix'] . "&amp;r=" . $row['Reference'] ." \"onclick=\"window.open(this.href, '';\">Ajouter au panier</a>";
			$contenu .= '</div>';
			$contenu .= '</div>';
			
			if($emplacement_row == 3)
			{
				$contenu .= '</div>';
			}
			$emplacement_row += 1;
				
				
			}
		
	$contenu .= '</div>';
	$contenu .= '</div>';
}

//--------------------------------- AFFICHAGE HTML ---------------------------------//
$contenu .= '</div>';
echo $contenu;
?>

		<!--Zone du footer-->

		<?php include("../include/footercatalogue.php"); ?>

		<!--<script type="text/javascript" src="../../js/Remise.js" charset="utf-8"></script>-->

</body>

</html>

