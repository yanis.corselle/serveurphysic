﻿<?php

	try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

	?>

<html lang="fr">

<head>

	<meta charset="utf-8"/>

	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>

	<link rel="icon" type="image/png" href="favicon.png" />

	<link type="text/css" rel="stylesheet" href="../css/style.css" />

	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>

	<link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css" />

</head>

	<body>

	<!--Entete-->

		<?php include("../include/page_up.php"); ?>



	


	<!--Classe Mobile-->

		<div class="zone4">

			
			<div class="classe_numerique">
				<div id="texte_classe"> La « classe mobile », est soit une valise, soit un meuble sur roulettes pouvant contenir un certain nombre d’ordinateurs portables, de tablettes ou de PC 2 en 1 selon les constructeurs. Les ordinateurs sont reliés à la valise ou au meuble pour être rechargés et connectés au réseau de l'établissement scolaire grâce à une borne Wi-Fi.</div>

				<img src="../img/classe_mobile.png" height="250px" id="classe_mobile"/>
			</div>

			<img src="../img/valise.png" width="250"/>

			
			
			

			<br/>

		</div>



<!--Zone du footer-->

	<?php include("../include/footer.php"); ?>

</body>

</html>

