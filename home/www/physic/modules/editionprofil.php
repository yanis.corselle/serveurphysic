﻿<?php
			session_start();

			try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

		if(isset($_SESSION['pseudo']))
			{
				$requser = $bdd->prepare("SELECT * FROM `membres` WHERE id = ?");
				$requser->execute(array($_SESSION['id']));
				$user = $requser->fetch();

					if(isset($_POST['newpseudo']) AND empty($_POST['newpseudo']) == FALSE AND $_POST['newpseudo'] != $user['pseudo'])
					{
						$newpseudo = htmlspecialchars($_POST['newpseudo']);
						$insertpseudo = $bdd->prepare("UPDATE membres SET pseudo = ? WHERE id = ?");
						$insertpseudo->execute(array($newpseudo, $_SESSION['id']));
						header('Location: ../modules/profil.php');
					}

					if(isset($_POST['newmail']) AND empty($_POST['newmail']) == FALSE AND $_POST['newmail'] != $user['mail'])
					{
						$newmail = htmlspecialchars($_POST['newmail']);
						$insertmail = $bdd->prepare("UPDATE membres SET mail = ? WHERE id = ?");
						$insertmail->execute(array($newmail, $_SESSION['id']));
						header('Location: ../modules/profil.php');
					}

					if(isset($_POST['newmdp']) AND empty($_POST['newmdp']) == FALSE AND isset($_POST['newmdp2']) AND !empty($_POST['newmdp2'])) {
					{
						$mdp1 = sha1($_POST['newmdp']);
						$mdp2 = sha1($_POST['newmdp2']);

							if($mdp1 == $mdp2)
							{
								$insertmdp = $bdd->prepare("UPDATE membres SET mdp = ? WHERE id = ?");
								$insertmdp->execute(array($mdp1, $_SESSION['id']));
								header('Location: ../modules/profil.php?id='.$_SESSION['id']);
							}else{
								$msg = "<font color='red'>Vos deux mots de passes ne correspondent pas !</font>";
							}
					}



			}
?>
<html lang="fr">
<head>
	<meta charset="utf-8"/>
	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>
	<link rel="icon" type="image/png" href="favicon.png" />
	<link type="text/css" rel="stylesheet" href="../css/style.css" />
	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>
	<link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css" />
</head>
	<body>
	<!--Entete-->
		<?php include("../include/page_up.php"); ?>

		<!--Image nouveauxproduits2-->
	<img href="../img/nouveauxproduits2.png">

		<!--Connexion-->


	<!--formulaire d'édition du profil-->

		<div class="zone4">
		<center>
         <h2>Edition du profil</h2>
         <form method="POST" action="" class="formedition">
			<input type="text" name="newpseudo" placeholder="Nom d'utilisateur" class="inputbasic" value="<?php echo $user['pseudo']; ?>"/></br></br>
			<input type="text" name="newmail" placeholder="Email" class="inputbasic" value="<?php echo $user['mail']; ?>"/></br></br>
			<input type="password" name="newmdp" class="inputbasic" placeholder="Mot de passe"/></br></br>
			<input type="password" name="newmdp2" class="inputbasic" placeholder="Veuillez retaper votre mot de passe."/></br></br>
			<input type="submit" value="Mettre à jour mon profil" name="modifier">
		</form>
		<?php if(isset($msg)) { echo $msg; } ?>
		</center>
		</div>

	<!--Zone du footer-->
	<?php include("../include/footer.php"); ?>
</body>
</html>
<?php
}
else{
		header("Location: ../modules/seconnecter.php");
}
?>
