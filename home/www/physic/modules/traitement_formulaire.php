<?php
	try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
	?>
<html lang="fr">
<head>
	<meta charset="utf-8"/>
	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>
	<link rel="icon" type="image/png" href="favicon.png" />
	<link type="text/css" rel="stylesheet" href="../css/style.css" />
	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>
	<link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css" />
</head>
	<body>
	<!--Entete-->
		<?php include("../include/page_up"); ?>

	<!--Zone "Contact"-->
		<div class="zonecontact">
			<img src="../img/contact.png"/>
		</div>

	<!--Zone "Formulaire de contact"-->
		<center>
		<div class="">
			<form action="traitement_formulaire.php" method="POST" class="formcontact">
				<table border="0px" name="formulaire" class="table">
					<tr>
						<td><center><legend>Formulaire de contact</legend></center></td>
					</tr>

					<tr>
					</tr>

					<tr>
						<td><input type="text" border="0px" placeholder="Nom" class="inputbasic"/></td>
					</tr>

					<tr>
						<td><input type="text" border="0px" placeholder="Prénom" class="inputbasic"/></td>
					</tr>
					<tr>
						<td><input type="text" border="0px" placeholder="Numéro de téléphone" class="inputbasic"></td>
					</tr>
					<tr>
						<td><input type="text" border="0px" placeholder="Adresse" class="inputbasic"></td>
					</tr>
					<tr>
						<td><input type="text" border="0px" placeholder="Code Postal" class="inputbasic"></td>
					</tr>
					<tr>
						<td><input type="text" border="0px" placeholder="Email" class="inputbasic"></td>
					</tr>
					<tr>
						<td>
							<select id="rappel" value="Rapp" class="select">
								<option value="oui">Oui</option>
								<option value="non">Non</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>
							<select id="sujet" name="sujet" placeholder="Sujet" class="select">
								<option value="problemecompatibilite">Problème compatibilité</option>
								<option value="problemetechnique">Problème technique</option>
								<option value="demandeprealableintervention">Demande préalable d'intervention</option>
								<option value="renseignement">Renseignements</option>
								<option value="autres">Autres</option>
							</select>
					</tr>
					<tr>
						<td><textarea id="message" cols="60" rows="10" placeholder="Veuillez entrer votre message ici." class="textarea"></textarea></td>
					</tr>
					<tr>
						<td><input type="submit" value="Envoyer" name="valider"></td>
					</tr>
					<tr>
						<td><input type="reset" name="reset" value="Annuler"/></td>
					</tr>
				</table>
			</form>
		</div>
		</center>

		<!--Zone du footer-->
		<?php include("../include/footer.php"); ?>
</body>
</html>
