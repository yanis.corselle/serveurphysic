﻿<html lang="fr">
<head>
	<meta charset="utf-8"/>
	<title>Proxy Error</title>
	<link rel="icon" type="image/png" href="favicon.png" />
	<link type="text/css" rel="stylesheet" href="/css/style.css" />
	<link type="image/jpg" rel="icon" href="/img/favicon.jpg"/>
</head>
	<body>
	<!--Entete-->
		<?php include("/include/page_up.php"); ?>
	<!---------->
		<div style="padding:50">
			<center><label><h2>En agissant en tant que serveur proxy ou passerelle, le serveur a reçu une réponse invalide depuis le serveur distant.</h2></label></center>
		</div>
	<!--Footer-->
		<?php include("/include/footer.php"); ?>
</body>
</html>
