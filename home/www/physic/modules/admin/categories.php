<?php
function Categories($variablecategorie) {
    if($variablecategorie=="Accessoires PC"){echo '<option value="Accessoires PC" selected="selected"> Accessoires PC </option>';}else{echo '<option value="Accessoires PC"> Accessoires PC </option>';}
    if($variablecategorie=="Accessoires portable"){echo '<option value="Accessoires portable" selected="selected"> Accessoires portable </option>';}else{echo '<option value="Accessoires portable"> Accessoires portable </option>';}
	if($variablecategorie=="Appareil photos"){echo '<option value="Appareil photos" selected="selected"> Appareil photos </option>';}else{echo '<option value="Appareil photos"> Appareil photos </option>';}
    if($variablecategorie=="Classe mobile"){echo '<option value="Classe mobile" selected="selected"> Classe mobile </option>';}else{echo '<option value="Classe mobile"> Classe mobile </option>';}
    if($variablecategorie=="Client léger"){echo '<option value="Client léger" selected="selected"> Client léger </option>';}else{echo '<option value="Client léger"> Client léger </option>';}
    if($variablecategorie=="Divers"){echo '<option value="Divers" selected="selected"> Divers </option>';}else{echo '<option value="Divers"> Divers </option>';}
    if($variablecategorie=="Ecran"){echo '<option value="Ecran" selected="selected"> Ecran </option>';}else{echo '<option value="Ecran"> Ecran </option>';}
	if($variablecategorie=="GPS"){echo '<option value="GPS" selected="selected"> GPS </option>';}else{echo '<option value="GPS"> GPS </option>';}
    if($variablecategorie=="Imprimante"){echo '<option value="Imprimante" selected="selected"> Imprimante </option>';}else{echo '<option value="Imprimante"> Imprimante </option>';}
	if($variablecategorie=="Lecteur DVD"){echo '<option value="Lecteur DVD" selected="selected"> Lecteur DVD </option>';}else{echo '<option value="Lecteur DVD"> Lecteur DVD </option>';}
    if($variablecategorie=="Mémoire"){echo '<option value="Mémoire" selected="selected"> Mémoire </option>';}else{echo '<option value="Mémoire"> Mémoire </option>';}
    if($variablecategorie=="Moniteur"){echo '<option value="Moniteur" selected="selected"> Moniteur </option>';}else{echo '<option value="Moniteur"> Moniteur </option>';}
	if($variablecategorie=="Option Appareil photos"){echo '<option value="Option Appareil photos" selected="selected"> Option Appareil photos </option>';}else{echo '<option value="Option Appareil photos"> Option Appareil photos </option>';}
	if($variablecategorie=="Option TV et DVD"){echo '<option value="Option TV et DVD" selected="selected"> Option TV et DVD </option>';}else{echo '<option value="Option TV et DVD"> Option TV et DVD </option>';}
	if($variablecategorie=="Option Video projecteur"){echo '<option value="Option Video projecteur" selected="selected"> Option Video projecteur </option>';}else{echo '<option value="Option Video projecteur"> Option Video projecteur </option>';}
	if($variablecategorie=="TV"){echo '<option value="TV" selected="selected"> TV </option>';}else{echo '<option value="TV"> TV </option>';}
    if($variablecategorie=="PC"){echo '<option value="PC" selected="selected"> PC </option>';}else{echo '<option value="PC"> PC </option>';}
    if($variablecategorie=="Périphérique"){echo '<option value="Périphérique" selected="selected"> Périphérique </option>';}else{echo '<option value="Périphérique"> Périphérique </option>';}
    if($variablecategorie=="Portable"){echo '<option value="Portable" selected="selected"> Portable </option>';}else{echo '<option value="Portable"> Portable </option>';}
    if($variablecategorie=="Produit Apple et accessoires"){echo '<option value="Produit Apple et accessoires" selected="selected"> Produit Apple et accessoires </option>';}else{echo '<option value="Produit Apple et accessoires"> Produit Apple et accessoires </option>';}
    if($variablecategorie=="Serveur"){echo '<option value="Serveur" selected="selected"> Serveur </option>';}else{echo '<option value="Serveur"> Serveur </option>';}
    if($variablecategorie=="Stockage"){echo '<option value="Stockage" selected="selected"> Stockage </option>';}else{echo '<option value="Stockage"> Stockage </option>';}
    if($variablecategorie=="Tablette et accessoires"){echo '<option value="Tablette et accessoires"> Tablette et accessoires </option>';}else{echo '<option value="Tablette et accessoires"> Tablette et accessoires </option>';}
    if($variablecategorie=="TBI/VPI"){echo '<option value="TBI/VPI"> TBI/VPI </option>';}else{echo '<option value="TBI/VPI"> TBI/VPI </option>';}
    if($variablecategorie=="Unité centrale"){echo '<option value="Unité centrale"> Unité centrale </option>';}else{echo '<option value="Unité centrale"> Unité centrale </option>';}
    if($variablecategorie=="Video projecteur"){echo '<option value="Video projecteur"> Video projecteur </option>';}else{echo '<option value="Video projecteur"> Video projecteur </option>';}
}
function CategoriesSansSelection() {
?>
    <option value="Accessoires PC"> Accessoires PC </option>
    <option value="Accessoires portable"> Accessoires portable </option>
    <option value="Appareil photos"> Appareil photos </option>
    <option value="Classe mobile"> Classe mobile </option>
    <option value="Divers"> Divers </option>
    <option value="Ecran"> Ecran </option>
    <option value="GPS"> GPS </option>
    <option value="Imprimante"> Imprimante </option>
    <option value="Lecteur DVD"> Lecteur DVD </option>
    <option value="Mémoire"> Mémoire </option>
    <option value="Moniteur"> Moniteur </option>
    <option value="Option Appareil photos"> Option Appareil photos </option>
    <option value="Option TV et DVD"> Option TV et DVD </option>
    <option value="Option Video projecteur"> Option Video projecteur </option>
    <option value="PC"> PC </option>
    <option value="Périphérique"> Périphérique </option>
    <option value="Portable"> Portable </option>
    <option value="Produit Apple et accessoires"> Produit Apple et accessoires </option>
    <option value="Serveur"> Serveur </option>
    <option value="Stockage"> Stockage </option>
    <option value="Tablette et accessoires"> Tablette et accessoires </option>
    <option value="TBI/VPI"> TBI/VPI </option>
    <option value="TV"> TV </option>
    <option value="Unité centrale"> Unité centrale </option>
    <option value="Video projecteur"> Video projecteur </option>
<?php
}
function SousCategoriePortable($variablesouscategorie) {
    if($variablesouscategorie=="<15"){echo '<option value="<15" selected="selected"> < 15\' </option>';}else{echo '<option value="<15"> < 15\' </option>';}
    if($variablesouscategorie=="15-16,9"){echo '<option value="15-16,9" selected="selected"> 15 - 16,9\' </option>';}else{echo '<option value="15-16,9"> 15 - 16,9\' </option>';}
    if($variablesouscategorie=="<17"){echo '<option value="<17" selected="selected"> 17\' + </option>';}else{echo '<option value="<17"> 17\' + </option>';}
}
function SousCategoriePC($variablesouscategorie) {
    if($variablesouscategorie=="Desktop"){echo '<option value="Desktop" selected="selected"> Desktop </option>';}else{echo '<option value="Desktop"> Desktop </option>';}
    if($variablesouscategorie=="Micro/Mini tour"){echo '<option value="Micro/Mini tour" selected="selected"> Micro/Mini tour </option>';}else{echo '<option value="Micro/Mini tour"> Micro/Mini tour </option>';}
    if($variablesouscategorie=="Moyen tour"){echo '<option value="Moyen tour" selected="selected"> Moyen tour </option>';}else{echo '<option value="Moyen tour"> Moyen tour </option>';}
    if($variablesouscategorie=="Grand public"){echo '<option value="Grand public" selected="selected"> Grand public </option>';}else{echo '<option value="Grand public"> Grand public </option>';}
    if($variablesouscategorie=="Client léger"){echo '<option value="Client léger" selected="selected"> Client léger </option>';}else{echo '<option value="Client léger"> Client léger </option>';}
}
?>
