<?php
			session_start();
			if(isset($_SESSION['attribut']) == FALSE OR $_SESSION['attribut'] == 0)
			{
				header("Location:../../modules/profil.php");
			}

			try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }


		
		if(isset($_SESSION['attribut']))
		{
			if($_SESSION['attribut'] == 1)
			{
				
				$requser = $bdd->prepare("SELECT * FROM accueil WHERE id=?");
				$requser->execute(array($_POST['id']));
				$acc = $requser->fetch();
				$id=$_POST['edit'];

				if(isset($_POST['new_acc_img']) AND $_POST['new_acc_img'] != $user['acc_img'])
				{
					$new_acc_img = htmlspecialchars($_POST['new_acc_img']);
					$insert_acc_img = $bdd->prepare("UPDATE accueil SET acc_img = ? WHERE id =?");
					$insert_acc_img->execute(array($new_acc_img, $_POST['edit']));
				}

				if(isset($_POST['new_acc_article']) AND $_POST['new_acc_article'] != $user['acc_article'])
				{
					$new_acc_article = htmlspecialchars($_POST['new_acc_article']);
					$insert_acc_article = $bdd->prepare("UPDATE accueil SET acc_article = ? WHERE id =?");
					$insert_acc_article->execute(array($new_acc_article, $_POST['edit']));
				}




			if(isset($_POST['modif']))
			{
				header('Location: gest_acc.php');
			}
	


			
		

?>
<html lang="fr">
<head>
	<meta charset="utf-8"/>
	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>
	<link rel="icon" type="image/png" href="favicon.png" />
	<link type="text/css" rel="stylesheet" href="../../../css/style.css" />
	<link type="image/jpg" rel="icon" href="../../../img/favicon.jpg"/>
	<link rel="stylesheet" href="../../../css/jquery.mCustomScrollbar.css" />
</head>
	<body>
	<!--Entete-->
		<?php include("../../../include/page_upacc.php"); ?>

	<!--formulaire d'édition du profil-->

		<div class="zone4">
		<center>
         <h2>Edition de l'article</h2>
         <form method="POST" action="" class="formedition">

		 	<input type="file" name="new_acc_img" placeholder="Nouvelle image" class="inputbasic" value="<?php echo $user['acc_img']; ?>"/></br></br>
			<input type="text" name="new_acc_article" placeholder="Nouveau texte de l'article" class="inputbasic" value="<?php echo $user['acc_article']; ?>"/></br></br>
			<input type="hidden" type='submit' name="modif" value="true">
			<input type="hidden" type='submit' name="edit" value=<?php echo($_POST['edit']) ?>>
			<input type="submit" value="Mettre à jour le profil" name="modifier">
		</form>
		<?php if(isset($msg)) { echo $msg; } ?>
		</center>
		</div>

	<!--Zone du footer-->
	<?php include("../../../include/footeracc.php"); ?>
</body>
<script type="text/javascript" src="../../../js/jquery.min.js"></script>


</html>
<?php
			}
		}
?>