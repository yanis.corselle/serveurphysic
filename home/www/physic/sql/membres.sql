-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Client: 127.0.0.1
-- Généré le : Lun 11 Février 2019 à 18:14
-- Version du serveur: 5.5.20
-- Version de PHP: 5.3.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `physic`
--

-- --------------------------------------------------------

--
-- Structure de la table `membres`
--

CREATE TABLE IF NOT EXISTS `membres` (
  `attribut` tinyint(2) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `mdp` text NOT NULL,
  `nom_societe` text NOT NULL,
  `fonction` text NOT NULL,
  `nom` text NOT NULL,
  `prenom` text NOT NULL,
  `adresse` text NOT NULL,
  `numero_tel` text NOT NULL,
  `marge` float(11,2) NOT NULL,
  `remise1` float(11,2) NOT NULL,
  `remise2` float(11,2) NOT NULL,
  `remise3` float(11,2) NOT NULL,
  `forgetpass` varchar(255) NOT NULL,
  PRIMARY KEY (`pseudo`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Contenu de la table `membres`
--

INSERT INTO `membres` (`attribut`, `id`, `pseudo`, `mail`, `mdp`, `nom_societe`, `fonction`, `nom`, `prenom`, `adresse`, `numero_tel`, `marge`, `remise1`, `remise2`, `remise3`, `forgetpass`) VALUES
(1, 20, 'admin', 'admin@admin.admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'physic', 'admin', 'admin', 'admin', 'admin', 'admin', 100.00, 74.00, 0.00, 0.00, ''),
(0, 21, 'azerty', 'azerty@azerty.azerty', '9cf95dacd226dcf43da376cdb6cbba7035218921', 'azerty', 'azerty', 'azerty', 'azerty', 'azerty', 'azerty', 0.00, 0.00, 0.00, 0.00, ''),
(0, 25, 'mairiearras', 'philippe.vilbert@physic.fr', 'b6a97a4af36e7aa7c8552e65bb393d8f975f0bd4', 'Mairie Arras', 'responsable info', 'Jerome', 'Mailleu', '1 place de ville', '0322670670', 0.00, 0.00, 0.00, 0.00, ''),
(0, 24, 'test1', 'test@test.test', 'b444ac06613fc8d63795be9ad0beaf55011936ac', 'test', 'test', 'test', 'test', 'test', '04875487503', 0.00, 0.00, 0.00, 0.00, '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
