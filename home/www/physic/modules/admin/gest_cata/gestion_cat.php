<?php
session_start();

if(isset($_SESSION['attribut']) == FALSE OR $_SESSION['attribut'] == 0)

{

	header("Location:../../profil.php");

}



$host = 'localhost';

$dbname = 'physic';

$username = 'admin';

$password = 'root';

  

$dsn = "mysql:host=$host;dbname=$dbname"; 

// récupérer tous les utilisateurs

if(isset($_POST['tri']))
{
	if($_POST['tri'] == 'prix_croissant') 
	{
		$sql = "SELECT * FROM catalogue ASC ORDER BY Prix";
	}
	elseif($_POST['tri'] == 'prix_decroissant') 
	{
		$sql = "SELECT * FROM catalogue DESC ORDER BY Prix";
	}
	elseif($_POST['tri'] == 'reference') 
	{
		$sql = "SELECT * FROM catalogue ORDER BY Reference";
	}
	elseif($_POST['tri'] == 'titre') 
	{
		$sql = "SELECT * FROM catalogue ORDER BY Designation";
	}
	elseif($_POST['tri'] == 'marque') 
	{
		$sql = "SELECT * FROM catalogue ORDER BY Marque";
	}

	else{
		$sql = "SELECT * FROM catalogue LIMIT :debut, :limite ";
	}
}
else
{
	$sql = "SELECT * FROM catalogue";
}
 

try{

 $pdo = new PDO($dsn, $username, $password);

 $stmt = $pdo->query($sql);

 

 if($stmt === false){

  die("Erreur");

 }

 

}catch (PDOException $e){

  echo $e->getMessage();

}

//include("RequeteAJAX.php");

?>



<html lang="fr">


<head>

	<meta charset="utf-8"/>

	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>

	<link rel="icon" type="image/png" href="../../../img/favicon.png" />

	<link type="text/css" rel="stylesheet" href="../../../css/style.css" />

	<link type="image/jpg" rel="icon" href="../../../img/favicon.jpg"/>

	<script type="text/javascript" src="../../../js/jquery.js"></script>

</head>

	<body>

	<!--Entete-->

		<?php include("../../../include/page_upacc.php"); ?>



	<!--Image logo-->



	<!--Zone du tableau-->

	<div class="gest_cata">
		<center>


			<div class="zones_boutique">
				<div class="gauche">
					<form action = "" method = "POST" >
					<input type = "text" name = "terme">
					<input type = "submit" name = "recherche" value = "Rechercher">
					</form>
				</div>
				
				<div class="droite">
					<form method="POST" action="" >
						<label for="tri-select">Trier par:</label>

						<select name="tri" id="tri-select">
							<option value="">Affichage par défaut</option>
							<option value="prix_croissant">Prix croissant</option>
							<option value="prix_decroissant">Prix décroissant</option>
							<option value="reference">Référence</option>
							<option value="titre">Nom</option>
							<option value="marque">Marque</option>
						</select>
						<input type="submit" value="Trier">

					</form>
				</div>
			</div>

			<form action="suppr_cata.php" method="POST">

	        <table class="tableacc" name="" border="1px">

	            <tr id="tablecata">

						<th id="ID">ID</th>
						
						<th id="Categorie">Catégorie</th>

						<th id="Sous_Categorie">Sous-Catégorie</th>

						<th id="Reference_fournisseur">Reference fournisseur</th>
						
						<th id="Reference">Reference fabricant</th>

	                    <th id="Marque">Marque</th>

						<th id="Designation">Designation</th>

						<th id="Prix_PU_HT">Prix PU HT (sans remise)</th>

						<th id="Unite_mesure">Unités de mesure</th>

						<th id="Nombre_pieces">Nombre de pièces</th>

						<th id="Minimum_commande">Minimum de commande d'unité de conditionnement</th>

	                    <th id="Modifier">Modifier</th>

						<th id="Supprimer" name="Supprimer">Supprimer</th>

	            </tr>



				<tbody>
					<?php
						if(isset($_POST['recherche']))
						{
							if($_POST['recherche'] == 'Rechercher')
							{
								$_POST['terme'] = htmlspecialchars($_POST['terme']);
			
								while($row = $stmt->fetch(PDO::FETCH_ASSOC)) 
								{ 

									if(isset($_POST['terme']))
									{
										$sup=[]; 

										if ((stristr($row['ID'], $_POST['terme']) != FALSE) || (stristr($row['Categorie'], $_POST['terme']) != FALSE) || (stristr($row['Sous_Categorie'], $_POST['terme']) != FALSE) || (stristr($row['Reference'], $_POST['terme']) != FALSE) || (stristr($row['Marque'], $_POST['terme']) != FALSE) || (stristr($row['Designation'], $_POST['terme']) != FALSE) || (stristr($row['Prix'], $_POST['terme']) != FALSE))
										{?>

											<tr>

												<td id="ID"><?php echo htmlspecialchars($row['id']); ?></td>

												<td id="Categorie"><?php echo htmlspecialchars($row['Categorie']); ?></td>

												<td id="Sous_Categorie"><?php echo htmlspecialchars($row['Sous_Categorie']); ?></td>

												<td id="Reference_fournisseur"><?php echo htmlspecialchars($row['Reference_fournisseur']); ?></td>

												<td id="Reference_fabricant"><?php echo htmlspecialchars($row['Reference_fabricant']); ?></td>

												<td id="Marque"><?php echo htmlspecialchars($row['Marque']); ?></td>

												<td id="Designation"><?php echo htmlspecialchars($row['Designation']); ?></td>

												<td id="Prix_PU_HT"><?php echo htmlspecialchars($row['Prix_PU_HT']); ?></td>

												<td id="Unite_mesure"><?php echo htmlspecialchars($row['Unite_mesure']); ?></td>

												<td id="Nombre_pieces"><?php echo htmlspecialchars($row['Nombre_pieces']); ?></td>

												<td id="Minimum_commande"><?php echo htmlspecialchars($row['Minimum_commande']); ?></td>

												<td id="Modifier"> <a href="<?php echo("editer_cat.php?id=" . $row['id'])?>" > Modifier </a> </td>

												<td id="supprimer"><input type='checkbox' name="sup" value="<?php echo $row['id']; ?>"> </td>

											</tr>
										<?php
										}
									}
								}
							}
						}
						else{
							while($row = $stmt->fetch(PDO::FETCH_ASSOC)) 
							{ 
								$sup=[]; ?>

									<tr>

										<td id="ID"><?php echo htmlspecialchars($row['id']); ?></td>

										<td id="Categorie"><?php echo htmlspecialchars($row['Categorie']); ?></td>

										<td id="Sous_Categorie"><?php echo htmlspecialchars($row['Sous_Categorie']); ?></td>

										<td id="Reference_fournisseur"><?php echo htmlspecialchars($row['Reference_fournisseur']); ?></td>

										<td id="Reference_fabricant"><?php echo htmlspecialchars($row['Reference_fabricant']); ?></td>

										<td id="Marque"><?php echo htmlspecialchars($row['Marque']); ?></td>

										<td id="Designation"><?php echo htmlspecialchars($row['Designation']); ?></td>

										<td id="Prix_PU_HT"><?php echo htmlspecialchars($row['Prix_PU_HT']); ?></td>

										<td id="Unite_mesure"><?php echo htmlspecialchars($row['Unite_mesure']); ?></td>

										<td id="Nombre_pieces"><?php echo htmlspecialchars($row['Nombre_pieces']); ?></td>

										<td id="Minimum_commande"><?php echo htmlspecialchars($row['Minimum_commande']); ?></td>

										<td id="Modifier"> <a href="<?php echo("editer_cat.php?id=" . $row['id'])?>" > Modifier </a> </td>

										<td id="supprimer"><input type='checkbox' name="sup" value="<?php echo $row['id']; ?>"> </td>

									</tr>
										
									<?php
							}
						}
						?>


				 
   				</tbody>

			</table> 
			<div class="droite_gest_cata"> <input type="submit" value="Supprimer"> </div>
		</form>








		</center>


	</div>



		<!--Zone du footer-->

		<?php include("../../../include/footeracc.php"); ?>

		<!--<script type="text/javascript" src="../../js/Remise.js" charset="utf-8"></script>-->



		    <script language="javascript">

		      function confirme( identifiant )

		      {

		        var confirmation = confirm( "Voulez vous vraiment supprimer cet enregistrement ?" ) ;

			if( confirmation )

			{

			  document.location.href = "suppr_cata.php?idligne="+identifiant ;

			}

		      }

		    </script>

</body>

</html>

