<?php
session_start();
require_once '../connect.php';

if(isset($_GET['PDF']) && $_GET['PDF']=="dll")
{
    require_once("../../pdf/fpdf.php");

    $pdf = new FPDF();

    $pdf->AddPage("Landscape", "A3");

    $pdf->SetFont('Arial','B',16);
    $pdf->SetFillColor(238,238,238);
    $query3 = mysql_query("SELECT * FROM client WHERE NoClient = ".$_SESSION['noclient']);
    $client = mysql_fetch_array($query3);


    $pdf->Cell(200,7,"M. ".$client['Prenom']." ".$client['Nom'],0, 0, "L", false); //colonne 1
    $pdf->Cell(200,7,"Adresse : ".$client['Adresse'],0, 1, "R", false); //colonne 2
    $pdf->Cell(200,7,"Soci�t� : ".$client['Societe'],0, 0, "L", false); //1
    $pdf->Cell(200,7,$client['CodePostal'].", ".$client['Ville'],0, 1, "R", false); //2
    $pdf->Cell(200,7,"",0, 0, "L", false); //1
    $pdf->Cell(200,7,"Tel : ".$client['NoTelephone'],0, 1, "R", false); //2
    $pdf->Cell(200,7,"",0, 0, "L", false); //1
    $pdf->Cell(200,7,"Email : ".$client['Email'],0, 1, "R", false); //2

    $pdf->Cell(400,7,"",0, 1, "R", false);

    $pdf->Cell(15,7,"",0,0,"C",false);
    $pdf->Cell(50,10,"R�f�rence",1,0,"C",true);
    $pdf->Cell(90,10,"Marque",1,0,"C",true);
    $pdf->Cell(100,10,"Categorie",1,0,"C",true);
    $pdf->Cell(60,10,"Prix Unitaire HT",1,0,"C",true);
    $pdf->Cell(20,10,"Qt�",1,0,"C",true);
    $pdf->Cell(50,10,"Prix",1,1,"C",true);

    $numerocase=0;
    $prixTotal=0;
    $debutligne=10;
    $debutcolonne=55;

    foreach($_SESSION['commande'] as $refcommande)
    {
        $query = 'SELECT reference, description, marque, categorie, tarif FROM catalogue WHERE reference="'.$refcommande.'"';
        $res = mysql_query($query) or die(mysql_error());

        $query2 = "SELECT * FROM client WHERE NoClient =".$_SESSION['noclient'];
        $res2 = mysql_query($query2) or die(mysql_error());

        while($data2 = mysql_fetch_array($res2))
        {
            while($data = mysql_fetch_array($res))
            {
                if($data['categorie']=="Produit Apple et accessoires")
                {
                $prix = round((($data['tarif']*0.94)/0.9), 2);
                $prixR = round($prix*0.95, 2);
                $prixHT = $prixR*$_SESSION['quantite'][$numerocase];
                }
                else
                {
                $prix = round($data['tarif']/(1-$data2['MargePhysic']), 2);
                $prixR = round($prix *0.95, 2);
                $prixHT = $prixR*$_SESSION['quantite'][$numerocase];
                }

            $pdf->Cell(15,7,"",0,0,"C",false);
            $pdf->Cell(50,14,$data['reference'],1,0,"C",false);
            $pdf->Cell(90,14,$data['marque'],1,0,"C",false);
            $pdf->Cell(100,14,$data['categorie'],1,0,"C",false);
            $pdf->Cell(60,14,$prixR,1,0,"C",false);
            $pdf->Cell(20,14,$_SESSION['quantite'][$numerocase],1,0,"C",false);
            $pdf->Cell(50,14,$prixHT,1,1,"C",false);

            $numerocase= $numerocase+1;
            $prixTotal= round($prixTotal + $prixHT, 2);
            $prixTotalTTC=$prixTotal*1.20;
            }
        }
    }

    $pdf->Cell(400,7,"",0,1,"C",false);

    $pdf->Cell(295,7,"",0,0,"C",false);
    $pdf->Cell(40,14,"Prix Total HT: ",0,0,"R",false);
    $pdf->Cell(50,14,$prixTotal,1,1,"C",false);

    $pdf->Cell(295,7,"",0,0,"C",false);
    $pdf->Cell(40,14,"Prix Total TTC: ",0,0,"R",false);
    $pdf->Cell(50,14,$prixTotalTTC,1,1,"C",false);

    ob_end_clean();

    $pdf->Output('macommande.pdf', 'D');
}
?>