<?php

	try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

	?>

<html lang="fr">

<head>

	<meta charset="utf-8"/>

	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>

	<link rel="icon" type="image/png" href="favicon.png" />

	<link type="text/css" rel="stylesheet" href="../css/style.css" />

	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>

	<link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css" />

</head>

	<body>

	<!--Entete-->

		<?php include("../include/page_up.php"); ?>



	<!--Contact-->

		<div class="zone3">

				PhYsic

				<br>

		59 Avenue Roger Dumoulin

				<br>

		Espace Industriel Nord

				<br>

		80046 AMIENS CEDEX 02

				<br>

		Tel : 03 22 670 670

				<br>


		Email : <a href="mailto:info@physic.fr">info@physic.fr</a>

				<br>

		Web : <a href="">www.physic.fr</a>

				<br>

		</div>

	<!--Zone du footer-->

	<?php include("../include/footer.php"); ?>

</body>

</html>

