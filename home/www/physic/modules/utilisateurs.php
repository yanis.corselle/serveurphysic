<?php
	try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
  session_start();
  $array =$bdd->query("SELECT * FROM `membres`");
    if(!empty($_SESSION['attribut']) == 0)
  		{
  			header("Location:../modules/profil.php");
  		}
?>
<html lang="fr">
<head>
	<meta charset="utf-8"/>
	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>
	<link rel="icon" type="image/png" href="favicon.png" />
	<link type="text/css" rel="stylesheet" href="../css/style.css" />
	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>
	<link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css" />
</head>
	<body>
	<!--Entete-->
		<?php include("../include/page_up.php"); ?>

  <!--Zone de gestion des inscrits-->
    <div class="zone4">
        <form method="post" action="">

          <?php try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'root', ''); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
                $sql = 'SELECT * FROM membres';
                $req = $bdd->query($sql);
          ?>

          <table class="table" border="1px">
              <tr>
                      <th>Attribut</th>
                      <th>id</th>
                      <th>Nom d'utilisateur</th>
                      <th>Email</th>
                      <th>Mot de passe</th>
                      <th>Nom de la société</th>
                      <th>Fonction</th>
                      <th>Nom</th>
                      <th>Prénom</th>
                      <th>Adresse</th>
                      <th>Tel.</th>
                      <th>Marge</th>
                      <th>Remise</th>
              </tr>

                <?php
                  //Ici on récupère dans la variable $row, le fetch de la requète mise en array dans la variable $req
                  //On passe d'un tableau à un ensemble de variable :)
                   while ($row = $req->fetch()) {
                     echo "<tr>
                       <td id=\"attribut\">" . $row['attribut'] . "</td>
                       <td id=\"id\">" . $row['id'] . "</td>
                       <td id=\"pseudo\">" . $row['pseudo'] . "</td>
                       <td id=\"mail\">" . $row['mail'] . " </td>
                       <td id=\"mdp\">" . $row['mdp'] . " </td>
                       <td id=\"nom_societe\">" . $row['nom_societe'] . " </td>
                       <td id=\"fonction\">" . $row['fonction'] . " </td>
                       <td id=\"nom\">" . $row['nom'] . " </td>
                       <td id=\"prenom\">" . $row['prenom'] . " </td>
                       <td id=\"adresse\">" . $row['adresse'] . " </td>
                       <td id=\"numero_tel\">" . $row['numero_tel'] . " </td>
                       <td id=\"marge\">" . $row['marge'] . " </td>
                       <td id=\"remise\">" . $row['remise1'] . " </td>
                     </tr>";
                   }
                   $req->closeCursor();
                //On retire le curseur et on vient le réinitialiser pour éviter tout problèmes de doublons si on utilise à nouveau la variable $req
                  $req = NULL;
                ?>


		<!--Zone du footer-->
		<?php include("../include/footer.php"); ?>
</body>
</html>
