<?php
try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
?>
<html lang="fr">
<head>
	<meta charset="utf-8"/>
	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>
	<link rel="icon" type="image/png" href="favicon.png" />
	<link type="text/css" rel="stylesheet" href="../css/style.css" />
	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>
	<link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css" />

</head>
	<body>
	<!--Entete-->
		<?php include("../include/page_up.php"); ?>
<?php
	if(isset($_POST['envoi']))
		{
			if(!empty($_POST['nom']) AND !empty($_POST['prenom']) AND !empty($_POST['numero_tel']) AND !empty($_POST['adresse']) AND !empty($_POST['code_postal']) AND !empty($_POST['mail']) AND !empty($_POST['rappel']) AND !empty($_POST['sujet']) AND !empty($_POST['description']))
			{
				$destinataire = 'technique@physic.fr';
				$nom = htmlspecialchars($_POST['nom']);
				$prenom = htmlspecialchars($_POST['prenom']);
				$numero_tel = htmlspecialchars($_POST['numero_tel']);
				$adresse = htmlspecialchars($_POST['adresse']);
				$code_postal = htmlspecialchars($_POST['code_postal']);
				$mail = htmlspecialchars($_POST['mail']);
				$rappel = htmlspecialchars($_POST['rappel']);
				$sujet = htmlspecialchars($_POST['sujet']);
				$description = htmlspecialchars($_POST['description']);

				

				$message =  "Cette personne souhaite vous contacter : " .$nom."  ".$prenom." .</br> Numero pour la contacter: ".$numero_tel." </br> Son adresse: ".$adresse."  ".$code_postal." </br> Adresse mail: ".$mail." <br> Le client veut etre rappele: ".$rappel." <br> Raison du contact: ".$sujet." <br> Message: ".$description."  .<br>";

							//echo $message;

				$subject = "Demande de contact";


				if(mail($destinataire, $subject, $message))
				{
					echo '<script languag="javascript" >alert("Votre message a bien été envoyé ");</script>';
				}
				else // Non envoyé
				{
					echo '<script languag="javascript">alert("Votre message n\'a pas pu être envoyé");</script>';
				}
				
			}

		}
?>
		<center>
		<div class="">
			<form action="" method="POST" class="formcontact">
				<table border="0px" name="formulaire" class="table">
					<tr>
						<td><center><legend>Formulaire de contact</legend></center></td>
					</tr>

					<tr>
					</tr>

					<tr>
						<td><input type="text" border="0px" name="nom" placeholder="Nom" class="inputbasic"/></td>
					</tr>

					<tr>
						<td><input type="text" border="0px" name="prenom" placeholder="Prénom" class="inputbasic"/></td>
					</tr>
					<tr>
						<td><input type="text" border="0px" maxlength="10" format="NNNNNNNNNNNN" name="numero_tel" placeholder="Numéro de téléphone" class="inputbasic"></td>
					</tr>
					<tr>
						<td><input type="text" border="0px" name="adresse" placeholder="Adresse" class="inputbasic"></td>
					</tr>
					<tr>
						<td><input type="text" border="0px" name="code_postal" placeholder="Code Postal" class="inputbasic"></td>
					</tr>
					<tr>
						<td><input type="text" border="0px" name="mail" placeholder="Email" class="inputbasic"></td>
					</tr>
					<tr>
						<td>
							<input type="text" name="rappel" class="inputbasic" placeholder="Rappel ( Oui ou Non )">
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" name="sujet" class="inputbasic" placeholder="Sujet">
						</td>
					</tr>
					<tr>
						<td><textarea cols="60" rows="10" name="description" placeholder="Veuillez entrer votre message ici." class="textarea"></textarea></td>
					</tr>
					<tr>
						<td><input type="submit" name="envoi" value="Envoyer" name="valider"></td>
					</tr>
					<tr>
						<td><input type="reset" name="reset" value="Annuler"/></td>
					</tr>
				</table>
			</form>
		</div>
		</center>

	<!--Zone du footer-->
	<?php include("../include/footer.php"); ?>
</body>
</html>
