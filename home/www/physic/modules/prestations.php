<?php
	try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }



	if(isset($_POST['demandeintervention']))

	{

		



		if(isset($_POST['nom_societe']) AND isset($_POST['nom_contact']) AND isset($_POST['adresse']) AND isset($_POST['tel']) AND isset($_POST['date_intervention']) AND isset($_POST['debut_plage_horaire']) AND isset($_POST['fin_plage_horaire']) AND isset($_POST['garantie']) AND isset($_POST['date_achat']) AND isset($_POST['urgence']) AND isset($_POST['intervention_sur_site']) AND isset($_POST['devis'])  AND isset($_POST['marque']) AND isset($_POST['type']) AND isset($_POST['numserie']) AND isset($_POST['descriptif']) AND empty($_POST['nom_societe']) == FALSE AND empty($_POST['nom_contact']) == FALSE AND empty($_POST['adresse']) == FALSE AND empty($_POST['garantie']) == FALSE AND empty($_POST['urgence']) == FALSE AND empty($_POST['intervention_sur_site']) == FALSE AND empty($_POST['devis']) == FALSE AND empty($_POST['marque']) == FALSE AND empty($_POST['type']) == FALSE AND empty($_POST['numserie']) == FALSE AND empty($_POST['descriptif']) == FALSE)

		{
			$nom_societe = $_POST['nom_societe'];

			$nom_contact = $_POST['nom_contact'];

			$adresse = $_POST['adresse'];

			$tel = $_POST['tel'];

			$fax = $_POST['fax'];

			$portable = $_POST['portable'];

			$date_intervention = $_POST['date_intervention'];

			$debut_plage_horaire = $_POST['debut_plage_horaire'];

			$fin_plage_horaire = $_POST['fin_plage_horaire'];

			$garantie = $_POST['garantie'];

			$date_achat = $_POST['date_achat'];

			$urgence = $_POST['urgence'];

			$intervention_sur_site = $_POST['intervention_sur_site'];

			$devis = $_POST['devis'];

			$marque = $_POST['marque'];

			$type = $_POST['type'];

			$numserie = $_POST['numserie'];

			$descriptif = $_POST['descriptif'];



			$insertpresta = $bdd->prepare("INSERT INTO intervention(nom_societe, nom_contact, adresse, tel, fax, portable, date_intervention, debut_plage_horaire, fin_plage_horaire, garantie, date_achat, urgence, intervention_sur_site, devis, marque, type, numserie, descriptif) values (:nom_societe, :nom_contact, :adresse, :tel, :fax, :portable, :date_intervention, :debut_plage_horaire, :fin_plage_horaire, :garantie, :date_achat, :urgence, :intervention_sur_site, :devis, :marque, :type, :numserie, :descriptif) ");

			$insertpresta -> execute(array(

				'nom_societe' => $nom_societe, 

				'nom_contact'  =>$nom_contact, 

				'adresse' => $adresse, 

				'tel' => $tel, 

				'fax' => $fax, 

				'portable' => $portable,

				'date_intervention' => $date_intervention, 

				'debut_plage_horaire' => $debut_plage_horaire, 

				'fin_plage_horaire' => $fin_plage_horaire, 

				'garantie' => $garantie, 

				'date_achat' => $date_achat,

				'urgence' => $urgence,

				'intervention_sur_site' => $intervention_sur_site,

				'devis' => $devis,

				'marque' => $marque,

				'type' => $type,

				'numserie' => $numserie,

				'descriptif' => $descriptif,

			));

			$destinataire = 'contact.physic@laposte.net';

			$message =  "Nouvelle intervention requise. Veuillez regarder sur le site dans l'espace de gestion des interventions.";


			$subject = "Nouvelle intervention !";


			if(mail($destinataire, $subject, $message))
			{
				echo '<script languag="javascript" >alert("Votre message a bien été envoyé ");</script>';
			}
			else // Non envoyé
			{
				echo '<script languag="javascript">alert("Votre message n\'a pas pu être envoyé");</script>';
			}



		}



	}





	?>

<html lang="fr">
<head>
	<meta charset="utf-8"/>
	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>
	<link rel="icon" type="image/png" href="favicon.png" />
	<link type="text/css" rel="stylesheet" href="../css/style.css" />
	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>
	<link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css" />
</head>
	<body>
	<!--Entete-->
		<?php include("../include/page_up.php"); ?>
		<center> <h1>Demande d'intervention</h1>

		<b>  Le prix de l'intervention vous sera envoyé par mail. </center> </b>
		<form action="" method="POST">
			<!--Definition premier tableau-->
			<center>
				
				<table class="table_inter">
					

					<!--Nom de la societe-->
					<tr>
						<td>Nom de la société*: <input type="text" name="nom_societe"/></td>
					</tr>

					<!--Nom de la personne à contacter-->
					<tr>
						<td>Nom de la personne à contacter*: <input type="text" name="nom_contact"></td>
					</tr>

					<!--Adresse-->
					<tr>
						<td>Adresse*: </td>
					</tr>

					<tr>
						<td> <textarea id="adresse" name="adresse"> </textarea> </td>
					</tr>

					<!--Tél-->
					<tr>
						<td>Tél*: <input type="text" name="tel"></td>
					</tr>

					<!--Fax-->
					<tr>
						<td>Fax: <input type="text" name="fax"></td>
					</tr>

					<!--Portable-->
					<tr>
						<td>Portable: <input type="text" name="portable"></td>
					</tr>

					<!--Date d'intervention-->
					<tr>
						<td>Date d'intervention: <b>(format: JJ/MM/AAAA)</b> </td>
					</tr>

					<tr>
						<td> <input type="date" name="date_intervention"></td>
					</tr>

					<!--Plage Horaire-->
					<tr>
						<td>Plage Horaire: <b>(format: HH/MM)</b> </td>
					</tr>

					<tr>
						<td>de <input type="time" name="debut_plage_horaire"> à <input type="time" name="fin_plage_horaire"></td>
					</tr>

					<!--Materiel sous garantie-->
					<tr>

						<td>Matériel sous garantie*: </td>
					</tr>

					<tr>
						<td>

							<input type="radio" name="garantie" value="1" id="garantie"/> <label for="garantie"> Matériel sous garantie </label> 

							<input type="radio" name="garantie" value="0" id="aucune_garantie"/> <label for="aucune_garantie"> Matériel plus sous garantie </label> 
						</td>
					</tr>
							


					<!--Date d'achat-->
					<tr>
						<td>Date d'achat: <b>(format:JJ/MM/AAAA)</b> </td>
					</tr>

					<tr>
						<td> <input type="date" name="date_achat"> </td>
					</tr>
					


					<tr>
						<td>Urgence de l'intervention*: </td>
					</tr>

					<tr>	

						<td>

							<input type="radio" id="4" name="urgence" class="inputbasic" /> <label for="4"> Sous 4 heures </label>

							<input type="radio" id="8" name="urgence" class="inputbasic" /> <label for="8"> Sous 8 heures </label>             

							<input type="radio" id="indeterminee" name="urgence" class="inputbasic" /> <label for="indeterminee"> Indéterminée </label>             

							<input type="radio" id="constructeur" name="urgence" class="inputbasic" /> <label for="constructeur"> En fonction du constructeur </label>
						</td>
					</tr>
					

						

					<!--Lieu de l'intervention-->
					<tr>
						<td>Lieu de l'intervention*: </td>
					</tr>

					<tr>

						<td>

								<input type="radio" name="intervention_sur_site" id="intervention_sur_site" value="1"/> <label for="intervention_sur_site"> Intervention sur site </label>

								<input type="radio" name="intervention_sur_site" id="intervention_ailleurs" value="0"/> <label for="intervention_ailleurs"> Intervention en dehors du site </label>

						</td>

					</tr>

					<!--Devis préalable-->

					<tr>
						<td>Devis préalable*: </td>
					</tr>

					<tr>

						<td>

							<input type="radio" name="devis"  value="1" id="devis" /> <label for="devis"> Devis préalable</label>

							<input type="radio" name="devis"  id="aucun_devis" value="0"/> <label for="aucun_devis"> Aucun devis préalable</label>

						</td>

					</tr>


					<tr>
						<td>Marque:* <input type="text" name="marque"></td>
					</tr>

					<tr>
						<td>Type:* <input type="text" name="type"></td>
					</tr>

					<tr>
						<td>N° de Série:* <input type="text" name="numserie"></td>
					</tr>

					<!--Description de la panne:*-->
					<tr>
						<td><b>Descriptif de la panne:* </b></td>
					</tr>

				<tr>

					<td><textarea id="descriptifpanne" name="descriptif" cols="60" rows="10"></textarea></td>

				</tr>


				
				<tr>
					<td> 
						<center> <input name="demandeintervention" value="Demande d'intervention" type="submit"> </center>
					</td>
				</tr>
		</table>
			</center>
	</form>

	<!--Zone du footer-->
	<?php include("../include/footer.php"); ?>
</body>
</html>
