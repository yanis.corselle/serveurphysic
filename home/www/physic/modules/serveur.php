﻿<html lang="fr">

<head>

	<meta charset="utf-8"/>

	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>

	<link rel="icon" type="image/png" href="favicon.png" />

	<link type="text/css" rel="stylesheet" href="../css/style.css" />

	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>

	<link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css" />

</head>

	<body>

	<!--Entete-->

		<?php include("../include/page_up.php"); ?>



	



	<!--Zone Principale-->

		<div class="zone4">

			<center><p>

			La baie de brassage est considédée comme le "système nerveux" de votre installation.



Elle abrite vos élèments sensibles de communication ou de sauvegarde (serveur,switch,standard téléphonique...) et c'est vers elle que convergent toutes les informations de l'entreprise telles que le voix ou les données de votre réseau informatique.



Elle permet le repérage et le brassage des prises réseaux installées. Chaque employé peut ainsi accéder à Internet et/ou l'Intranet de la société. Selon la capacité, certaines permettent d'intégrer le coeur du réseau ainsi que le ou les serveurs.

			<center><img src="../img/reseaux.png"></center>

			<center><img src="../img/serv1.jpg" height="250px"></center>

				</center></p>

		</div><br>



		<!--Zone de footer-->

			<?php include("../include/footer.php"); ?>

</body>

</html>

