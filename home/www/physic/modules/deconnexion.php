﻿<!--Déconnexion-->
<?php
	session_start();
	
	try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

	$_SESSION = array();
	session_destroy();
	header("Location:../modules/seconnecter.php");
?>