<br />
<center>
    <fieldset class="champ-bleu" style="width:550;">
        <legend><font size="2" color="#3991B5"><b>&nbsp;&nbsp;Ajout d'un article dans le catalogue&nbsp;&nbsp;</b></font></legend>
        <br />
        <form name="form" method="post" action="espaceclients.php?menu=catalogue&amp;partie=ajout&article=nouveau">
            <table border="0" cellpadding="5" cellspacing="0">
                <tbody>
                <tr>
                    <td class="txt-gras" width="150px"> R�f�rence * :</td>
                    <td><input name="reference" class="champ-bleu" size="30" type="text" value="<?php if (isset($reference)) { echo "$reference"; } ?>"></td>
                    <td class="txt-gras" width="20"><small>&nbsp;</small></td>
                </tr>
                <tr>
                    <td class="txt-gras"> D�signation  * :</td>
                    <td><textarea name="description" rows="10" cols="21" style="width: 194px; height: 166px; resize:none"><?php if (isset($description)) { echo "$description"; } ?></textarea></td>
                    <td class="txt-gras" width="20"><small>&nbsp;</small></td>
                </tr>
                <tr>
                    <td class="txt-gras"> Marque * :</td>
                    <td>
                        <?php if(isset($_GET['newmarque'])){ ?>
                            <input name="marque" class="champ-bleu" size="30" type="text" value="<?php if (isset($marque)) { echo "$marque"; } ?>">
                            <a href="espaceclients.php?menu=catalogue&partie=ajout&article=nouveau"> <img src="../img/croix.jpg" title="R�initialise le formulaire"> </a></td>
                            <td class="txt-gras" width="20"><small>&nbsp;</small></td>
                        <?php
                        }
                        else
                        {?>
                        <select style="width:188px" name="marque" id="marque">
                            <?php
                            $query="SELECT DISTINCT marque FROM catalogue ORDER BY marque ASC";
                            $res = mysql_query($query) or die(mysql_error());
                            while($data = mysql_fetch_array($res)) {
                                if(isset($_POST['marque']) && $_POST['marque']==$data['marque'])
                                {
                                    echo '<option value="'.$data['marque'].'" selected="selected">'.$data['marque'].'</option>';
                                }
                                else
                                {
                                    echo '<option value="'.$data['marque'].'">'.$data['marque'].'</option>';
                                }
                            }
                        ?>
                        </select>
                        <a href="espaceclients.php?menu=catalogue&partie=ajout&article=nouveau&newmarque"> <img src="../img/plus.jpg" title="R�initialise le formulaire"> </a>
                        <?php
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="txt-gras"> Cat�gorie * : </td>
                    <td class="txt-gras">
                    <select style="width:188px" name="categorie" id="select_categories">
                        <?php
                        include('categories.php');
                        if(isset($_POST['categorie']))
                        {
                            Categories($_POST['categorie']);
                        }
                        else
                        {
                            CategoriesSansSelection();
                        }
                        ?>
                    </select>
                    </td>
                </tr>
                <?php
                if(isset($categorie, $souscategorie) && $souscategorie!=null && $categorie=="Portable")
                {
                    ?>
                    <tr id="sous_categories">
                        <td class="txt-gras">
                            Sous Cat�gorie :
                        </td>
                        <td>
                            <select style="width:188px" name="souscategorie" id="select_sous_categories">
                                <?php
                                SousCategoriePortable($souscategorie);
                                ?>
                            </select>
                        </td>
                    </tr>
                <?php
                }
                elseif(isset($categorie, $souscategorie) && $souscategorie!=null && $categorie=="PC")
                {
                    ?>
                    <tr id="sous_categories">
                        <td class="txt-gras">
                            Sous Cat�gorie :
                        </td>
                        <td>
                            <select style="width:188px" name="souscategorie" id="select_sous_categories">
                                <?php
                                SousCategoriePC($souscategorie);
                                ?>
                            </select>
                        </td>
                    </tr>
                <?php
                }
                else
                {
                    ?>
                    <tr id="sous_categories" style="display: none;">
                        <td class="txt-gras">
                            Sous Cat�gorie :
                        </td>
                        <td class="txt-gras">
                            <select style="width:188px" name="souscategorie" id="select_sous_categories">
                            </select>
                        </td>
                    </tr>
                <?php
                }
                ?>
                <tr>
                    <td class="txt-gras"> Prix ** :</td>
                    <td><input name="prix" class="champ-bleu" size="30" type="text" value="<?php if (isset($prix)) { echo "$prix"; } ?>"></td>
                    <td class="txt-gras" width="20"><small>&nbsp;</small></td>
                </tr>
                <tr>
                    <td class="txt-gras"> Quantit� * :</td>
                    <td><input name="quantite" class="champ-bleu" size="30" type="text" value="<?php if (isset($quantite)) { echo "$quantite"; } ?>"></td>
                    <td class="txt-gras" width="20"><small>&nbsp;</small></td>
                </tr>
                <tr>
                <tr>
                    <td class="txt-gras"> Remise * :</td>
                    <td><input name="remise" class="champ-bleu" size="30" type="text" value="<?php if (isset($remise)) { echo "$remise"; } ?>"> 
                    %</td>
                    <td class="txt-gras" width="20"><small>&nbsp;</small></td>
                </tr>
                <tr>
                    <td colspan="4" class="txt-gras" align="center" height="13" valign="middle"><br />
                        <input name="Submit" value="Enregistrer" type="submit" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="Reset" value="Effacer" type="reset" />
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
        * Les champs marqu�s d'un ast�risque sont obligatoires. <br />
        ** Les prix pour les produits Apple sont les prix publics (Apple Store), pour les autres produits ce sont les prix d'achat.
    </fieldset>
</center>
<br />