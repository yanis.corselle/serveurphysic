<?php
	session_start();

 ?>
<!DOCTYPE HTML>

<html lang="fr">
<head>
	<meta charset="utf-8"/>
	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>
	<link rel="icon" type="image/png" href="favicon.png" />
	<link type="text/css" rel="stylesheet" href="../css/style.css" />
	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>
	<link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css" />

</head>
	<body>

		<!--Entête + boutons-->
		<?php
			include("../include/page_up.php");
		?>

	<!--Ajout à la BDD-->
	<?php
		if (isset($_POST['mdpadmin']) AND $_POST['mdpadmin'] ==  "PhYsic")
		{
			header("Location: ../modules/administration.php");
		}else
		{
			echo '<br><font color="red">Mot de passe incorrect !</font>';
		}
	?>
		<div class="zone4">
		<center>
		<form action="" enctype="multipart/form-data" method="POST" class="styleconnexion">
			<input type="text" name="mdpadmin" class="inputbasic" placeholder="Mot de passe">
				<br>
			<input type="submit" name="connexion" value="Se connecter">
		</form>
		</center>
		</div>

	<!--Zone du footer-->
		<?php include("../include/footer.php"); ?>
</body>
</html>
