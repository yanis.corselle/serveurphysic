<?php
//extract($_POST);
extract($_GET);

    if(isset($fournisseur)&& $fournisseur=="techdata"){ ?>
    <br />
    <center>
        <fieldset class="champ-bleu" style="width:550;">
            <legend><font size="2" color="#3991B5"><b>&nbsp;&nbsp;Ajout d'une feuille excel de chez TechData&nbsp;&nbsp;</b></font></legend>
            <br />
            <form enctype="multipart/form-data"  name="form" method="post" action="espaceclients.php?menu=catalogue&amp;partie=ajout&amp;fournisseur=techdata">
                <table border="0" cellpadding="5" cellspacing="0">
                    <tbody>
                    <tr>
                        <td class="txt-gras"> Catégorie du fichier :</td>
                        <td class="txt-gras">
                            <select style="width:206px" name="techdatacategorie" id="select_categories">
                                <?php
                                include('categories.php');
                                if(isset($_POST['techdatacategorie']))
                                {
                                    Categories($_POST['techdatacategorie']);
                                }
                                else
                                {
                                    CategoriesSansSelection();
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <?php
                    if(isset($techdatacategorie, $techdatasouscategorie) && $techdatasouscategorie!=null && $techdatacategorie=="Portable")
                    {
                        ?>
                        <tr id="sous_categories">
                            <td class="txt-gras">
                                Sous Catégorie :
                            </td>
                            <td>
                                <select style="width:206px" name="techdatasouscategorie" id="select_sous_categories">
                                    <?php
                                    SousCategoriePortable($techdatasouscategorie);
                                    ?>
                                </select>
                            </td>
                        </tr>
                    <?php
                    }
                    elseif(isset($techdatacategorie, $techdatasouscategorie) && $techdatasouscategorie!=null && $techdatacategorie=="PC")
                    {
                        ?>
                        <tr id="sous_categories">
                            <td class="txt-gras">
                                Sous Catégorie :
                            </td>
                            <td>
                                <select style="width:206px" name="techdatasouscategorie" id="select_sous_categories">
                                    <?php
                                    SousCategoriePC($techdatasouscategorie);
                                    ?>
                                </select>
                            </td>
                        </tr>
                    <?php
                    }
                    else
                    {
                        ?>
                        <tr id="sous_categories" style="display: none;">
                            <td class="txt-gras">
                                Sous Catégorie :
                            </td>
                            <td class="txt-gras">
                                <select style="width:206px" name="techdatasouscategorie" id="select_sous_categories">
                                </select>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                    <tr>
                        <td class="txt-gras"> Fichier Excel a Upload : </td>
                        <td class="txt-gras"> <input name="techdataexcel" id="techdataexcel" type="file" style="width:200px; font-size:10px;" /> </td>
                    </tr>
                    <tr>
                    <td colspan="4" class="txt-gras" align="center" height="13" valign="middle"><br />
                            <input name="Submit" value="Enregistrer" type="submit" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </fieldset>
    </center>
    <br />
    <?php
    }
    if(isset($fournisseur)&& $fournisseur=="also"){ ?>
        <br />
        <center>
            <fieldset class="champ-bleu" style="width:550;">
                <legend><font size="2" color="#3991B5"><b>&nbsp;&nbsp;Ajout d'une feuille excel de chez ALSO&nbsp;&nbsp;</b></font></legend>
                <br />
                <form enctype="multipart/form-data"  name="form" method="post" action="espaceclients.php?menu=catalogue&amp;partie=ajout&amp;fournisseur=also">
                    <table border="0" cellpadding="5" cellspacing="0">
                        <tbody>
                        <tr>
                            <td class="txt-gras"> Catégorie du fichier :</td>
                            <td class="txt-gras">
                                <select style="width:206px" name="alsocategorie" id="select_categories">
                                    <?php
                                    include('categories.php');
                                    if(isset($_POST['alsocategorie']))
                                    {
                                        Categories($_POST['alsocategorie']);
                                    }
                                    else
                                    {
                                        CategoriesSansSelection();
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <?php
                        if(isset($alsocategorie, $alsosouscategorie) && $alsosouscategorie!=null && $alsocategorie=="Portable")
                        {
                            ?>
                            <tr id="sous_categories">
                                <td class="txt-gras">
                                    Sous Catégorie :
                                </td>
                                <td>
                                    <select style="width:206px" name="alsosouscategorie" id="select_sous_categories">
                                        <?php
                                        SousCategoriePortable($alsosouscategorie);
                                        ?>
                                    </select>
                                </td>
                            </tr>
                        <?php
                        }
                        elseif(isset($alsocategorie, $alsosouscategorie) && $alsosouscategorie!=null && $alsocategorie=="PC")
                        {
                            ?>
                            <tr id="sous_categories">
                                <td class="txt-gras">
                                    Sous Catégorie :
                                </td>
                                <td>
                                    <select style="width:206px" name="alsosouscategorie" id="select_sous_categories">
                                        <?php
                                        SousCategoriePC($alsosouscategorie);
                                        ?>
                                    </select>
                                </td>
                            </tr>
                        <?php
                        }
                        else
                        {
                            ?>
                            <tr id="sous_categories" style="display: none;">
                                <td class="txt-gras">
                                    Sous Catégorie :
                                </td>
                                <td class="txt-gras">
                                    <select style="width:206px" name="alsosouscategorie" id="select_sous_categories">
                                    </select>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                        <tr>
                            <td class="txt-gras"> Fichier Excel a Upload : </td>
                            <td class="txt-gras"> <input name="alsoexcel" id="alsoexcel" type="file" style="width:200px; font-size:10px;" /> </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="txt-gras" align="center" height="13" valign="middle"><br />
                                <input name="Submit" value="Enregistrer" type="submit" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </fieldset>
        </center>
        <br />
    <?php
    }
    if(isset($fournisseur)&& $fournisseur=="ingram"){ ?>
        <br />
        <center>
            <fieldset class="champ-bleu" style="width:550;">
                <legend><font size="2" color="#3991B5"><b>&nbsp;&nbsp;Ajout d'une feuille excel de chez Ingram&nbsp;&nbsp;</b></font></legend>
                <br />
                <form enctype="multipart/form-data"  name="form" method="post" action="espaceclients.php?menu=catalogue&amp;partie=ajout&amp;fournisseur=ingram">
                    <table border="0" cellpadding="5" cellspacing="0">
                        <tbody>
                        <tr>
                            <td class="txt-gras"> Catégorie du fichier :</td>
                            <td class="txt-gras">
                                <select style="width:206px" name="ingramcategorie" id="select_categories">
                                    <?php
                                    include('categories.php');
                                    if(isset($_POST['ingramcategorie']))
                                    {
                                        Categories($_POST['ingramcategorie']);
                                    }
                                    else
                                    {
                                        CategoriesSansSelection();
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <?php
                        if(isset($ingramcategorie, $ingramsouscategorie) && $ingramsouscategorie!=null && $ingramcategorie=="Portable")
                        {
                            ?>
                            <tr id="sous_categories">
                                <td class="txt-gras">
                                    Sous Catégorie :
                                </td>
                                <td>
                                    <select style="width:206px" name="ingramsouscategorie" id="select_sous_categories">
                                        <?php
                                        SousCategoriePortable($ingramsouscategorie);
                                        ?>
                                    </select>
                                </td>
                            </tr>
                        <?php
                        }
                        elseif(isset($ingramcategorie, $ingramsouscategorie) && $ingramsouscategorie!=null && $ingramcategorie=="PC")
                        {
                            ?>
                            <tr id="sous_categories">
                                <td class="txt-gras">
                                    Sous Catégorie :
                                </td>
                                <td>
                                    <select style="width:206px" name="ingramsouscategorie" id="select_sous_categories">
                                        <?php
                                        SousCategoriePC($ingramsouscategorie);
                                        ?>
                                    </select>
                                </td>
                            </tr>
                        <?php
                        }
                        else
                        {
                            ?>
                            <tr id="sous_categories" style="display: none;">
                                <td class="txt-gras">
                                    Sous Catégorie :
                                </td>
                                <td class="txt-gras">
                                    <select style="width:206px" name="ingramsouscategorie" id="select_sous_categories">
                                    </select>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                        <tr>
                            <td class="txt-gras"> Fichier Excel a Upload : </td>
                            <td class="txt-gras"> <input name="ingramexcel" id="ingramexcel" type="file" style="width:200px; font-size:10px;" /> </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="txt-gras" align="center" height="13" valign="middle"><br />
                                <input name="Submit" value="Enregistrer" type="submit" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </fieldset>
        </center>
        <br />
    <?php
    }
    if(isset($fournisseur)&& $fournisseur=="physic"){ ?>
        <br />
        <center>
            <fieldset class="champ-bleu" style="width:550;">
                <legend><font size="2" color="#3991B5"><b>&nbsp;&nbsp;Ajout d'une feuille excel de chez Physic&nbsp;&nbsp;</b></font></legend>
                <br />
                <form enctype="multipart/form-data"  name="form" method="post" action="espaceclients.php?menu=catalogue&amp;partie=ajout&amp;fournisseur=physic">
                    <table border="0" cellpadding="5" cellspacing="0">
                        <tbody>
                        <tr>
                            <td class="txt-gras"> Catégorie du fichier :</td>
                            <td class="txt-gras">
                                <select style="width:206px" name="physiccategorie" id="select_categories">
                                    <?php
                                    include('categories.php');
                                    if(isset($_POST['physiccategorie']))
                                    {
                                        Categories($_POST['physiccategorie']);
                                    }
                                    else
                                    {
                                        CategoriesSansSelection();
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <?php
                        if(isset($physiccategorie, $physicsouscategorie) && $physicsouscategorie!=null && $physiccategorie=="Portable")
                        {
                            ?>
                            <tr id="sous_categories">
                                <td class="txt-gras">
                                    Sous Catégorie :
                                </td>
                                <td>
                                    <select style="width:206px" name="physicsouscategorie" id="select_sous_categories">
                                        <?php
                                        SousCategoriePortable($physicsouscategorie);
                                        ?>
                                    </select>
                                </td>
                            </tr>
                        <?php
                        }
                        elseif(isset($physiccategorie, $physicsouscategorie) && $physicsouscategorie!=null && $physiccategorie=="PC")
                        {
                            ?>
                            <tr id="sous_categories">
                                <td class="txt-gras">
                                    Sous Catégorie :
                                </td>
                                <td>
                                    <select style="width:206px" name="physicsouscategorie" id="select_sous_categories">
                                        <?php
                                        SousCategoriePC($physicsouscategorie);
                                        ?>
                                    </select>
                                </td>
                            </tr>
                        <?php
                        }
                        else
                        {
                            ?>
                            <tr id="sous_categories" style="display: none;">
                                <td class="txt-gras">
                                    Sous Catégorie :
                                </td>
                                <td class="txt-gras">
                                    <select style="width:206px" name="physicsouscategorie" id="select_sous_categories">
                                    </select>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                            <td class="txt-gras"> Fichier Excel a Upload : </td>
                            <td class="txt-gras"> <input name="physicexcel" id="physicexcel" type="file" style="width:200px; font-size:10px;" /> </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="txt-gras" align="center" height="13" valign="middle"><br />
                                <input name="Submit" value="Enregistrer" type="submit" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </fieldset>
        </center>
        <br />
    <?php
    }
    ?>