﻿<html lang="fr">

<head>

	<meta charset="utf-8"/>

	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>

	<link rel="icon" type="image/png" href="favicon.png" />

	<link type="text/css" rel="stylesheet" href="../css/style.css" />

	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>

	<link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css" />



</head>

	<body>

	<!--Entete-->

		<?php include("../include/page_up.php"); ?>

<div class="page">


	<!--Zone centrale-->

		<div class="zone4">

			<center><p>

			Depuis plus de 10 ans, nous fournissons à nos clients les marques les plus fiables et les plus reconnues.

				<br/>

			Nous étudions avec vous toutes les possibilités techniques afin de vous proposer la solution s'adaptant à vos besoins.

				<br/>

			Les ordinateurs et serveurs que nous vous proposons issue des catalogues constructeurs, sont testés et validés, et peuvent être adaptés sur mesure.

				<br/>

			Spécialistes Apple, nous vous proposons toute la gamme du constructeur à la pomme

				<br/>

			(iMac, Macbook (Pro), Xserve, accessoires...).

				<br/>

			Comme un bon matériel ne va pas sans l'assistance et le service adéquat, nous proposons à nos clients divers

				<br/>

			types de contrats leur permettant ainsi d'oublier tous les soucis inhérents aux nouvelles technologies:

				<br/>

				<br/>

			<b><a href="../modules/services.php"><u>PhYsic :</u> garantie et qualité du service.</a></b>

			</center></p>

		</div>



		<!--Zone du footer-->

		<?php include("../include/footer.php"); ?>

</body>

</html>

