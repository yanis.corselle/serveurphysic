<?php
// classe d'upload vite fait par nOOr
class Upload {

  private $uploadedFileName ;
  private $uploadedFile ;
  private $uploadedFileSize ;
  private $uploadedFileType ;

  public function __construct(){ // constructeur
    // je pr�f�re d�finir mes param�tres d'upload dans la m�thode doUpload()
    // comme �a je peux construire mon objet Upload() avant m�me de recevoir un fichier d'un formulaire
    // et balancer l'upload quand je veux avec cette m�thode
  }
  
  // M�thode doUpload() 
  // 1er param�tre : nom du champs file du formulaire ;
  // 2�me : chemin de destination sur le serveur ;
  // 3�me : nouveau nom de fichier SANS EXTENSION car on prend celle du fichier d'origine (facultatif) ;
  // 4�me : c�t� maxi de l'image (90px par d�faut) ;
  // 5�me : type attendu (d�fault : n'importe quel type) ;
  public function doUpload($file_form_name,$uploadPath=".",$newname="",$sizemax=90,$constraint=""){
    // si l'on a pas donn� un nouveau nom au fichier, il garde le nom d'origine
    if ($newname=="") {
      $this->uploadedFileName = $_FILES[$file_form_name]["name"] ;
    } else {
      $extent = pathinfo($_FILES[$file_form_name]["name"]) ;
      $extent = $extent["extension"] ;
      $this->uploadedFileName = $newname.".$extent" ;
    }
    
    // r�cup�ration du nom temporaire sur le serveur, de la taille du fichier et de son type
    $this->uploadedFile = $_FILES[$file_form_name]["tmp_name"];
    $this->uploadedFileSize = $_FILES[$file_form_name]["size"];
    $this->uploadedFileType = $_FILES[$file_form_name]["type"];
    //on commence par verifier que le dossier d'upload existe
    if (file_exists($uploadPath)) {
      //on verifie que le fichier soit bien uploader pour des questions de securite
      if (is_uploaded_file($this->uploadedFile)) {
        if (!file_exists($uploadPath.$this->uploadedFileName)) { // on v�rifie que le fichier n'existe pas d�ja (sinon, on lui concat�ne un 1)
          //on appelle la fonction d'upload
          $this->uploadFile($this->uploadedFile, $this->uploadedFileName, $uploadPath, $newname, $sizemax, $this->uploadedFileType, $constraint);
          //pour activer la modification des permissions enlever les deux // devant la ligne suivante
          //chmod ("$uploadPath$uploadedFileName", $permission);
        }else{
          $this->uploadedFileName="1".$this->uploadedFileName;
          $this->uploadFile($this->uploadedFile, $this->uploadedFileName, $uploadPath, $newname, $sizemax, $this->uploadedFileType, $constraint);
      }
    }
    }else{
      print "Erreur: le dossier $uploadPath n'existe pas";
    }
  }
  
  //fonction pour l'upload des fichier, on utilise la fonction copy et on modifie les noms
  private function uploadFile($file, $name, $directory, $newname, $sizemax, $type,$constraint) {
    copy($file, $directory.$name) or die ("Impossible d'uploader le fichier");
    
    $fullpath = $directory.$this->uploadedFileName ; // chemin complet de l'image sur le serveur

    // Si c'est un fichier image, on v�rifie que c'est bien un fichier image (type MIME)

    if ($constraint=="image") {
      if ($type!="image/jpeg" && $type!="image/png" && $type!="image/gif") {
        @unlink($fullpath);
        echo '<script>alert("Ce n\'est pas un fichier image !");</script>' ;
      }
    }

    // redimensionnement si c'est un thumb (avatar par exemple)
    
    if ($constraint=="thumb") {
      switch($type){
      case "image/jpeg":
      $function_image_create = "ImageCreateFromJpeg";
      $function_image_new = "ImageJpeg";
      break;
      case "image/png":
      $function_image_create = "ImageCreateFromPng";
      $function_image_new = "ImagePNG";
      break;
      case "image/gif":
      $function_image_create = "ImageCreateFromGif";
      $function_image_new = "ImageGif";
      break;
      default:
      @unlink($fullpath);
      if ($newname=="avatar") {
        echo '<script>alert("Votre avatar n\'est pas un fichier image, vous aurez donc l\'avatar par d�faut !\n Vous pouvez toutefois le modifier dans votre profil.");</script>' ;
      } else {
        echo '<script>alert("Ce n\'est pas un fichier image !");</script>' ;
      }
      exit;
      break;
      }
      
      list($width, $height) = getimagesize($fullpath); // on r�cup�re les dimensions de l'image
      
      // si l'un des c�t� est sup�rieur � $sizemax, on redimensionne en conservant les proportions
      if ($width>$sizemax || $height>$sizemax) {
              
              $ratio = $height/$width;
              $newheight = ($height > $width) ? $sizemax : $sizemax*$ratio;
              $newwidth = $newheight/$ratio;
                                              
              $thumb = ImageCreateTrueColor($newwidth,$newheight);
              $source = @$function_image_create($fullpath);
              
              ImageCopyResampled($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
              
              @$function_image_new($thumb,$fullpath);
      }
    }
  }
  
  public function getName() {
          return $this->uploadedFileName;
  }

}// fin de la classe


?>