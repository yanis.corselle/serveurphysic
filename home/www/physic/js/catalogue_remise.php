<?php

try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

//include("RequeteAJAX.php");

?>

<html lang="fr">

<head>

	<meta charset="utf-8"/>

	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>

	<link rel="icon" type="image/png" href="../../img/favicon.png" />

	<link type="text/css" rel="stylesheet" href="../../css/style.css" />

	<link type="image/jpg" rel="icon" href="../../img/favicon.jpg"/>

	<link rel="stylesheet" href="../../css/jquery.mCustomScrollbar.css" />

	<script type="text/javascript" src="../../js/jquery.js"></script>

</head>

	<body>

	<!--Entete-->

		<?php include("../../include/page_upcatalogue.php"); ?>



	<!--Image logo-->



	<!--Zone du tableau-->

	<div class="catalogue">

		<center>

	        <?php

	             try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

				 $sql = "SELECT * FROM catalogue";

				 $stmt = $bdd->query($sql);





	        ?>



	        <table class="tablecatalogue" border="1px">

	            <tr id="tablecata">

						<th id="image">Image</th>

	                    <th id="reference">Reference</th>

	                    <th id="marque">Marque</th>

	                    <th id="designation">Designation</th>

	                    <th id="prix_remise">Prix remisé</th>

						<th id="ajout_panier">Ajouter au panier</th>

	            </tr>



				<!--Ici on récupère dans la variable $row, le fetch de la requète mise en array dans la variable $req

				//On passe d'un tableau à un ensemble de variable :) -->

				<?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)) : 

					if($_SESSION['remise'] != 0 )

					{

						$remise = $_SESSION['remise'];

						$row['Prix'] = $row['Prix'] * (1 - $remise / 100);



					}

					?>

				<tr>
				<?php 
						$id = htmlspecialchars($row['id']);
						if(file_exists("../upload/img_cata/" . $id . ".png"))
						{
							$image = "../upload/img_cata/" . $id . ".png";
						}  

						elseif(file_exists("../upload/img_cata/" . $id . ".jpg"))
						{
							$image = "../upload/img_cata/" . $id . ".jpg";
						}  

						elseif(file_exists("../upload/img_cata/" . $id . ".jpeg"))
						{
							$image = "../upload/img_cata/" . $id . ".jpeg";
						}  

						else{
							$image = "../upload/img_cata/null.png";
						}

						?>

					<td id="image"> <img src=<?php echo $image ?> width = 50px height = 50px /> </td> 
					
					<td id="reference"><?php echo htmlspecialchars($row['Reference']); ?></td>

					<td id="marque"><?php echo htmlspecialchars($row['Marque']); ?></td>

					<td id="designation"><?php echo htmlspecialchars($row['Designation']); ?></td>

					<td id="prix_remise"><?php echo htmlspecialchars($row['Prix']); ?></td>

					<td id="ajout_prix"><?php echo ("<a href=\"panier.php?action=ajout&amp;l=" . $row['Designation'] . "&amp;q=1&amp;p=" . $row['Prix'] . "&amp;r=" . $row['Reference'] ." \"onclick=\"window.open(this.href, '';\">Ajouter au panier</a>"); ?></td>



				</tr>



				<?php endwhile; ?>



				<form action ='catalogue.php' method = 'POST'><input type="submit" id="boutonremise" value="Revoir mon catalogue non remisé" class="inputbasic" name="catalogueremise"> </form>

	    </table>

		</center>

	</div>



							<form method="post" action="impression.php">

								<input type="submit" id="boutonimpression" value="Imprimer le catalogue" class="inputbasic" name="Imprimer">

							</form>

	



		<!--Zone du footer-->

		<?php include("../../include/footercatalogue.php"); ?>

		<!--<script type="text/javascript" src="../../js/Remise.js" charset="utf-8"></script>-->

</body>

</html>

