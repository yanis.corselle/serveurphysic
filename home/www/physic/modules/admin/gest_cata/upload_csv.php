<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set("display_startup_errors", 1);
try 
{ 
    $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); 
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $bdd->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    
    $sql = "SELECT * FROM catalogue";
    $stmt = $bdd->query($sql);

    if($stmt === false)
    {
        die("Erreur");
    }

} 

catch (Exception $e) 
{ 
    die('Erreur : ' . $e->getMessage()); 
}
	

			
    ?>
    
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="fr-FR">
<head>
	<meta http-equiv="Content-Type" content="text/html" charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>
	<link rel="icon" type="image/png" href="../../../img/favicon.png"/>
	<link type="text/css" rel="stylesheet" href="../../../css/style.css"/>
	<link type="image/jpg" rel="icon" href="../../../img/favicon.jpg"/>
	<link rel="stylesheet" href="../../../css/jquery.mCustomScrollbar.css"/>
</head>
	<body>
	<!--Entete-->
    <?php include("../../../include/page_upacc.php"); 
        
// Vérifier si le formulaire a été soumis
if($_SERVER["REQUEST_METHOD"] == "POST")
{
    // Vérifie si le fichier a été uploadé sans erreur.
    if(isset($_FILES["csv"]) && $_FILES["csv"]["error"] == 0)
    {
        $allowed = array( "csv" => "file/csv");
        $filename = $_FILES["csv"]["name"];
        $filetype = $_FILES["csv"]["type"];
        $filesize = $_FILES["csv"]["size"];

        // Vérifie l'extension du fichier
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die("Erreur : Veuillez sélectionner un format de fichier valide.");

        // Vérifie la taille du fichier - 5Mo maximum
        $maxsize = 5 * 1024 * 1024;
        if($filesize > $maxsize) die("Error: La taille du fichier est supérieure à la limite autorisée.");

            // Vérifie si le fichier existe avant de le télécharger.
            if(file_exists("upload/" . $_FILES["csv"]["name"]))
            {
                echo $_FILES["csv"]["name"] . " existe déjà.";
            }
             
            else
            {
                move_uploaded_file($_FILES["csv"]["tmp_name"], "upload/" . $_FILES["csv"]["name"]);
                echo "Votre fichier a été téléchargé avec succès.";
            } 
    } 
    else
    {
        echo "Error: " . $_FILES["csv"]["error"];
    }

	$csvFileToLoad = "upload/" . $_FILES["csv"]["name"]; 

}




$header = true;
$Categorie = $_POST['Categorie'];
$Sous_Categorie = $_POST['Sous_Categorie'];
$file = fopen($csvFileToLoad, "r");
        while (($emapData = fgetcsv($file, 10000, ';')) !== FALSE)
        {
            if($header)
            {
                $header = false;
            }
            else
            {
                $Reference_fournisseur = $emapData[0];
                $Reference_fabricant = $emapData[1];
                $Marque = $emapData[4];
                $Designation = $emapData[3];
                $Prix_PU_HT = floatval($emapData[5]);
                $Quantite = floatval($emapData[9]);

                if($Quantite != 0)
                {
                    if(strpos($Prix_PU_HT, ',') !== FALSE) 
                    {
                        $Prix_PU_HT = str_replace(',', '.', $Prix_PU_HT);
                    }
                    if(strpos($Prix_PU_HT, ' ') !== FALSE) 
                    {
                        $Prix_PU_HT = str_replace(' ', '', $Prix_PU_HT);
                    }
                    

                    $item_Exist = $bdd->prepare("SELECT Reference_fournisseur FROM catalogue WHERE Reference_fournisseur = :Reference_fournisseur");
                    //On recupère les références de la base qui sont égales aux références passées en paramètre
                    $item_Exist->bindValue('Reference_fournisseur', $Reference_fournisseur, PDO::PARAM_STR);
                    $item_Exist->execute();
                    //on exécute la requête
                    
                    $itemINbdd = $item_Exist->rowCount();
                    //Rowcount permet de sortir le nombre de valeurs que la requête renvoie, que l'on rentre dans la variable itemINbdd 
                    
                    if($itemINbdd == 0)
                    //Si la requête renvoi 0, la référence n'existe pas dans la base, sinon la référence existe.
                    {

                        
                        $sql = $bdd->prepare("INSERT INTO catalogue(Reference_fournisseur, Reference_fabricant, Marque, Designation, Prix_PU_HT, Categorie, Sous_Categorie) VALUES (:Reference_fournisseur, :Reference_fabricant, :Marque, :Designation, :Prix_PU_HT, :Categorie, :Sous_Categorie)");
                        $sql -> execute(array(
                            'Reference_fournisseur' => $Reference_fournisseur,
                            'Reference_fabricant' => $Reference_fabricant, 
                            'Marque'  =>$Marque, 
                            'Designation' => $Designation, 
                            'Prix_PU_HT' => $Prix_PU_HT,
                            'Categorie' => $Categorie,
                            'Sous_Categorie' => $Sous_Categorie
                        ));
                    
                            
                        sleep(0.01);

                        
                        
                        
                    }
                }
            }
            
        }
        fclose($file);

        if (@file_exists($file)) {
            unlink($file);
            echo "Le fichier a été correctement supprimé.";
        }

?>

	<!--Zone du footer-->
	<?php include("../../../include/footeracc.php"); ?>
</body>
</html>
