<?php
session_start();
if(isset($_SESSION['attribut']) == FALSE OR $_SESSION['attribut'] == 0)
{
	header("Location:../../profil.php");
}
			try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }


		$requser = $bdd->prepare("SELECT * FROM destockage WHERE id=?");
		$requser->execute(array($_POST['id']));
		$vente = $requser->fetch();
		$id=$_POST['edit'];

		if(isset($_POST['newReference']) AND $_POST['newReference'] != $vente['Reference'])
		{
			$newReference = htmlspecialchars($_POST['newReference']);
			$insertReference = $bdd->prepare("UPDATE destockage SET Reference = ? WHERE id =?");
			$insertpseudo->execute(array($newReference, $_POST['edit']));
		}

		if(isset($_POST['newMarque']) AND $_POST['newMarque'] != $vente['Marque'])
		{
			$newMarque = htmlspecialchars($_POST['newMarque']);
			$insertMarque = $bdd->prepare("UPDATE destockage SET Marque = ? WHERE id =?");
			$insertMarque->execute(array($newMarque, $id));
			
		}

		if(isset($_POST['newDesignation']) AND $_POST['newDesignation'] != $vente['Designation'])
		{
			$newDesignation = htmlspecialchars($_POST['newDesignation']);
			$insertDesignation = $bdd->prepare("UPDATE destockage SET Designation = ? WHERE id =?");
			$insertDesignation->execute(array($newDesignation, $_POST['edit']));
		}

		if(isset($_POST['newPrix']) ) 
		{
			$newPrix = htmlspecialchars($_POST['newPrix']);
			$insertPrix = $bdd->prepare("UPDATE destockage SET Prix = ? WHERE id =?");
			$insertPrix->execute(array($newPrix, $_POST['edit']));
		}


	if(isset($_POST['modif']))
	{
		header('Location: gestion_ven_flash.php');
	}
	


			
		

?>
<html lang="fr">
<head>
	<meta charset="utf-8"/>
	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>
	<link rel="icon" type="image/png" href="favicon.png" />
	<link type="text/css" rel="stylesheet" href="../../../css/style.css" />
	<link type="image/jpg" rel="icon" href="../../../img/favicon.jpg"/>
	<link rel="stylesheet" href="../../../css/jquery.mCustomScrollbar.css" />
</head>
	<body>
	<!--Entete-->
		<?php include("../../../include/page_upacc.php"); ?>

	<!--formulaire d'édition du profil-->

		<div class="zone4">
		<center>
         <h2>Edition du profil</h2>
         <form method="POST" action="" class="formedition">

		 	<input type="text"  name="newReference"  class="inputbasic" value="<?php echo $vente['Reference']; ?>"/></br></br>
			<input type="text" name="newMarque" class="inputbasic" value="<?php echo $vente['Marque']; ?>"/></br></br>
			<input type="text" name="newDesignation" class="inputbasic" value="<?php echo $vente['Designation']; ?>"/></br></br>
			<input type="number" min="0" name="newPrix" class="inputbasic" value="<?php echo $vente['Prix']; ?>"/></br></br>
			<input type="hidden" type='submit' name="modif" value="true">
			<input type="hidden" type='submit' name="edit" value=<?php echo($_POST['edit']) ?>>
			<input type="submit" value="Mettre à jour" name="modifier">
		</form>
		<?php if(isset($msg)) { echo $msg; } ?>
		</center>
		</div>

	<!--Zone du footer-->
	<?php include("../../../include/footeracc.php"); ?>
</body>
<script type="text/javascript" src="../../../js/jquery.min.js"></script>


</html>