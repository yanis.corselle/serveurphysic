<?php
	try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
	?>
<html lang="fr">
<head>
	<meta charset="utf-8"/>
	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>
	<link rel="icon" type="image/png" href="favicon.png" />
	<link type="text/css" rel="stylesheet" href="../css/style.css" />
	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>
	<link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css" />
</head>
	<body>
	<!--Entete-->
		<?php include("../include/page_up.php"); ?>

	<!--Zone centrale-->
		<div class="zone4">
		<?php
				if(empty($_SESSION) == FALSE)
					{
						echo "<center>Bienvenue ".$_SESSION['pseudo']." !</center>";
						echo '<br><center><li><a href="../modules/client/catalogue.php">Mon catalogue</a></li></center>';
						echo '<center><li><a href="../modules/client/panier.php">Mon panier</a></li></center>';
						echo '<center><li><a href="../modules/client/commandes.php">Mes commandes</a></li></center>';
						echo '<center><li><a href="../modules/client/mesinterventions.php">Mes interventions</a></li></center>';
						echo '<center><li><a href="../modules/profil.php">Mon profil</a></li></center><br>';
					}
					else{
						echo '<center>Veuillez vous connecter pour accéder à cet espace.
							<br/> <a href="../modules/seconnecter.php">Me connecter</a></center><br>';
					}

		?>
		</div>

	<!--Zone du footer-->
	<?php include("../include/footer.php"); ?>
</body>
</html>
