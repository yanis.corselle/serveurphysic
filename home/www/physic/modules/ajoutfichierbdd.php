<?php
	try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
	?>

<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="fr-FR">
<head>
	<meta http-equiv="Content-Type" content="text/html" charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>
	<link rel="icon" type="image/png" href="favicon.png"/>
	<link type="text/css" rel="stylesheet" href="../css/style.css"/>
	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>
	<link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css"/>
</head>
	<body>
	<!--Entete-->
		<?php include("../include/page_up.php"); ?>

		<form action="upload_csv.php" method="post" enctype="multipart/form-data">
			<h2>Upload Fichier</h2>
			<label for="fileUpload">Fichier:</label>
			<input type="file" name="csv" id="fileUpload">
			<input type="submit" name="submit" value="Upload">
			<p><strong>Note:</strong> Seuls les formats .csv sont autorisés jusqu'à une taille maximale de 5 Mo.</p>
    	</form>

	<!--Zone du footer-->
	<?php include("../include/footer.php"); ?>
</body>
</html>
