<?php
session_start();
if(isset($_SESSION['attribut']) == FALSE OR $_SESSION['attribut'] == 0)
{
	header("Location:../../profil.php");
}
$host = 'localhost';
$dbname = 'physic';
$username = 'admin';
$password = 'root';
  
$dsn = "mysql:host=$host;dbname=$dbname"; 
// récupérer tous les utilisateurs
$sql = "SELECT * FROM destockage";
 
try{
 $pdo = new PDO($dsn, $username, $password);
 $stmt = $pdo->query($sql);
 
 if($stmt === false){
  die("Erreur");
 }
 
}catch (PDOException $e){
  echo $e->getMessage();
}
//include("RequeteAJAX.php");
?>

<html lang="fr">
<html>
<head>
	<meta charset="utf-8"/>
	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>
	<link rel="icon" type="image/png" href="../../../img/favicon.png" />
	<link type="text/css" rel="stylesheet" href="../../../css/style.css" />
	<link type="image/jpg" rel="icon" href="../../../img/favicon.jpg"/>
	<link rel="stylesheet" href="../../../css/jquery.mCustomScrollbar.css" />
	<script type="text/javascript" src="../../../js/jquery.js"></script>
</head>
	<body>
	<!--Entete-->
		<?php include("../../../include/page_upacc.php"); ?>

	<!--Image logo-->

	<!--Zone du tableau-->
	<div class="gest_cata">
		<form action="" name="gest_cata" method="post" enctype="multipart/form-data" charset="utf-8" accept-charset="utf-8">
		<center>
	        <?php
	             try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
	        ?>

	        <table class="tablecata" name="" border="1px">
	            <tr id="tablecata">
	                    <th id="ID">ID</th>
	                    <th id="Reference">Reference</th>
	                    <th id="Marque">Marque</th>
						<th id="Designation">Designation</th>
						<th id="Prix">Prix</th>
	                    <th id="modifier">Modifier</th>
						<th id="suppr" name="suppr">Supprimer</th>
	            </tr>

				<tbody>
     			<?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)) : ?>
     				<tr>
						<td id="ID"><?php echo htmlspecialchars($row['id']); ?></td>
						<td id="Reference"><?php echo htmlspecialchars($row['Reference']); ?></td>
						<td id="Marque"><?php echo htmlspecialchars($row['Marque']); ?></td>
						<td id="Designation"><?php echo htmlspecialchars($row['Designation']); ?></td>
						<td id="Prix"><?php echo htmlspecialchars($row['Prix']); ?></td>
						<td id="Modifier"><form action='editer_ven_flash.php' method='POST'><input type="hidden" type='submit' name="edit" value="<?php echo $row['id']; ?>"><input type='submit' name="edit" value="<?php echo Modifier; ?>"></form>
						<td id="supprimer"><form action='suppr_ven_flash.php' method='POST'><input type="hidden" type='submit' name="sup" value="<?php echo $row['id']; ?>"><input type='submit' name="sup2" value="<?php echo X; ?>"></form>
					</tr>
     			<?php endwhile; ?>
   				</tbody>
			</table>



	    </table>
		</center>
	</form>
	</div>

		<!--Zone du footer-->
		<?php include("../../../include/footeracc.php"); ?>
		<!--<script type="text/javascript" src="../../js/Remise.js" charset="utf-8"></script>-->

		    <script language="javascript">
		      function confirme( identifiant )
		      {
		        var confirmation = confirm( "Voulez vous vraiment supprimer cet enregistrement ?" ) ;
			if( confirmation )
			{
			  document.location.href = "suppr_cata.php?idligne="+identifiant ;
			}
		      }
		    </script>
</body>
</html>