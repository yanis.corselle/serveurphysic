<?php

	try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

	?>

<html lang="fr">

<head>

	<meta charset="utf-8"/>

	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>

	<link rel="icon" type="image/png" href="favicon.png" />

	<link type="text/css" rel="stylesheet" href="../css/style.css" />

	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>

	<link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css" />

</head>

	<body>

	<!--Entete-->

		<?php include("../include/page_up.php"); ?>

<!--CONTACT-->

	<div class="zone3" value="Contact">

		<h4>

			PhYsic

				<br>

			59 Avenue Roger Dumoulin

				<br>

			Espace Industriel Nord

				<br>

			80046 AMIENS CEDEX 02

				<br>

			<b>

			Tel : 03 22 670 670

			

			</b>

				<br>

			Email: <a href="mailto:info@physic.fr">info@physic.fr</a>

		</h4>

	</div>



<!--Google Maps-->

	<div class="zone4">

		<center>

		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2568.18054735346!2d2.28283931583683!3d49.932953432397596!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e786b9993d4c63%3A0x158f6c425b2e1678!2sPhysic!5e0!3m2!1sfr!2sfr!4v1526971856302" height="600" width="600" frameborder="0" style="border:0" allowfullscreen></iframe>

		</center>

	</div>

	<br>



	<!--Zone du footer-->

	<?php include("../include/footer.php"); ?>

</body>

</html>

