﻿<html lang="fr">
<head>
	<meta charset="utf-8"/>
	<title>Forbidden</title>
	<link rel="icon" type="image/png" href="favicon.png" />
	<link type="text/css" rel="stylesheet" href="/css/style.css" />
	<link type="image/jpg" rel="icon" href="/img/favicon.jpg"/>
</head>
	<body>
	<!--Entete-->
		<?php include("/include/page_up_erreurs.php"); ?>
	<!---------->
		<div style="padding:50">
			<center><label><h2>Le serveur a compris la requête, mais refuse de l'exécuter. Contrairement à l'erreur 401, s'authentifier ne fera aucune différence. Sur les serveurs où l'authentification est requise, cela signifie généralement que l'authentification a été acceptée mais que les droits d'accès ne permettent pas au client d'accéder à la ressource.</h2></label></center>
		</div>

	<!--Footer-->
		<?php include("/include/footer.php"); ?>
</body>
</html>
