<!--Page footer + inclusion des JS-->

<link rel="stylesheet" href="../../../css/style.css">

<div class="zone7">

<footer>

<center> <div class="whitefooter"> ©<?php echo date("m/Y"); ?> PhYsic SARL - Tous droits réservés </div></center>

  <div class="logo_footer"> <a href="../../../index.php"><img src="../../../img/logo.png"></a> </div>
  <div class="logos_footer">
    <a href="../../../modules/boutique.php?terme=microsoft"> <img src="../../../img/microsoft.jpg" height="30px" width="60px"> </a>
    <a href="../../../modules/boutique.php?terme=avast"> <img src="../../../img/Avast-Antivirus.jpg" height="30px" width="60px"> </a>
    <a href="../../../modules/boutique.php?terme=apple"> <img src="../../../img/apple.jpg" height="30px" width="60px"> </a>
    <a href="../../../modules/boutique.php?terme=lenovo"> <img src="../../../img/lenovo.jpg" height="30px" width="60px"> </a>
    <a href="../../../modules/boutique.php?terme=hp"> <img src="../../../img/hp.jpg" height="30px" width="60px"> </a>
    <br>
    <a href="../../../modules/boutique.php?terme=zyxel"> <img src="../../../img/Zyxel-logo.jpg" height="30px" width="60px"> </a>
    <a href="../../../modules/boutique.php?terme=vmware"> <img src="../../../img/vmware.jpg" height="30px" width="60px"> </a>
    <a href="../../../modules/boutique.php?terme=d-link"> <img src="../../../img/d-link.jpg" height="30px" width="60px"> </a>
    <a href="../../../modules/boutique.php?terme=vivitek"> <img src="../../../img/vivitek.jpg" height="30px" width="60px"> </a>
    <a href="../../../modules/boutique.php?terme=fujitsu"> <img src="../../../img/fujitsu.png" height="30px" width="60px">
  </div>
  <br>
    <center><a href="../../../modules/mentions.php">Mentions légales</a> - <a href="../../../modules/plan.php">Plan d'accès</a> - <a href="../../../modules/partenaires.php">Nos partenaires privilégiés</a></center>

</footer>



</div>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script type="text/javascript" src="../../../js/jquery.mCustomScrollbar.concat.min.js"></script>

<script type="text/javascript">

  (function($){

      $(window).on("load",function(){

          $("body").mCustomScrollbar({

      theme:'inset-dark',

      scrollInertia:50

    });

      });

  })(jQuery);

</script>

