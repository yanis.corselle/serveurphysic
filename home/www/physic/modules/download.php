<?php

	session_start();?>

<!DOCTYPE HTML>

<html lang="fr">

<head>

	<meta charset="utf-8"/>

	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>

	<link rel="icon" type="image/png" href="../img/favicon.png" />

	<link type="text/css" rel="stylesheet" href="../css/style.css" />

	<link type="text/css" rel="stylesheet" href="../css/icones_admin.css" />

	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>

	<link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css" />

</head>

	<body>



		<!--Entête + boutons-->



		<?php include("../include/page_upadministration.php"); ?>



		<!--Zone des gestions-->

		<br>

		
		<center>
			<table border="0px" class="tablecatalogue">

					<tr>

						<th id="adobe">Adobe Acrobat Reader DC</th>

						<th></th>

						<th id ="avast">Avast Antivirus</th>

						<th></th>

						<th id="microsoft">Microsoft</th>

						<th></th>


					</tr>



					<tr>

						<td id="adobe"><a href="https://get.adobe.com/fr/reader/"><img src="../img/adobe_acrobat_reader.png" OnMouseOver="document.getElementById('icone_acc').src='../img/adobe_acrobat_reader_anim.png'" OnMouseOut="document.getElementById('icone_acc').src='../img/adobe_acrobat_reader.png'" width="90px" height="75px" id="icone_acc"/></a></td>

						<td></td>

						<td id="avast"><a href="https://www.avast.com/fr-fr/business"><img src="../img/Avast-Antivirus.jpg" OnMouseOver="document.getElementById('icone_user').src='../img/avast_anim.jpeg'" OnMouseOut="document.getElementById('icone_user').src='../img/Avast-Antivirus.jpg'" width="90px" height="75px" id="icone_user"/></a></td>

						<td></td>

						<td id="microsoft"><a href="https://www.microsoft.com/fr-fr"><img src="../img/microsoft.jpg" OnMouseOver="document.getElementById('icone_adduser').src='../img/microsoft_anim.png'" OnMouseOut="document.getElementById('icone_adduser').src='../img/microsoft.jpg'" width="90px" height="75px" id="icone_adduser"/></a></td>

						<td></td>
					</tr>
			</table>

			<table border="0px" class="tablecatalogue">
			<tr><td>&nbsp;</td></tr> 
					<tr>
						<th id="tv">TeamViewer</th>
						
						<th></th>
						
						<th id="vmware">VMWare</th>

						<th></th>
						
						<th id="knox">Knox</th>
					
					</tr>

					<tr>

						<td id="tv"><a href="https://download.teamviewer.com/download/version_13x/TeamViewer_Setup.exe"><img src="../img/team_viewer.png" OnMouseOver="document.getElementById('icone_catalogue').src='../img/team_viewer_anim.png'" OnMouseOut="document.getElementById('icone_catalogue').src='../img/team_viewer.png'" width="90px" height="75px" id="icone_catalogue"/></a></td>

						<td></td>

						<td id="vmware"><a href="https://www.vmware.com/fr.html"><img src="../img/vmware.jpg" OnMouseOver="document.getElementById('icone_vmware').src='../img/vmware_anim.png'" OnMouseOut="document.getElementById('icone_vmware').src='../img/vmware.jpg'" width="90px" height="75px" id="icone_vmware"/></a></td>

						<td></td>

						<td id="knox"><a href="../knox.apk"><img src="../img/knox.png" OnMouseOver="document.getElementById('icone_knox').src='../img/knox.png'" OnMouseOut="document.getElementById('icone_knox').src='../img/knox.png'" width="90px" height="75px" id="icone_knox"/></a></td>

					</tr>
			</table>
</center>
			
		






	<!--Zone du footer-->

	<?php include("../include/footeradministration.php"); ?>

</body>

</html>

