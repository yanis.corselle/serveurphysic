﻿<?php

try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

?>

<html lang="fr">

<head>

	<meta charset="utf-8"/>

	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>

	<link rel="icon" type="image/png" href="favicon.png" />

	<link type="text/css" rel="stylesheet" href="../css/style.css" />

	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>

	<link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css" />

</head>

	<body>

	<!--Entete-->

		<?php include("../include/page_up.php"); ?>



				<?php

			$bdd = new PDO("mysql:host=127.0.0.1;dbname=physic;charset=utf8", "admin", "root");

			if(isset($_GET['id']) AND !empty($_GET['id'])) {

			   $get_id = htmlspecialchars($_GET['id']);

			   $article = $bdd->prepare('SELECT * FROM catalogue WHERE id = ?');

			   $article->execute(array($get_id));

			   if($article->rowCount() == 1) {

				  $article = $article->fetch();

				  $titre = $article['titre'];

				  $contenu = $article['contenu'];

			   } else {

				  die('Cet article n\'existe pas !');

			   }

			} else {

			   die('Erreur');

			}

			?>

		<h1><?= $titre ?></h1>

		<p><?= $contenu ?></p>

		<!--Zone du footer-->

		<?php include("../include/footer.php"); ?>

	</body>

</html>

