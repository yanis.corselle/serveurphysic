﻿<!DOCTYPE html>

<html lang="fr">
<head>
	<meta charset="utf-8"/>
	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>
	<link rel="icon" type="image/png" href="favicon.png" />
	<link type="text/css" rel="stylesheet" href="../css/style.css" />
	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>
	<link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css" />

</head>
	<body>
	<!--Entete-->
		<?php include("../include/page_up.php"); ?>
	<img href="../img/nouveauxproduits2.png">

		<!--Connexion-->
		<?php
			try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

			if(isset($_SESSION['id']) AND $_SESSION['id'] > 0) {
			   $id = intval($_SESSION['id']);
			   $requser = $bdd->prepare('SELECT * FROM membres WHERE id = ?');
			   $requser->execute(array($id));
			   $userinfo = $requser->fetch();
			}
?>

	<!--formulaire d'édition du profil-->

		<div class="zone4">
		<center>
         <h2>Profil de <?php echo $_SESSION['pseudo'];?></h2>
         <br><br>
         Pseudo = <?php echo $_SESSION['pseudo'];?>
         <br>
         Mail = <?php echo $_SESSION['mail'];?>
         <br>
				 <br>
				 <?php
				 		if ($_SESSION['attribut'] == 1){
							echo '<a href="../modules/admin/administration.php">Espace d\'administration</a>';
						}
				  ?>
         <?php
         if (isset($_SESSION['id']) AND $userinfo['id'] == $_SESSION['id']) {
					 echo "<br>
					 <a href=\"../modules/editionprofil.php\">Editer mon profil</a>
					 <a href=\"../modules/deconnexion.php\">Se déconnecter</a>";
				 }
				 ?>
		</center>
		</div>
		<!--Zone du footer-->
		<?php include("../include/footer.php"); ?>
</body>
</html>
