<!DOCTYPE html>

<html lang="fr">

<head>

	<meta charset="utf-8"/>

	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>

	<link rel="icon" type="image/png" href="../../../img/favicon.png" />

	<link type="text/css" rel="stylesheet" href="../../../css/style.css" />

	<link type="image/jpg" rel="icon" href="../../../img/favicon.jpg"/>

	<script src='https://www.google.com/recaptcha/api.js'></script>

	<link rel="stylesheet" href="../../../css/jquery.mCustomScrollbar.css" />

</head>

	<body>

	<!--Entete-->

		<?php include("../../../include/page_upacc.php"); ?>



	<!--Image nouveauxproduits2-->

	<img href="../../../img/nouveauxproduits2.png">



<!--Inscriptions-->



<?php

try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }



	if(isset($_POST['forminscription']))

	{

		



		if(isset($_POST['nom_societe']) AND isset($_POST['nom']) AND isset($_POST['prenom']) AND isset($_POST['fonction']) AND isset($_POST['adresse']) AND isset($_POST['mail']) AND isset($_POST['numero_tel']) AND isset($_POST['pseudo']) AND isset($_POST['mdp']) AND isset($_POST['mdp2']) AND empty($_POST['nom_societe']) == FALSE AND empty($_POST['nom']) == FALSE AND empty($_POST['prenom']) == FALSE AND empty($_POST['fonction']) == FALSE  AND empty($_POST['adresse']) == FALSE AND empty($_POST['mail']) == FALSE AND empty($_POST['numero_tel']) == FALSE AND empty($_POST['pseudo']) == FALSE AND empty($_POST['mdp']) == FALSE AND empty($_POST['mdp2']) == FALSE)	

		{

			$attribut = 0;

			$nom_societe = htmlspecialchars($_POST['nom_societe']);

			$nom = htmlspecialchars($_POST['nom']);

			$prenom = htmlspecialchars($_POST['prenom']);

			$fonction = htmlspecialchars($_POST['fonction']);

			$adresse = htmlspecialchars($_POST['adresse']);

			$mail = htmlspecialchars($_POST['mail']);

			$numero_tel = htmlspecialchars($_POST['numero_tel']);

			$pseudo = htmlspecialchars($_POST['pseudo']);

			$mdp = sha1($_POST['mdp']);

			$mdp2 = sha1($_POST['mdp2']);

			$pseudolength = strlen($pseudo);



			if($pseudolength <= 255)

			{

				if(filter_var($mail, FILTER_VALIDATE_EMAIL))

				{

					$reqmail = $bdd->prepare("SELECT * FROM `membres` WHERE mail = ?");

					$reqmail->execute($mail);

					$mailexist = $reqmail->rowCount();

					if($mailexist == 0)

					{

						if($mdp == $mdp2)

						{

							$insertmbr = $bdd->prepare("INSERT INTO membres(attribut, pseudo, mail, mdp, nom_societe, fonction, nom, prenom, adresse, numero_tel) values (:attribut, :pseudo, :mail, :mdp, :nom_societe, :fonction, :nom, :prenom, :adresse, :numero_tel) ");

							$insertmbr -> execute(array(

								'attribut' => $attribut, 

								'pseudo'  =>$pseudo, 

								'mail' => $mail, 

								'mdp' => $mdp, 

								'nom_societe' => $nom_societe, 

								'fonction' => $fonction, 

								'nom' => $nom, 

								'prenom' => $prenom, 

								'adresse' => $adresse, 

								'numero_tel' => $numero_tel));



							$erreur = '<font color="green">Votre compte a bien été créé ! <a href="../../../modules/seconnecter.php">Me connecter</a></font>';



						} else {

							$erreur = "Vos mots de passes ne correspondent pas !";

						}

					} else {

						$erreur = "L'adresse mail saisie est déjà utilisée !";

					}

					} else {

					$erreur = "Votre adresse mail n'est pas valide !";

					}

				

			} else {

				$erreur = "Votre pseudo ne doit pas dépasser 255 caractères !";

			}

		} else {

			$erreur = "Tous les champs doivent être complétés !";

		}

	}





   

?>

	<!--formulaire d'inscription-->

		<div class="zone4" align="center">

		<img src="../../../img/accesauxservices.png" height="140px" width="700px">

		<form action="" method="POST" class="styleinscription">

			<table border="0px" class="inputbasic table">

				<tr>

					<td>

						<input type="text" name="nom_societe" id="nom_societe" class="inputbasic nom_societe" placeholder="Nom de la société" value="<?php if(isset($nom_societe)) { echo $nom_societe; } ?>">

					</td>

				</tr>



				<tr>

					<td>

						<input type="text" name="nom" id="nom" class="inputbasic nom" placeholder="Nom" value="<?php if(isset($nom)) { echo $nom; } ?>">

					</td>

				</tr>



				<tr>

					<td>

						<input type="text" name="prenom" id="prenom" class="inputbasic prenom" placeholder="Prénom" value="<?php if(isset($prenom)) { echo $prenom; } ?>"></td>

				</tr>



				<tr>

					<td>

						<input type="text" name="fonction" id="fonction" class="inputbasic fonction" placeholder="Fonction" value="<?php if(isset($fonction)) { echo $fonction; }?>">

					</td>

				</tr>



				<tr>

					<td>

						<input type="text" name="adresse" id="adresse" class="inputbasic adresse" placeholder="Adresse" value="<?php if(isset($adresse)) { echo $adresse; } ?>">

					</td>

				</tr>



				<tr>

					<td>

						<input type="email" name="mail" id="mail" class="inputbasic email" placeholder="Email" value="<?php if(isset($mail)) { echo $mail; } ?>">

					</td>

				</tr>



				<tr>

					<td>

						<input type="tel" name="numero_tel" id="numero_tel" class="inputbasic numero_tel" placeholder="Numéro de téléphone" value="<?php if(isset($numero_tel)) { echo $numero_tel; } ?>">

					</td>

				</tr>



				<tr>

					<td>

						<input type="text" name="pseudo" id="pseudo" class="inputbasic pseudo" placeholder="Nom d'utilisateur" value="<?php if(isset($pseudo)) { echo $pseudo; } ?>">

					</td>

				</tr>



				<tr>

					<td>

						<input type="password" name="mdp" id="mdp" class="inputbasic motdepasse" placeholder="Mot de passe" value="<?php if(isset($mdp)) { echo $mdp; } ?>">

					</td>

				</tr>



				<tr>

					<td>

						<input type="password" name="mdp2" id="mdp2" class="inputbasic motdepasse2" placeholder="Confirmation du mot de passe" value="<?php if(isset($mdp2)) { echo $mdp2; } ?>">

					</td>

				</tr>



				<tr>

					<td>

						<input type="reset" name="reset" value="Annuler">

					</td>

				</tr>



				<!-- <tr>

					<td>

						<div class="g-recaptcha" data-sitekey="6LdpclsUAAAAAJbJlK-w5bpU5QqBLkWZWNsG-owf"></div>

					</td>

				</tr> -->



				<tr>

					<td>

						<input type="submit" name="forminscription" value="Inscrire un client">

					</td>

				</tr>



			</table>

		</form>



	<?php

		if(isset($erreur))

		{

				echo '<font color="red">'.$erreur.'</font>';

		}

	?>

		</div>



<!--Zone du footer-->

<?php include("../../../include/footeracc.php"); ?>

</body>

</html>

