﻿<?php

$host = 'localhost';

$dbname = 'physic';

$username = 'admin';

$password = 'root';


$dsn = "mysql:host=$host;dbname=$dbname"; 

// récupérer tous les utilisateurs
if(isset($_POST['tri']))
{
	if($_POST['tri'] == 'prix_croissant') 
	{
		$sql = "SELECT * FROM catalogue ORDER BY Prix ASC";
	}
	elseif($_POST['tri'] == 'prix_decroissant') 
	{
		$sql = "SELECT * FROM catalogue ORDER BY Prix DESC";
	}

	else{
		$sql = "SELECT * FROM catalogue";
	}
}
else
{
	$sql = "SELECT * FROM catalogue";
}

$sql2 = "SELECT * FROM categories_boutique";

try{

 $pdo = new PDO($dsn, $username, $password);

 $stmt = $pdo->query($sql);
 $stmt2 = $pdo->query($sql2);

 if($stmt === false || $stmt2 == false){

  die("Erreur");

 }

 

}catch (PDOException $e){

  echo $e->getMessage();

}
if((isset($_GET['categorie'])) AND empty($_GET['categorie']) == FALSE)
{
	$categorie = $_GET['categorie'];
}
else{
	$categorie = "tout";
}



//include("RequeteAJAX.php");

?>



<html lang="fr">

<head>

	<meta charset="utf-8"/>

	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>

	<link rel="icon" type="image/png" href="../img/favicon.png" />

	<link type="text/css" rel="stylesheet" href="../css/style.css" />
	<link type="text/css" rel="stylesheet" href="../css/simple-grid.css" />

	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>



</head>

	<body>

	<!--Entete-->

		<?php include("../include/page_upcatalogue.php"); ?>



	<!--Image logo-->

<?php
$contenu .= "<center> <h1> Nos produits : </h1> </center>";
	//--------------------------------- TRAITEMENTS PHP ---------------------------------//
//--- AFFICHAGE DES CATEGORIES ---//

$contenu .= '<div class="boutique-gauche">';
$contenu .= "<ul class='menu-boutique'>";
while($row = $stmt2->fetch(PDO::FETCH_ASSOC))
		{
			$contenu .= "<li>";
			if(file_exists("../img/icones_boutique/" . $row['categorie'] . ".png"))
					{
						$image = "../img/icones_boutique/" . $row['categorie'] . ".png";
					}  

					elseif(file_exists("../img/icones_boutique/" . $row['categorie'] . ".jpg"))
					{
						$image = "../img/icones_boutique/" . $row['categorie'] . ".jpg";
					}  

					elseif(file_exists("../img/icones_boutique/" . $row['categorie'] . ".jpeg"))
					{
						$image = "../img/icones_boutique/" . $row['categorie'] . ".jpeg";
					}  

					else{
						$image = "null.png";
					}

			$contenu .= "<div class=\"categories\"> <a href='?categorie=" . $row['categorie'] . "'> <div class=\"img_cat_boutique\"> <img src=$image width=\"40\" height=\"40\"> </div> <div class=\"texte_cat_boutique\">" . $row['categorie'] . " </div></a> </div>";
			$contenu .= "<ul>";
			if(empty($row['sous_categorie1']) == FALSE)
			{
				$contenu .= "<li><a href='?categorie=" . $row['sous_categorie1'] . "'>" . $row['sous_categorie1'] . "</a> </li>" ;
			} 

			if(empty($row['sous_categorie2']) == FALSE)
			{
				$contenu .= "<li><a href='?categorie=" . $row['sous_categorie2'] . "'>" . $row['sous_categorie2'] . "</a> </li>" ;
			} 

			if(empty($row['sous_categorie3']) == FALSE)
			{
				$contenu .= "<li><a href='?categorie=" . $row['sous_categorie3'] . "'>" . $row['sous_categorie3'] . "</a> </li>" ;
			} 
			
			if(empty($row['sous_categorie4']) == FALSE)
			{
				$contenu .= "<li><a href='?categorie=" . $row['sous_categorie4'] . "'>" . $row['sous_categorie4'] . "</a> </li>" ;
			} 

			if(empty($row['sous_categorie5']) == FALSE)
			{
				$contenu .= "<li><a href='?categorie=" . $row['sous_categorie5'] . "'>" . $row['sous_categorie5'] . "</a> </li>" ;
			} 

			if(empty($row['sous_categorie6']) ==FALSE)
			{
				$contenu .= "<li><a href='?categorie=" . $row['sous_categorie6'] . "'>" . $row['sous_categorie6'] . "</a> </li>" ;
			} 

			if(empty($row['sous_categorie7']) == FALSE)
			{
				$contenu .= "<li><a href='?categorie=" . $row['sous_categorie7'] . "'>" . $row['sous_categorie7'] . "</a> </li>" ;
			} 


			$contenu .= "</ul>";
			
			$contenu .= "</li>";
		}
$contenu .= "</ul>";
$contenu .= "</div>";

//--- AFFICHAGE DES PRODUITS ---//
$contenu .= '<div class="boutique-droite">';	

?>
<div class="zones_boutique">
	<div class="gauche">
		<form action = "" method = "POST" >
		<input type = "text" name = "terme">
		<input type = "submit" name = "recherche" value = "Rechercher">
		</form>
	</div>
	
	<div class="droite">
		<form method="POST" action="" id="zone5">
			<label for="tri-select">Trier par:</label>

			<select name="tri" id="tri-select">
				<option value="">Affichage par défaut</option>
				<option value="prix_croissant">Prix croissant</option>
				<option value="prix_decroissant">Prix décroissant</option>
			</select>
			<input type="submit" value="Trier">

		</form>
	</div>
</div>

<?php
if(isset($_POST['recherche']))
{
	$_POST['terme'] = htmlspecialchars($_POST['terme']);
	if($_POST['recherche'] == 'Rechercher')
	{
		$contenu .= '<div class="container">';
		$emplacement_row=0;
		while($row = $stmt->fetch(PDO::FETCH_ASSOC))
		{
			if($row['Categorie'] == $categorie || $categorie == "tout" || $row['Sous_Categorie'] == $categorie)
			{
				if ((stristr($row['Designation'], $_POST['terme']) != FALSE) || (stristr($row['Reference'], $_POST['terme']) != FALSE))
				{
					if($emplacement_row == 4)
					{
						$emplacement_row = 0;
					}

					if($emplacement_row == 0)
					{
						$contenu .= '<div class="row">';
					}

					$contenu .= '<div class="col-3">';
					$titre = stristr($row['Designation'], ' - ', true);
					$id = htmlspecialchars($row['id']);
					if(file_exists("upload/img_cata/" . $id . ".png"))
					{
						$image = "upload/img_cata/" . $id . ".png";
					}  

					elseif(file_exists("upload/img_cata/" . $id . ".jpg"))
					{
						$image = "upload/img_cata/" . $id . ".jpg";
					}  

					elseif(file_exists("upload/img_cata/" . $id . ".jpeg"))
					{
						$image = "upload/img_cata/" . $id . ".jpeg";
					}  

					else{
						$image = "upload/img_cata/null.png";
					}
					$contenu .= '<div class="boutique-produit">';
					$contenu .= "<div class=\"visible_boutique\"><h2>$titre</h2> </div>";
					$contenu .= "<div class=\"invisible_boutique\"><p>$row[Designation]</p> </div>";
					$contenu .= "<div class=\"photos_boutique\"><img src=$image width=\"130\" height=\"100\"> </div>";
					$contenu .= "<p>$row[Prix] €</p>";
					$contenu .= "<p>Référence du produit : $row[Reference] </p>";
					$contenu .= "<a href=\"client/panier.php?action=ajout&amp;l=" . $row['Designation'] . "&amp;q=1&amp;p=" . $row['Prix'] . "&amp;r=" . $row['Reference'] ." \"onclick=\"window.open(this.href, '';\">Ajouter au panier</a>";
					$contenu .= '</div>';
					$contenu .= '</div>';
					if($emplacement_row == 3)
					{
						$contenu .= '</div>';
					}
					$emplacement_row += 1;
					
					
				}
			}
			
		}
		$contenu .= '</div>';
	}
	
}
else
{
	$emplacement_row = 0;
	$contenu .= '<div class="container">';
	while($row = $stmt->fetch(PDO::FETCH_ASSOC))
	{
		if($row['Categorie'] == $categorie || $categorie == "tout" || $row['Sous_Categorie'] == $categorie)
		{
			if($emplacement_row == 4)
			{
				$emplacement_row = 0;
			}

			if($emplacement_row == 0)
			{
				$contenu .= '<div class="row">';
			}

			$contenu .= '<div class="col-3">';
			$titre = stristr($row['Designation'], ' - ', true);
			$id = htmlspecialchars($row['id']);
			if(file_exists("upload/img_cata/" . $id . ".png"))
			{
				$image = "upload/img_cata/" . $id . ".png";
			}  

			elseif(file_exists("upload/img_cata/" . $id . ".jpg"))
			{
				$image = "upload/img_cata/" . $id . ".jpg";
			}  

			elseif(file_exists("upload/img_cata/" . $id . ".jpeg"))
			{
				$image = "upload/img_cata/" . $id . ".jpeg";
			}  

			else{
				$image = "upload/img_cata/null.png";
			}
			$contenu .= '<div class="boutique-produit">';
			$contenu .= "<div class=\"visible_boutique\"><h2>$titre</h2> </div>";
			$contenu .= "<div class=\"invisible_boutique\"><p>$row[Designation]</p> </div>";
			$contenu .= "<div class=\"photos_boutique\"><img src=$image width=\"130\" height=\"100\"> </div>";
			$contenu .= "<p>$row[Prix] €</p>";
			$contenu .= "<p>Référence du produit : $row[Reference] </p>";
			$contenu .= "<a href=\"client/panier.php?action=ajout&amp;l=" . $row['Designation'] . "&amp;q=1&amp;p=" . $row['Prix'] . "&amp;r=" . $row['Reference'] ." \"onclick=\"window.open(this.href, '';\">Ajouter au panier</a>";
			$contenu .= '</div>';
			$contenu .= '</div>';
			
			if($emplacement_row == 3)
			{
				$contenu .= '</div>';
			}
			$emplacement_row += 1;
				
					
		}
	}
		
	$contenu .= '</div>';
	$contenu .= '</div>';
}

//--------------------------------- AFFICHAGE HTML ---------------------------------//
$contenu .= '</div>';
?> 
<div class="boutique"> <?php echo $contenu; ?> </div>

		<!--Zone du footer-->

		<?php include("../include/footercatalogue.php"); ?>

		<!--<script type="text/javascript" src="../../js/Remise.js" charset="utf-8"></script>-->

</body>

</html>

