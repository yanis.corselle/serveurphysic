<?php
$tabmois = array( 1 => "Janvier", 2 => "F�vrier", 3 => "Mars", 4 => "Avril", 5 => "Mai", 6 => "Juin", 7 => "Juillet", 8 => "Ao�t", 9 => "Septembre", 10 => "Octobre", 11 => "Novembre", 12 => "D�cembre", "mois" => "Mois", "01" => "Janvier", "02" => "F�vrier", "03" => "Mars", "04" => "Avril", "05" => "Mai", "06" => "Juin", "07" => "Juillet", "08" => "Ao�t", "09" => "Septembre" );
?>
<table border="0" cellpadding="5" cellspacing="0" width="570">
<tr>
	<td class="txt" width="200">
		<b>N� de commande :</b> 
	</td>
	<td class="txt">
		<input name="ncommande" class="champ-bleu" size="12" maxlength="12" type="text" readonly value="<?php if (isset($_SESSION['modifcommande_numerocommande'])) { echo "$_SESSION[modifcommande_numerocommande]"; } ?>">
	</td>
</tr>
<tr>
	<td class="txt">
		<b>Statut de commande :</b>
	</td>
	<td class="txt">
		<select name="statut" class="champ-bleu">
				<?php if (isset($statut) && $statut==1) {  ?>
				 <option value="1" selected="selected">Ouvert</option>
				 <option value="0">Clos</option>
				<?php } ?>
				<?php if (isset($statut) && $statut==0) {  ?>
				 <option value="1">Ouvert</option>
				 <option value="0" selected="selected">Clos</option>
				<?php } ?>
				<?php if (!isset($statut)) {  ?>
				 <option value="1">Ouvert</option>
				 <option value="0">Clos</option>
				<?php } ?>
		</select>
	</td>
</tr>
<tr>
	<td class="txt">
		<br />
		<b>Date commande :</b>
	</td>
	<td class="txt">
		<br />
		<select name="dc_jour" class="champ-bleu">
			<?php
			if (isset($dc_jour)) {
					echo "<option value=\"$dc_jour\">$dc_jour</option>";
				}
			?>
			<option value="jour">Jour</option>
			<?php
			$i=1;
			for ($i ; $i<=31 ; $i++)
			{	
				echo "<option value=\"$i\">$i</option>";
			}
			?>
		</select>
		
		<select name="dc_mois" class="champ-bleu">
			<?php
			if (isset($dc_mois)) {
					echo "<option value=\"$dc_mois\">$tabmois[$dc_mois]</option>";
				}
			?>
			<option value="mois">Mois</option>
			<option value="1">Janvier</option>
			<option value="2">F�vrier</option>
			<option value="3">Mars</option>
			<option value="4">Avril</option>
			<option value="5">Mai</option>
			<option value="6">Juin</option>
			<option value="7">Juillet</option>
			<option value="8">Ao�t</option>
			<option value="9">Septembre</option>
			<option value="10">Octobre</option>
			<option value="11">Novembre</option>
			<option value="12">D�cembre</option>
		</select>
		
		<input name="dc_annee" class="champ-bleu" size="2" maxlength="4" type="text" value="<?php if (isset($dc_annee)) { echo "$dc_annee"; } ?>"> Ann�e (Ex : 1999)
	</td>
</tr>
<tr>
<td class="txt">
		<b>Date livraison :</b>
	</td>
	<td class="txt">
		<select name="dl_jour" class="champ-bleu">
			<?php
			if (isset($dl_jour)) {
					echo "<option value=\"$dl_jour\">$dl_jour</option>";
				}
			?>
			<option value="jour">Jour</option>
			<?php
			$i=1;
			for ($i ; $i<=31 ; $i++)
			{
				echo "<option value=\"$i\">$i</option>";
			}
			?>
		</select>
		
		<select name="dl_mois" class="champ-bleu">
			<?php
			if (isset($dl_mois)) {
					echo "<option value=\"$dl_mois\">$tabmois[$dl_mois]</option>";
				}
			?>
			<option value="mois">Mois</option>
			<option value="1">Janvier</option>
			<option value="2">F�vrier</option>
			<option value="3">Mars</option>
			<option value="4">Avril</option>
			<option value="5">Mai</option>
			<option value="6">Juin</option>
			<option value="7">Juillet</option>
			<option value="8">Ao�t</option>
			<option value="9">Septembre</option>
			<option value="10">Octobre</option>
			<option value="11">Novembre</option>
			<option value="12">D�cembre</option>
		</select>
		
		<input name="dl_annee" class="champ-bleu" size="2" maxlength="4" type="text" value="<?php if (isset($dl_annee)) { echo "$dl_annee"; } ?>"> Ann�e (Ex : 1999)
	</td>
</tr>
<tr>
	<td class="txt">
		<b>Date commande fournisseur :</b>
	</td>
	<td class="txt">
		<select name="dcf_jour" class="champ-bleu">
			<?php
			if (isset($dcf_jour)) {
					echo "<option value=\"$dcf_jour\">$dcf_jour</option>";
				}
			?>
			<option value="jour">Jour</option>
			<?php
			$i=1;
			for ($i ; $i<=31 ; $i++)
			{
				echo "<option value=\"$i\">$i</option>";
			}
			?>
		</select>
		
		<select name="dcf_mois" class="champ-bleu">
			<?php
			if (isset($dcf_mois)) {
					echo "<option value=\"$dcf_mois\">$tabmois[$dcf_mois]</option>";
				}
			?>
			<option value="mois">Mois</option>
			<option value="1">Janvier</option>
			<option value="2">F�vrier</option>
			<option value="3">Mars</option>
			<option value="4">Avril</option>
			<option value="5">Mai</option>
			<option value="6">Juin</option>
			<option value="7">Juillet</option>
			<option value="8">Ao�t</option>
			<option value="9">Septembre</option>
			<option value="10">Octobre</option>
			<option value="11">Novembre</option>
			<option value="12">D�cembre</option>
		</select>
		
		<input name="dcf_annee" class="champ-bleu" size="2" maxlength="4" type="text" value="<?php if (isset($dcf_annee)) { echo "$dcf_annee"; } ?>"> Ann�e (Ex : 1999)
	</td>
</tr>
<tr>
<td class="txt">
		<b>Date r�ception fournisseur :</b>
	</td>
	<td class="txt">
		<select name="drf_jour" class="champ-bleu">
			<?php
			if (isset($drf_jour)) {
					echo "<option value=\"$drf_jour\">$drf_jour</option>";
				}
			?>
			<option value="jour">Jour</option>
			<?php
			$i=1;
			for ($i ; $i<=31 ; $i++)
			{
				echo "<option value=\"$i\">$i</option>";
			}
			?>
		</select>
		
		<select name="drf_mois" class="champ-bleu">
			<?php
			if (isset($drf_mois)) {
					echo "<option value=\"$drf_mois\">$tabmois[$drf_mois]</option>";
				}
			?>
			<option value="mois">Mois</option>
			<option value="1">Janvier</option>
			<option value="2">F�vrier</option>
			<option value="3">Mars</option>
			<option value="4">Avril</option>
			<option value="5">Mai</option>
			<option value="6">Juin</option>
			<option value="7">Juillet</option>
			<option value="8">Ao�t</option>
			<option value="9">Septembre</option>
			<option value="10">Octobre</option>
			<option value="11">Novembre</option>
			<option value="12">D�cembre</option>
		</select>
		
		<input name="drf_annee" class="champ-bleu" size="2" maxlength="4" type="text" value="<?php if (isset($drf_annee)) { echo "$drf_annee"; } ?>"> Ann�e (Ex : 1999)
	</td>
</tr>
</table>