﻿<?php

session_start();

include_once("fonctions-panier.php");

if(isset($_SESSION['attribut']) == FALSE)

{

	header("Location:../../modules/profil.php");

}



$erreur = false;



$action = (isset($_POST['action'])? $_POST['action']:  (isset($_GET['action'])? $_GET['action']:null )) ;

if($action !== null)

{

   if(!in_array($action,array('ajout', 'suppression')))

   $erreur=true;



   //récupération des variables en POST ou GET


   $q = (isset($_POST['q'])? $_POST['q']:  (isset($_GET['q'])? $_GET['q']:null )) ;


   $id = (isset($_POST['id'])? $_POST['id']:  (isset($_GET['id'])? $_GET['id']:null )) ;





   if (is_array($q)){


      $QteArticle = array();

      $i=0;

      foreach ($q as $contenu){

         $QteArticle[$i] = intval($contenu);
         $i++;
      }

   }

   else

   $q = intval($q);

    

}



if (!$erreur){

   switch($action){

      case "ajout":

         ajouterArticle($q, $id);

         break;



      case "suppression":

         supprimerArticle($id);

         break;

      default:

         break;

   }

}



$host = 'localhost';

$dbname = 'physic';

$username = 'admin';

$password = 'root';


$dsn = "mysql:host=$host;dbname=$dbname"; 
$sql = "SELECT * FROM catalogue";

try{

   $pdo = new PDO($dsn, $username, $password);
   $stmt = $pdo->prepare($sql);
   $stmt->execute();
   if($stmt === false){

      die("Erreur");
    
   }
    
}
catch (PDOException $e)
{
    
   echo $e->getMessage();
    
}


?>


<html lang="fr">

<head>

	<meta charset="utf-8"/>

	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>

	<link rel="icon" type="image/png" href="favicon.png" />

	<link type="text/css" rel="stylesheet" href="../../css/style.css" />

	<link type="image/jpg" rel="icon" href="../../img/favicon.jpg"/>

	<link rel="stylesheet" href="../../css/jquery.mCustomScrollbar.css" />

</head>

	<body>

	<!--Entete-->

      <?php include("../../include/page_upcatalogue.php"); ?>


<form method="post" action="">



    

    





    <?php

    if (creationPanier())

    {
       ?> <?php

       $message = "Bonjour, " . $_SESSION['pseudo'] . "(" . $_SESSION['id'] . ")" . "souhaite commander :";

       $nbArticles=count($_SESSION['panier']['idProduit']);

       if($nbArticles>1)
       {
          $q = array();
       }

       if ($nbArticles <= 0)

       echo "<center> <h2>Votre panier est vide </h2> </center>";

       else

       {
          ?>
          
         
            <center> <h1> Votre panier </h1> </center>
      <table id='tb_panier'>
    
         <tr>

            <th id="Reference_fournisseur">Référence fournisseur</th>

            <th id="Reference_fabricant">Référence fabricant</th>

            <th id="Libelle"> <div id="txt_center"> Libellé </div> </th> 

            <th id="Quantite">Quantité</th>

            <th id="Prix_Pu_HT">Prix PU HT du conditionnement (avant remise)</th>

            <th id="Remise">Votre remise</th>

            <th id="Prix_PU_HT_remise">Prix PU HT du conditionnement remisé</th>

            <th id="TVA">TVA en vigueur</th>

            <th id="Prix_PU_TTC_remise">Prix PU TTC du conditionnement remisé</th>

            <th id="Prix_total_TTC_remise">Prix Total TTC du conditionnement remisé</th>

            <th id="Action">Action</th>

    
         </tr> 
         
         <?php
          $MontantGlobal = 0;
          
         while($row = $stmt->fetch(PDO::FETCH_ASSOC))
         {
            for($i=0; $i<$nbArticles;$i++)
            {
                
                if($_SESSION['panier']['idProduit'][$i] == $row['id'])
                {
                  if($_SESSION['panier']['qteProduit'][$i] > 0)
                  {
                     if($_SESSION['panier']['qteProduit'][$i] < 16)
                     {
                        $remise = $_SESSION['remise1'];
                     }
                     else if($_SESSION['panier']['qteProduit'][$i] < 151)
                     {
                        $remise = $_SESSION['remise2'];
                     }
                     else
                     {
                        $remise = $_SESSION['remise3'];
                     }
                  }

                  else
                  {
                     $remise = $_SESSION['remise1'];
                  }

               $_SESSION['panier']['Prix_PU_HT_remise'][$i] = $row['Prix_PU_HT'] - ($row['Prix_PU_HT'] * (($remise/100)));


               $_SESSION['panier']['Prix_PU_TTC_remise'][$i] = $_SESSION['panier']['Prix_PU_HT_remise'][$i] + ($_SESSION['panier']['Prix_PU_HT_remise'][$i] * ($_SESSION['TVA']/100));

               $_SESSION['panier']['Prix_total_TTC_remise'][$i] = $_SESSION['panier']['Prix_PU_TTC_remise'][$i] * $_SESSION['panier']['qteProduit'][$i];

               $MontantGlobal += $_SESSION['panier']['Prix_total_TTC_remise'][$i];
               

            echo "<tr>";

               echo "<td id=\"Reference_fournisseur[$i]\">".htmlspecialchars($row['Reference_fournisseur'])."</td>";
               
               echo "<td id=\"Reference_fabricant[$i]\">".htmlspecialchars($row['Reference_fabricant'])."</td>";

               echo "<td id=\"Libelle[$i]\">".htmlspecialchars($row['Designation'])."</td>";

               echo "<td id=\"Quantite[$i]\"> <input type=\"text\" id=\"quantite[$i]\" size=\"4\" name=\"q[$i]\" onchange=\"controle$i()\" value=\"".htmlspecialchars($_SESSION['panier']['qteProduit'][$i])."\"/></td>";

               echo "<td id=\"Prix_PU_HT[$i]\"> <span id='prix_PU_HT[$i]'>" .htmlspecialchars($row['Prix_PU_HT'])." </span> € </td>";

               echo "<td id=\"Remise[$i]\"> <span id='remise[$i]'>".htmlspecialchars($remise)." </span>% </td>";

               echo "<td id=\"Prix_PU_HT_remise[$i]\"> <span id='prix_PU_HT_remise[$i]'>".htmlspecialchars($_SESSION['panier']['Prix_PU_HT_remise'][$i])." </span>€ </td>";

               echo "<td id=\"TVA[$i]\">".htmlspecialchars($_SESSION['TVA'])."% </td>";

               echo "<td id=\"Prix_PU_TTC_remise[$i]\"> <span id='prix_PU_TTC_remise[$i]'> ". htmlspecialchars($_SESSION['panier']['Prix_PU_TTC_remise'][$i])." </span>€ </td>";

               echo "<td id=\"Prix_total_TTC_remise[$i]\"> <span id='prix_total_TTC_remise[$i]'>".htmlspecialchars($_SESSION['panier']['Prix_total_TTC_remise'][$i])."</span>€ </td>";

               echo "<td id=\"Action[$i]\"><a href=\"".htmlspecialchars("panier.php?action=suppression&id=".rawurlencode($_SESSION['panier']['idProduit'][$i]))."\">X</a></td>";

               echo "</tr>";

               }

               ?>

               <script type="text/javascript">

                  prix_total_TTC_remise = [];

                  function roundDecimal(nombre, precision){
                     var precision = precision || 2;
                     var tmp = Math.pow(10, precision);
                     return Math.round( nombre*tmp )/tmp;
                  }

                  function controle<?php echo json_encode($i); ?>(){
                     var quantite = parseInt(document.getElementById("quantite[<?php echo json_encode($i); ?>]").value);
                        
                     if(quantite > 0)
                     {
                           if(quantite < 16)
                           {
                              var remise = <?php echo json_encode($_SESSION['remise1']); ?>;
                           }
                           else if(quantite < 151)
                           {
                              var  remise = <?php echo json_encode($_SESSION['remise2']); ?>;
                           }
                           else if(150 < quantite)
                           {
                              var remise = <?php echo json_encode($_SESSION['remise3']); ?>;
                           }
                           remise = roundDecimal(remise);
                           
                     }
                     else
                     {
                           
                           alert('Veuillez saisir une quantité valide ou supprimer l\'article du panier !');
                     }
                     if(remise != null)
                     {
                           document.getElementById('remise[<?php echo json_encode($i); ?>]').innerHTML =  remise;
                           var prix_PU_HT_remise = document.getElementById("prix_PU_HT[<?php echo json_encode($i); ?>]").innerHTML - document.getElementById("prix_PU_HT[<?php echo json_encode($i); ?>]").innerHTML * (remise*0.01);
                           prix_PU_HT_remise = roundDecimal(prix_PU_HT_remise);
                           document.getElementById('prix_PU_HT_remise[<?php echo json_encode($i); ?>]').innerHTML =  prix_PU_HT_remise;
                           var prix_PU_TTC_remise = prix_PU_HT_remise * (1+(<?php echo json_encode($_SESSION['TVA']); ?>*0.01));
                           prix_PU_TTC_remise = roundDecimal(prix_PU_TTC_remise);
                           document.getElementById('prix_PU_TTC_remise[<?php echo json_encode($i); ?>]').innerHTML =  prix_PU_TTC_remise;
                           prix_total_TTC_remise[<?php echo json_encode($i); ?>] = prix_PU_TTC_remise*quantite;
                           prix_total_TTC_remise[<?php echo json_encode($i); ?>] = roundDecimal(prix_total_TTC_remise[<?php echo json_encode($i); ?>]);
                           document.getElementById('prix_total_TTC_remise[<?php echo json_encode($i); ?>]').innerHTML =  prix_total_TTC_remise[<?php echo json_encode($i); ?>];
                           montantTotal = MontantGlobal();
                           document.getElementById('montantGlobal').innerHTML =  montantTotal;
                     }
                       
                  }

                  function MontantGlobal()
                  {
                     var montantGlobal = [];
                     <?php
                     for($index_MontantGlobal=0; $index_MontantGlobal<$nbArticles;$index_MontantGlobal++)
                     {
                           ?>
                           montantGlobal[<?php echo json_encode($index_MontantGlobal); ?>] = document.getElementById("prix_total_TTC_remise[<?php echo json_encode($index_MontantGlobal); ?>]").innerHTML;
                           
                           <?php
                     }
                     

                     

                     ?>
                     longueur_montantGlobal = montantGlobal.length;
                     var montantTotal = 0;
                     for(let index_longueur_montantGlobal = 0; index_longueur_montantGlobal < longueur_montantGlobal ; index_longueur_montantGlobal++)
                     {
                        montantTotal = parseFloat(montantTotal) + parseFloat(montantGlobal[index_longueur_montantGlobal]);
                     }
                     montantTotal = parseFloat(montantTotal);
                     montantTotal = roundDecimal(montantTotal);
                     return montantTotal;
                  }
               </script>
               <?php
             }
            
          }

          echo "</table>";

         
          echo " <p> <center> Total : <span id='montantGlobal'>". MontantGlobal() . "</span>€ </center> </p>";
          
       }

	}



   if(isset($_POST['envoipdf']) AND empty($_POST['envoipdf']) == FALSE)

   {

      while($row = $stmt->fetch(PDO::FETCH_ASSOC))
      {
         for($index_montantTotal = 0; $index_montantTotal < $nbArticles;$index_montantTotal++)
         {
            if($_SESSION['panier']['idProduit'][$i] == $row['id'])
            {
               if($_SESSION['panier']['qteProduit'][$i] > 0)
               {
               $message .= " " . htmlspecialchars($_SESSION['panier']['qteProduit'][$index_montantTotal]) . " exemplaires de " . htmlspecialchars($row['Designation']) . "(" . htmlspecialchars($row['id']) . "), ";
               }
            }
         }
      }

      $message .= "Veuillez le contacter.";

      echo "Contenu du message envoyé:";

      echo $message;

   }
 

    $to = "technique@physic.fr";

 

    $subject = "Nouvelle commande";


 

    mail($to,$subject,$message);

   

    ?>



</form>

<div id="txt_center">

	<a href="javascript:window.print()"><button class="inputbasic">Imprimer cette page</button></a>

	<form method = 'POST' action=''> <input type="submit" value="Envoyer un mail" name="envoipdf"> </form>


</div>
	<br />



	

			

		<?php include("../../include/footercatalogue.php"); ?>

	</body>

</html>