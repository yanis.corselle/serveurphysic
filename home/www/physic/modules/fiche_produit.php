<?php

session_start();


$host = 'localhost';

$dbname = 'physic';

$username = 'admin';

$password = 'root';


$dsn = "mysql:host=$host;dbname=$dbname"; 
$sql = "SELECT * FROM catalogue WHERE id = '$_GET[id]'";
try{

    $pdo = new PDO($dsn, $username, $password);
    $stmt = $pdo->prepare($sql);
    
    if($stmt === false){

        die("Erreur");
      
       }
   
   }catch (PDOException $e){
   
     echo $e->getMessage();
   
   }

function tronquer_texte($texte, $limite){
	$tab = explode(' ', $texte, ($limite+1));
	if(count($tab) > $limite){ array_pop($tab); }
	return implode(' ', $tab);
}
if(isset($_GET['id']))  {$stmt->execute();}
else { header("location:boutique.php"); exit(); }


$produit = $stmt->fetch(PDO::FETCH_ASSOC);


$titre = tronquer_texte($produit['Designation'], 3);
if(stristr($titre, 'K/'))
{
    $titre = str_replace('K/', ' ', $titre);
}
elseif(stristr($titre, 'CS/'))
{
    $titre = str_replace('CS/', ' ', $titre);
}

$id = htmlspecialchars($produit['id']);
if(file_exists("upload/img_cata/" . $id . ".png"))
{
    $image = "upload/img_cata/" . $id . ".png";
}  

elseif(file_exists("upload/img_cata/" . $id . ".jpg"))
{
    $image = "upload/img_cata/" . $id . ".jpg";
}  

elseif(file_exists("upload/img_cata/" . $id . ".jpeg"))
{
    $image = "upload/img_cata/" . $id . ".jpeg";
}  

else
{
    if(file_exists("../img/icones_boutique/" . $produit['Categorie'] . ".png"))
    {
        $image = "../img/icones_boutique/" . $produit['Categorie'] . ".png";
    }  

    elseif(file_exists("../img/icones_boutique/" . $produit['Categorie'] . ".jpg"))
    {
        $image = "../img/icones_boutique/" . $produit['Categorie'] . ".jpg";
    }  

    elseif(file_exists("../img/icones_boutique/" . $produit['Categorie'] . ".jpeg"))
    {
        $image = "../img/icones_boutique/" . $produit['Categorie'] . ".jpeg";
    }  

    else{
        $image = "null.png";
    }
}

if(isset($_SESSION['TVA']))
{
    $TVA = $_SESSION['TVA'];
} 
else{
    $TVA = 20;
}

if(isset($_SESSION['remise1']))
{
    $remise1 = $_SESSION['remise1'];
} 
else{
    $remise1 = 0;
}

if(isset($_SESSION['remise2']))
{
    $remise2 = $_SESSION['remise2'];
} 
else{
    $remise2 = 0;
}

if(isset($_SESSION['remise3']))
{
    $remise3 = $_SESSION['remise3'];
} 
else{
    $remise3 = 0;
}

$prix_PU_HT_remise = $produit['Prix_PU_HT'] * (1-$remise1*0.01);
$prix_PU_TTC_remise = $prix_PU_HT_remise * (1+$TVA*0.01);
$prix_total_TTC_remise = $prix_PU_TTC_remise;

?>
<script type="text/javascript">

    function roundDecimal(nombre, precision){
        var precision = precision || 2;
        var tmp = Math.pow(10, precision);
        return Math.round( nombre*tmp )/tmp;
    }

	function controle(){
        var quantite = parseInt(document.getElementById("quantite").value);
        if(quantite > 0)
        {
            if(quantite < 16)
            {
                var remise = <?php echo json_encode($remise1); ?>;
            }
            else if(quantite < 151)
            {
                var  remise = <?php echo json_encode($remise2); ?>;
            }
            else if(150 < quantite)
            {
                var remise = <?php echo json_encode($remise3); ?>;
            }
        }
        else
        {
            remise = roundDecimal(remise);
            alert('Veuillez saisir une quantité valide !');
        }
        if(remise != null)
        {
            document.getElementById('remise').innerHTML =  remise;
            var prix_PU_HT_remise = <?php echo json_encode($produit['Prix_PU_HT']); ?> * (1-remise*0.01);
            prix_PU_HT_remise = roundDecimal(prix_PU_HT_remise);
            document.getElementById('prix_PU_HT_remise').innerHTML =  prix_PU_HT_remise;
            var prix_PU_TTC_remise = prix_PU_HT_remise * (1+<?php echo json_encode($TVA); ?>*0.01);
            prix_PU_TTC_remise = roundDecimal(prix_PU_TTC_remise);
            document.getElementById('prix_PU_TTC_remise').innerHTML =  prix_PU_TTC_remise;
            var prix_total_TTC_remise = prix_PU_TTC_remise*quantite;
            prix_total_TTC_remise = roundDecimal(prix_total_TTC_remise);
            document.getElementById('prix_total_TTC_remise').innerHTML =  prix_total_TTC_remise;
        }
    }
</script>

<?php

$contenu = "<center> <h2>$titre</h2> </center> <hr><br>";
$contenu .= "<p>Catégorie: $produit[Categorie]</p>";
$contenu .= "<p>Sous-Catégorie: $produit[Sous_Categorie]</p>";
$contenu .= "<p>Référence fournisseur: $produit[Reference_fournisseur] </p>";
$contenu .= "<p>Référence fabricant: $produit[Reference_fabricant] </p>";
$contenu .= "<p>Unité de mesure du conditionnement: $produit[Unite_mesure] </p>";
$contenu .= "<p>Nombre de pièces contenues dans l'unité de mesure: $produit[Nombre_pieces] </p>";
$contenu .= "<p>Nature des pièces contenues dans l'unité de mesure: $produit[Categorie]</p>";
$contenu .= "<p>Minimum de commande d'unité de conditionnement: $produit[Minimum_commande]</p>";
$contenu .= "<p>Délai de livraison en heures ouvrées (hors samedi, dimanche et jours fériés): </p>";


$contenu .= "<img src='$image' width='150' height='150'>";
$contenu .= "<p>Description: <i> $produit[Designation]</i></p><br>";
$contenu .= "<p>Prix PU HT du conditionnement (avant remise): $produit[Prix_PU_HT]€</p>";
$contenu .= "<p> Remise(modifiable selon la quantité) : <span id='remise'> $remise1</span>% ";
$contenu .= "<p>Prix PU HT (prix unitaire hors taxes) du conditionnement remisé: <span id='prix_PU_HT_remise'> $prix_PU_HT_remise</span>€ </p>";
$contenu .= "<p>TVA en vigeur : $TVA%</p>";
$contenu .= "<p>Prix PU TTC (prix unitaire toutes taxes comprises) du conditionnement remisé: <span id='prix_PU_TTC_remise'> $prix_PU_TTC_remise</span>€ </p>";
$contenu .= "<p>Prix Total TTC (prix total toutes taxes comprises) du conditionnement remisé: <span id='prix_total_TTC_remise'> $prix_total_TTC_remise</span>€ </p>";
 

					//pqlri
		
$contenu .= '<form method="get" action="client/panier.php">';
    $contenu .= " <input type='hidden' name='action' value='ajout'>";
    $contenu .= " <input type='hidden' name='id' value='$produit[id]'>";
    $contenu .= " <label for=\"quantite\">Quantité : </label>";
    $contenu .= " <input type=\"text\" id=\"quantite\" name='q' size=\"2\" onchange=\"controle()\" value=1 onfocus=\"if(this.value==\'\'){this.value=1}\" onblur=\"if(this.value==\'\'){this.value = 1}\">";
    $contenu .= " <input type=\"submit\" name=\"ajout_panier\" value=\"ajout au panier\">";
$contenu .= " </form>";

$contenu .= "<br><a href='boutique.php'>Retour vers la boutique</a>";
//--------------------------------- AFFICHAGE HTML ---------------------------------//
?> 
<html lang="fr">

<head>

	<meta charset="utf-8"/>

	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>

	<link rel="icon" type="image/png" href="../img/favicon.png" />

	<link type="text/css" rel="stylesheet" href="../css/style.css" />

	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>



</head>

	<body>

    <?php 
        include("../include/page_up.php"); 
        echo $contenu;
        include("../include/footer.php"); ?>
    </body>

</html>