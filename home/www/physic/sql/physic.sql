-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Client: 127.0.0.1
-- Généré le : Lun 11 Février 2019 à 18:13
-- Version du serveur: 5.5.20
-- Version de PHP: 5.3.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `physic`
--

-- --------------------------------------------------------

--
-- Structure de la table `accueil`
--

CREATE TABLE IF NOT EXISTS `accueil` (
  `acc_id` int(255) NOT NULL AUTO_INCREMENT,
  `acc_img` varchar(255) NOT NULL,
  `acc_article` text NOT NULL,
  UNIQUE KEY `acc_id` (`acc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `administrateur`
--

CREATE TABLE IF NOT EXISTS `administrateur` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) NOT NULL,
  `mdp` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `administrateur`
--

INSERT INTO `administrateur` (`id`, `pseudo`, `mdp`, `email`) VALUES
(1, 'admin', 'physic', 'info@physic.fr');

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) NOT NULL,
  `contenu` text NOT NULL,
  `date_time_publication` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `catalogue`
--

CREATE TABLE IF NOT EXISTS `catalogue` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `Reference` varchar(255) NOT NULL,
  `Marque` varchar(255) NOT NULL,
  `Designation` varchar(255) NOT NULL,
  `Prix` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=129 ;

--
-- Contenu de la table `catalogue`
--

INSERT INTO `catalogue` (`id`, `Reference`, `Marque`, `Designation`, `Prix`) VALUES
(117, 'Reference', 'Marque', 'Designation', 'Prix'),
(118, '4095914', 'EPSON', 'Epson EB-2155W - Projecteur 3LCD - 5000 lumens (blanc) - 5000 lumens (couleur) - WXGA (1280 x 800) - 16:10 - HD 720p - 802.11b/g/n wireless / LAN / Miracast', '920'),
(119, '4095917', 'EPSON', 'Epson EB-2250U - Projecteur LCD - 5000 lumens (blanc) - 5000 lumens (couleur) - WUXGA (1920 x 1200) - 16:10 - HD 1080p - LAN', '1 234,96'),
(120, '3601413', 'BENQ', 'BenQ SW921 - Projecteur DLP - 3D - 5000 ANSI lumens - WXGA (1280 x 800) - 16:10 - HD 720p', '899,36'),
(121, '4095913', 'EPSON', 'Epson EB-2055 - Projecteur 3LCD - 5000 lumens (blanc) - 5000 lumens (couleur) - XGA (1024 x 768) - 4:3 - 802.11 b/g/n sans fil/ LAN', '909,38'),
(122, '3601412', 'BENQ', 'BenQ SX920 - Projecteur DLP - 3D - 5000 ANSI lumens - XGA (1024 x 768) - 4:3', '994,49'),
(123, '4386743', 'BENQ', 'BenQ MH760 - Projecteur DLP - 3D - 5000 ANSI lumens - Full HD (1920 x 1080) - 16:9 - HD 1080p', '995,61'),
(124, '3604772', 'BENQ', 'BenQ SU922 - Projecteur DLP - 3D - 5000 ANSI lumens - WUXGA (1920 x 1200) - 16:10 - HD 1080p', '1 240,08'),
(125, '2079124', 'BENQ', 'BenQ SP870 - Projecteur DLP - 5000 lumens - XGA (1024 x 768) - 4:3', '1 470,69'),
(126, '4095916', 'EPSON', 'Epson EB-2255U - Projecteur LCD - 5000 lumens (blanc) - 5000 lumens (couleur) - WUXGA (1920 x 1200) - 16:10 - HD 1080p - sans fil 802.11n/LAN/Miracast', '1 514,08'),
(127, '3331030', 'BENQ', 'BenQ SU917 - Projecteur DLP - 3D - 5000 ANSI lumens - WUXGA (1920 x 1200) - 16:10 - HD 1080p - LAN', '2 755,05'),
(128, '4495825', 'EPSON', 'Epson EB-L1000U - Projecteur LCD - 5000 lumens (blanc) - 5000 lumens (couleur) - WUXGA (1920 x 1200) - 16:10 - HD 1080p - LAN', '5 968,04');

-- --------------------------------------------------------

--
-- Structure de la table `commentaires`
--

CREATE TABLE IF NOT EXISTS `commentaires` (
  `commentaires` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `devis`
--

CREATE TABLE IF NOT EXISTS `devis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Reference` varchar(255) NOT NULL,
  `Marque` varchar(255) NOT NULL,
  `Designation` varchar(255) NOT NULL,
  `Prix` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) 
ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `intervention`
--

CREATE TABLE IF NOT EXISTS `intervention` (
  `temps_intervention_restant` int(11) NOT NULL,
  `temps_intervention_derniere` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `membres`
--

CREATE TABLE IF NOT EXISTS `membres` (
  `attribut` tinyint(2) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `mdp` text NOT NULL,
  `nom_societe` text NOT NULL,
  `fonction` text NOT NULL,
  `nom` text NOT NULL,
  `prenom` text NOT NULL,
  `adresse` text NOT NULL,
  `numero_tel` text NOT NULL,
  `marge` float(11,2) NOT NULL,
  `remise1` float(11,2) NOT NULL,
  `remise2` float(11,2) NOT NULL,
  `remise3` float(11,2) NOT NULL,
  `forgetpass` varchar(255) NOT NULL,
  PRIMARY KEY (`pseudo`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Contenu de la table `membres`
--

INSERT INTO `membres` (`attribut`, `id`, `pseudo`, `mail`, `mdp`, `nom_societe`, `fonction`, `nom`, `prenom`, `adresse`, `numero_tel`, `marge`, `remise1`, `remise2`, `remise3`, `forgetpass`) VALUES
(1, 20, 'admin', 'admin@admin.admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'physic', 'admin', 'admin', 'admin', 'admin', 'admin', 100.00, 74.00, 0.00, 0.00, ''),
(0, 21, 'azerty', 'azerty@azerty.azerty', '9cf95dacd226dcf43da376cdb6cbba7035218921', 'azerty', 'azerty', 'azerty', 'azerty', 'azerty', 'azerty', 0.00, 0.00, 0.00, 0.00, ''),
(0, 25, 'mairiearras', 'philippe.vilbert@physic.fr', 'b6a97a4af36e7aa7c8552e65bb393d8f975f0bd4', 'Mairie Arras', 'responsable info', 'Jerome', 'Mailleu', '1 place de ville', '0322670670', 0.00, 0.00, 0.00, 0.00, ''),
(0, 24, 'test1', 'test@test.test', 'b444ac06613fc8d63795be9ad0beaf55011936ac', 'test', 'test', 'test', 'test', 'test', '04875487503', 0.00, 0.00, 0.00, 0.00, '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
