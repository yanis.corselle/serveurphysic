﻿<?php

	try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }

	?>

<html lang="fr">

<head>

	<meta charset="utf-8"/>

	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>

	<link rel="icon" type="image/png" href="favicon.png" />

	<link type="text/css" rel="stylesheet" href="../css/style.css" />

	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>

	<link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css" />

</head>

	<body>

	<!--Entete-->

		<?php include("../include/page_up.php"); ?>



	<!--Zone produits-->

		<div class="zone3">


			<img src="../img/tbij.jpg" width="270px" style="margin-top: 200px; margin-bottom: 20px; border-radius: 5px;"/>

		</div>



	<!--TBI-->

		<div class="zone4">

			<center>

				<img src="../img/tbi.png"/>

				<img src="../img/tbi3m.png"/>

				<li>Réduction de l'ombre portée</li>

				<li>Installation sur le même mur que le tableau (faible encombrement)</li>

				<br/>

				<br/>

				<h3><u>Les atouts</u></h3>

					<li><u>L'intéractivité:</u></li>

					L'intérêt du TBI est de favoriser l'intéractivité entre le professeur et ses élèves ( 2 stylets fournis ).

					<li><u>Facilite le travail collectif</u></li>

					Le TBI  offre une large surface de projection visible par tous, et sur laquelle chacun peut intervenir.

				<li><u>Gain de temps</u></li>

					Grâce au format numérique, il est plus facile de réutiliser les supports de cours.

				<br/>

				<br/>

				<video width="300" src="../video/tbi.ogv" controls="" autoplay="true" poster="../img/tab.jpg"/>

			</center>

			<br/>

		</div>



<!--Zone du footer-->

	<?php include("../include/footer.php"); ?>

</body>

</html>

