<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8"/>
	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>
	<link rel="icon" type="image/png" href="favicon.png" />
	<link type="text/css" rel="stylesheet" href="../css/style.css" />
	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css" />
</head>
	<body>
	<!--Entete-->
		<?php include("../include/page_up.php"); ?>

	<!--Image nouveauxproduits2-->
	<img href="../img/nouveauxproduits2.png">

<!--Inscriptions-->

<?php

	try
    {
		$pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
		$bdd = new PDO('mysql:host=localhost; dbname=physic;charset=UTF8', 'root', 'root', $pdo_options );
		
    }
catch (Exception $e){
            die('Erreur : ' . $e->getMessage());
    }

	if(isset($_POST['forminscription']))
	{
					$nom_societe = $_POST['nom_societe'];
					$nom = $_POST['nom'];
					$prenom = $_POST['prenom'];
					$fonction = $_POST['fonction'];
					$adresse = $_POST['adresse'];
					$mail = $_POST['mail'];
					$numero_tel = $_POST['numero_tel'];
					$pseudo = $_POST['pseudo'];
					$mdp = sha1($_POST['mdp']);
					$mdp2 = sha1($_POST['mdp2']);
					$marge = $_POST['marge'];
					$remise = $_POST['remise1'];
					$remise2 = $_POST['remise2'];
					$remise3 = $_POST['remise3'];
   if(!empty($_POST['nom_societe']) AND !empty($_POST['nom']) AND !empty($_POST['prenom']) AND !empty($_POST['fonction']) AND !empty($_POST['adresse']) AND !empty($_POST['mail']) AND !empty($_POST['numero_tel']) AND !empty($_POST['pseudo']) AND !empty($_POST['mdp']) AND !empty($_POST['mdp2']) AND !empty($_POST['marge']) AND !empty($_POST['remise1']) AND !empty($_POST['remise2']) AND !empty($_POST['remise3']))
	{
      $pseudolength = strlen($pseudo);
      if($pseudolength <= 255)
		{
            if(filter_var($mail, FILTER_VALIDATE_EMAIL))
			{
               $reqmail = $bdd->prepare("SELECT * FROM `membres` WHERE mail = ".$mail."");
               $reqmail->execute(array($mail));
               $mailexist = $reqmail->rowCount();
               if($mailexist == 0)
			   {
                  if($mdp == $mdp2)
				  {
					$bdd->query("SET NAMES UTF8");
                    $req = $bdd->prepare('INSERT INTO membres(nom_societe, nom, prenom, fonction, adresse, numero_tel, pseudo, mdp, mail, marge, remise1, remise2, remise3) VALUES(:nom_societe, :nom, :prenom, :fonction, :adresse, :numero_tel, :pseudo, :mdp, :mail, :marge, :remise1, :remise2, :remise3)');
					$req->execute(array(
						'nom_societe' => $nom_societe,
						'nom' => $nom,
						'prenom' => $prenom,
						'fonction' => $fonction,
						'adresse' => $adresse,
						'duree_amort' => $duree_amort,
						'libelle_sousclasse' => $libelle_sousclasse,
						'num_sousclasse' => $num_sousclasse,
						'libelle_compte' => $libelle_compte,
						'num_compte' => $num_compte,
						'libelle_ecriture' => $ecriture,
						'quantite' => $quantite,
						'debit' => $debit,
						'credit' => $credit,
						'solde' => $credit-$debit,
						));                    $erreur = '<font color="green">Votre compte a bien été créé ! <a href="../modules/seconnecter.php">Me connecter</a></font>';
                  } else {
                    $erreur = "Vos mots de passes ne correspondent pas !";
                  }
               } else {
                  $erreur = "L'adresse mail saisie est déjà utilisée !";
               }
            } else {
               $erreur = "Votre adresse mail n'est pas valide !";
            }
         }
      } else {
         $erreur = "Votre pseudo ne doit pas dépasser 255 caractères !";
      }
   } else {
      $erreur = "Tous les champs doivent être complétés !";
   }
?>

	<!--Zone du footer-->
	<?php include("../include/footer.php"); ?>
</body>
</html>
