<?php
session_start();
if(isset($_SESSION['attribut']) == FALSE OR $_SESSION['attribut'] == 0)
{
	header("Location:../../profil.php");
}
			try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }


		$requser = $bdd->prepare("SELECT * FROM intervention WHERE id=?");
		$requser->execute(array($_POST['id']));
		$presta = $requser->fetch();
		$id=$_POST['edit'];

		if(isset($_POST['new_nom_societe']) AND $_POST['new_nom_societe'] != $presta['nom_societe'])
		{
			$new_nom_societe = htmlspecialchars($_POST['new_nom_societe']);
			$insert_nom_societe = $bdd->prepare("UPDATE intervention SET nom_societe = ? WHERE id =?");
			$insert_nom_societe->execute(array($new_nom_societe, $_POST['edit']));
        }
        
        if(isset($_POST['new_nom_contact']) AND $_POST['new_nom_contact'] != $presta['nom_contact'])
		{
			$new_nom_contact = htmlspecialchars($_POST['new_nom_contact']);
			$insert_nom_contact = $bdd->prepare("UPDATE intervention SET nom_contact = ? WHERE id =?");
			$insert_nom_contact->execute(array($new_nom_contact, $_POST['edit']));
        }
        
        if(isset($_POST['new_adresse']) AND $_POST['new_adresse'] != $presta['adresse'])
		{
			$new_adresse = htmlspecialchars($_POST['new_adresse']);
			$insert_adresse = $bdd->prepare("UPDATE intervention SET adresse = ? WHERE id =?");
			$insert_adresse->execute(array($new_adresse, $_POST['edit']));
        }
        
        if(isset($_POST['new_tel']) AND $_POST['new_tel'] != $presta['tel'])
		{
			$new_tel = htmlspecialchars($_POST['new_tel']);
			$insert_tel = $bdd->prepare("UPDATE intervention SET tel = ? WHERE id =?");
			$insert_tel->execute(array($new_tel, $_POST['edit']));
        }
        
        if(isset($_POST['new_fax']) AND $_POST['new_fax'] != $presta['fax'])
		{
			$new_fax = htmlspecialchars($_POST['new_fax']);
			$insert_fax = $bdd->prepare("UPDATE intervention SET fax = ? WHERE id =?");
			$insert_fax->execute(array($new_fax, $_POST['edit']));
        }
        
        if(isset($_POST['new_portable']) AND $_POST['new_portable'] != $presta['portable'])
		{
			$new_portable = htmlspecialchars($_POST['new_portable']);
			$insert_new_portable = $bdd->prepare("UPDATE intervention SET portable = ? WHERE id =?");
			$insert_portable->execute(array($new_portable, $_POST['edit']));
        }
        
        if(isset($_POST['new_date_intervention']) AND $_POST['new_date_intervention'] != $presta['date_intervention'])
		{
			$new_date_intervention = htmlspecialchars($_POST['new_date_intervention']);
			$insert_date_intervention = $bdd->prepare("UPDATE intervention SET date_intervention = ? WHERE id =?");
			$insert_date_intervention->execute(array($new_date_intervention, $_POST['edit']));
        }
        
        if(isset($_POST['new_debut_plage_horaire']) AND $_POST['new_debut_plage_horaire'] != $presta['debut_plage_horaire'])
		{
			$new_debut_plage_horaire = htmlspecialchars($_POST['new_debut_plage_horaire']);
			$insert_debut_plage_horaire = $bdd->prepare("UPDATE intervention SET debut_plage_horaire = ? WHERE id =?");
			$insert_debut_plage_horaire->execute(array($debut_plage_horaire, $_POST['edit']));
        }
        
        if(isset($_POST['new_fin_plage_horaire']) AND $_POST['new_fin_plage_horaire'] != $presta['fin_plage_horaire'])
		{
			$new_fin_plage_horaire = htmlspecialchars($_POST['new_fin_plage_horaire']);
			$insert_fin_plage_horaire = $bdd->prepare("UPDATE intervention SET fin_plage_horaire = ? WHERE id =?");
			$insert_fin_plage_horaire->execute(array($fin_plage_horaire, $_POST['edit']));
        }
        
        if(isset($_POST['new_garantie']) AND $_POST['new_garantie'] != $presta['garantie'])
		{
			$new_garantie = htmlspecialchars($_POST['new_garantie']);
			$insert_garantie = $bdd->prepare("UPDATE intervention SET garantie = ? WHERE id =?");
			$insert_garantie->execute(array($new_garantie, $_POST['edit']));
        }

        if(isset($_POST['new_date_achat']) AND $_POST['new_date_achat'] != $presta['date_achat'])
		{
			$new_date_achat = htmlspecialchars($_POST['date_achat']);
			$insert_date_achat = $bdd->prepare("UPDATE intervention SET date_achat = ? WHERE id =?");
			$insert_date_achat->execute(array($new_date_achat, $_POST['edit']));
        }

        if(isset($_POST['new_urgence']) AND $_POST['new_urgence'] != $presta['urgence'])
		{
			$new_urgence = htmlspecialchars($_POST['new_urgence']);
			$insert_urgence = $bdd->prepare("UPDATE intervention SET urgence = ? WHERE id =?");
			$insert_urgence->execute(array($new_urgence, $_POST['edit']));
        }

        if(isset($_POST['new_intervention_sur_site']) AND $_POST['new_intervention_sur_site'] != $presta['tel'])
		{
			$new_intervention_sur_site = htmlspecialchars($_POST['new_intervention_sur_site']);
			$insert_intervention_sur_site = $bdd->prepare("UPDATE intervention SET intervention_sur_site = ? WHERE id =?");
			$insert_intervention_sur_site->execute(array($new_intervention_sur_site, $_POST['edit']));
        }

        if(isset($_POST['new_devis']) AND $_POST['new_devis'] != $presta['devis'])
		{
			$new_devis = htmlspecialchars($_POST['new_devis']);
			$insert_devis = $bdd->prepare("UPDATE intervention SET devis = ? WHERE id =?");
			$insert_devis->execute(array($new_devis, $_POST['edit']));
        }

        if(isset($_POST['new_marque']) AND $_POST['new_marque'] != $presta['marque'])
		{
			$new_marque = htmlspecialchars($_POST['new_marque']);
			$insert_marque = $bdd->prepare("UPDATE intervention SET marque = ? WHERE id =?");
			$insert_marque->execute(array($new_marque, $_POST['edit']));
        }

        if(isset($_POST['new_type']) AND $_POST['new_type'] != $presta['type'])
		{
			$new_type = htmlspecialchars($_POST['new_type']);
			$insert_type = $bdd->prepare("UPDATE intervention SET type = ? WHERE id =?");
			$insert_type->execute(array($new_type, $_POST['edit']));
        }

        if(isset($_POST['new_numserie']) AND $_POST['new_numserie'] != $presta['numserie'])
		{
			$new_numserie = htmlspecialchars($_POST['new_numserie']);
			$insert_numserie = $bdd->prepare("UPDATE intervention SET numserie = ? WHERE id =?");
			$insert_numserie->execute(array($new_numserie, $_POST['edit']));
        }

        if(isset($_POST['new_descriptif']) AND $_POST['new_descriptif'] != $presta['descriptif'])
		{
			$new_descriptif = htmlspecialchars($_POST['new_descriptif']);
			$insert_descriptif = $bdd->prepare("UPDATE intervention SET descriptif = ? WHERE id =?");
			$insert_descriptif->execute(array($new_descriptif, $_POST['edit']));
        }

        

        

		
		


	if(isset($_POST['modif']))
	{
		header('Location: gestion_presta.php');
	}
	


			
		

?>
<html lang="fr">
<head>
	<meta charset="utf-8"/>
	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>
	<link rel="icon" type="image/png" href="favicon.png" />
	<link type="text/css" rel="stylesheet" href="../../../css/style.css" />
	<link type="image/jpg" rel="icon" href="../../../img/favicon.jpg"/>
	<link rel="stylesheet" href="../../../css/jquery.mCustomScrollbar.css" />
</head>
	<body>
	<!--Entete-->
		<?php include("../../../include/page_upacc.php"); ?>

	<!--formulaire d'édition du profil-->

		<div class="zone4">
		<center>
         <h2>Edition du profil</h2>
         <form method="POST" action="" class="formedition">

            <input type="text" name="new_nom_societe" placeholder="Nom de la société" class="inputbasic" value="<?php echo $presta['nom_societe']; ?>"/></br></br>
            <input type="text" name="new_nom_contact" placeholder="Nom du contact" class="inputbasic" value="<?php echo $presta['nom_contact']; ?>"/></br></br>
            <input type="text" name="new_adresse" placeholder="Adresse" class="inputbasic" value="<?php echo $presta['adresse']; ?>"/></br></br>
            <input type="tel" name="new_tel" placeholder="Numéro de téléphone" class="inputbasic" value="<?php echo $presta['tel']; ?>"/></br></br>
            <input type="tel" name="new_fax" placeholder="Numéro de fax:" class="inputbasic" value="<?php echo $presta['fax']; ?>"/></br></br>
            <input type="tel" name="new_portable" placeholder="Numéro de portable" class="inputbasic" value="<?php echo $presta['portable']; ?>"/></br></br>
            <input type="date" name="new_date_intervention" placeholder="Date de l'intervention" class="inputbasic" value="<?php echo $presta['date_intervention']; ?>"/></br></br>
            <input type="time" name="new_debut_plage_horaire" placeholder="Début de la plage horaire" class="inputbasic" value="<?php echo $presta['debut_plage_horaire']; ?>"/></br></br>
            <input type="time" name="new_fin_plage_horaire" placeholder="Fin de la plage horaire" class="inputbasic" value="<?php echo $presta['fin_plage_horaire']; ?>"/></br></br>
            <input type="checkbox" name="new_garantie" placeholder="Nom de la société" class="inputbasic" value="<?php echo $presta['garantie']; ?>"/> <label for="new_garantie">Garantie</label></br></br>
            <input type="date" name="date_achat" placeholder="Date d'achat" class="inputbasic" value="<?php echo $presta['date_achat']; ?>"/></br></br>
            <input type="radio" id="4" name="new_urgence" class="inputbasic" value="<?php echo $presta['urgence']; ?>"/> <label for="4"> Sous 4 heures </label></br></br>            
            <input type="radio" id="8" name="new_urgence" class="inputbasic" value="<?php echo $presta['urgence']; ?>"/> <label for="8"> Sous 8 heures </label></br></br>             
            <input type="radio" id="indeterminee" name="new_urgence" class="inputbasic" value="<?php echo $presta['urgence']; ?>"/> <label for="indeterminee"> Indéterminée </label></br></br>             
            <input type="radio" id="constructeur" name="new_urgence" class="inputbasic" value="<?php echo $presta['urgence']; ?>"/> <label for="constructeur"> En fonction du constructeur </label></br></br>
            <input type="checkbox" name="new_intervention_sur_site" placeholder="Intervention sur site" class="inputbasic" value="<?php echo $presta['nom_societe']; ?>"/> <label for="new_intervention_sur_site"> Intervention sur site</br></br>
            <input type="checkbox" name="new_devis" placeholder="Devis" class="inputbasic" value="<?php echo $presta['devis']; ?>"/> <label for="new_devis"> Devis </label></br></br>
            <input type="text" name="new_marque" placeholder="Marque" class="inputbasic" value="<?php echo $presta['marque']; ?>"/></br></br>
            <input type="text" name="new_type" placeholder="Type" class="inputbasic" value="<?php echo $presta['type']; ?>"/></br></br>
            <input type="text" name="new_num_serie" placeholder="Numéro de série" class="inputbasic" value="<?php echo $presta['numserie']; ?>"/></br></br>
            <input type="text" name="new_descriptif" placeholder="Descriptif" class="inputbasic" value="<?php echo $presta['descriptif']; ?>"/></br></br>

			<input type="hidden" type='submit' name="modif" value="true">
			<input type="hidden" type='submit' name="edit" value=<?php echo($_POST['edit']) ?>>
			<input type="submit" value="Mettre à jour le profil" name="modifier">
		</form>
		<?php if(isset($msg)) { echo $msg; } ?>
		</center>
		</div>

	<!--Zone du footer-->
	<?php include("../../../include/footeracc.php"); ?>
</body>
<script type="text/javascript" src="../../../js/jquery.min.js"></script>


</html>