<?php
	session_start();
	if(isset($_SESSION['attribut']) == FALSE OR $_SESSION['attribut'] == 0)

			{

				header("Location:../../profil.php");

			}
			
			try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
			$sql = "SELECT * FROM categories_boutique";

			try{
			
			$stmt = $bdd->query($sql);
			 if($stmt === false){
			
			  die("Erreur");
			
			 }
			
			 
			
			}catch (PDOException $e){
			
			  echo $e->getMessage();
			
			}

			
	?>

<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="fr-FR">
<head>
	<meta http-equiv="Content-Type" content="text/html" charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>
	<link rel="icon" type="image/png" href="../../../img/favicon.png"/>
	<link type="text/css" rel="stylesheet" href="../../../css/style.css"/>
	<link type="image/jpg" rel="icon" href="../../../img/favicon.jpg"/>
</head>
	<body>
	<!--Entete-->
		<?php include("../../../include/page_upacc.php"); ?>

		<form action="upload_csv.php" method="post" enctype="multipart/form-data">
			<h2>Upload Fichier</h2>
			<label for="fileUpload">Fichier:</label>
			<input type="file" name="csv" id="fileUpload"> </br> </br>

			<label for="categorie-select">Sélectionner la catégorie: </label>
			<script type="text/javascript">
				function gensel2()
				{
				s2.length=0;
				for ( var n=0; n<liste[s1.selectedIndex].length; n++ )
				{
				s2.length++; 
				s2.options[s2.length-1].text=liste[s1.selectedIndex][n];
				}
				}
			</script> 

			<select name="Categorie" id="categorie-select" onchange="gensel2();" >
				<optgroup label="newCategorie" > 
					<option value="null">Veuillez choisir une catégorie</option>	
			
				<?php
				while($row = $stmt->fetch(PDO::FETCH_ASSOC))
				{
					$contenu .= "<option value='" . $row['categorie'] . "'>" . $row['categorie'] . "</option>";
				}
				echo $contenu;
				?>
				</optgroup>
			</select>
			</br> </br>

			<label for="newSousCategorie">Sélectionner la sous-catégorie: </label>
			<select name="Sous_Categorie" id="newSousCategorie">
				<optgroup label="Sous Catégorie">
				</optgroup>
			</select>

			<script type="text/javascript">
				var s1=document.getElementById("categorie-select");
				var s2=document.getElementById("newSousCategorie"); 
				var liste=new Array(
									new Array(),
									new Array("ordinateurs portables", "ordinateurs de bureau", "serveurs", "tablettes"),
									new Array("moniteurs", "vidéo-projecteurs", "imprimantes", "scanners", "onduleurs", "claviers _ souris", "stockage externe"),
									new Array("systèmes dexploitation", "logiciels de sauvegarde", "logiciels de sécurité _ antivirus", "applications bureautique"),
									new Array("RACKS et armoires", "réseaux wi-fi", "réseaux filaires", "accessoires réseaux"),
									new Array("cartes-mères _ processeurs", "cartes-vidéo _ cartes-son", "boîtiers _ alimentation", "stockage interne"),
									new Array()
									);
			</script>
			<input type="submit" name="submit" value="Upload">
			<p><strong>Note:</strong> Seuls les formats .csv sont autorisés jusqu'à une taille maximale de 5 Mo.</p>
    	</form>

	<!--Zone du footer-->
	<?php include("../../../include/footeracc.php"); ?>
</body>
</html>
