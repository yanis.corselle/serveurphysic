<?php

			session_start();

			if(isset($_SESSION['attribut']) == FALSE OR $_SESSION['attribut'] == 0)

			{

				header("Location:../../profil.php");

			}



			try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
			$sql = "SELECT * FROM categories_boutique";

			try{
			
			$stmt = $bdd->query($sql);
			 if($stmt === false){
			
			  die("Erreur");
			
			 }
			
			 
			
			}catch (PDOException $e){
			
			  echo $e->getMessage();
			
			}




		$requser = $bdd->prepare("SELECT * FROM catalogue WHERE id=?");

		$requser->execute(array($_GET['id']));

		$vente = $requser->fetch();

		$id=$_GET['id'];



		if(isset($_POST['newReference']) AND $_POST['newReference'] != $vente['Reference'])

		{

			$newReference = htmlspecialchars($_POST['newReference']);

			$insertReference = $bdd->prepare("UPDATE catalogue SET Reference = ? WHERE id =?");

			$insertpseudo->execute(array($newReference, $_GET['id']));

		}



		if(isset($_POST['newMarque']) AND $_POST['newMarque'] != $vente['Marque'])

		{

			$newMarque = htmlspecialchars($_POST['newMarque']);

			$insertMarque = $bdd->prepare("UPDATE catalogue SET Marque = ? WHERE id =?");

			$insertMarque->execute(array($newMarque, $id));

			

		}



		if(isset($_POST['newDesignation']) AND $_POST['newDesignation'] != $vente['Designation'])

		{

			$newDesignation = htmlspecialchars($_POST['newDesignation']);

			$insertDesignation = $bdd->prepare("UPDATE catalogue SET Designation = ? WHERE id =?");

			$insertDesignation->execute(array($newDesignation, $_GET['id']));

		}



		if(isset($_POST['newPrix_PU_HT']) ) 

		{

			$newPrix_PU_HT = htmlspecialchars($_POST['newPrix_PU_HT']);

			$insertPrix_PU_HT = $bdd->prepare("UPDATE catalogue SET Prix_PU_HT = ? WHERE id =?");

			$insertPrix_PU_HT->execute(array($newPrix_PU_HT, $_GET['id']));

		}

		if(isset($_POST['newCategorie']) ) 

		{

			$newCategorie = htmlspecialchars($_POST['newCategorie']);

			$insertCategorie = $bdd->prepare("UPDATE catalogue SET Categorie = ? WHERE id =?");

			$insertCategorie->execute(array($newCategorie, $_GET['id']));

		}

		if(isset($_POST['newSousCategorie']) ) 

		{

			$newSousCategorie = htmlspecialchars($_POST['newSousCategorie']);

			$insertSousCategorie = $bdd->prepare("UPDATE catalogue SET Sous_Categorie = ? WHERE id =?");

			$insertSousCategorie->execute(array($newSousCategorie, $_GET['id']));

		}


		if(isset($_POST['newImage']) ) 

		{
			if(isset($_FILES["newImage"]) && $_FILES["newImage"]["error"] == 0){
				
				$png = strpos($_FILES["newImage"]["name"], "png");
				$jpg = strpos($_FILES["newImage"]["name"], "jpg");
				$jpeg = strpos($_FILES["newImage"]["name"], "jpeg");

				if($png != FALSE)
				{
					$filename = $id .".png";
				}

				if($jpg != FALSE)
				{
					$filename = $id .".jpg";
				}

				if($jpeg != FALSE)
				{
					$filename = $id .".jpeg";
				}
				
				
				$filetype = $_FILES["newImage"]["type"];
				$filesize = $_FILES["newImage"]["size"];
		
				
		
				// Vérifie la taille du fichier - 5Mo maximum
				$maxsize = 5 * 1024 * 1024;
				if($filesize > $maxsize) die("Error: La taille du fichier est supérieure à la limite autorisée.");
		
					// Vérifie si le fichier existe avant de le télécharger.
					if(file_exists("../../upload/img_cata/" . $filename)){
						echo $_FILES["newImage"]["name"] . " existe déjà.";
					} else{
						move_uploaded_file($_FILES["newImage"]["tmp_name"], "../../upload/img_cata/" . $filename);
						echo "Votre fichier a été téléchargé avec succès.";

					} 
			} else{
				echo "Error: " . $_FILES["newImage"]["error"];
				print_r($_FILES);
				print_r($_POST);
				exit();
			}
		}





	if(isset($_POST['modif']))

	{

		header('Location: gestion_cat.php');

	}
	

	





			

		



?>

<html lang="fr">

<head>

	<meta charset="utf-8"/>

	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>

	<link rel="icon" type="image/png" href="favicon.png" />

	<link type="text/css" rel="stylesheet" href="../../../css/style.css" />

	<link type="image/jpg" rel="icon" href="../../../img/favicon.jpg"/>

	<link rel="stylesheet" href="../../../css/jquery.mCustomScrollbar.css" />

</head>

	<body>

	<!--Entete-->

		<?php include("../../../include/page_upacc.php"); ?>



	<!--formulaire d'édition du profil-->



		<div class="zone4">

		<center>

         <h2>Edition :</h2>

         <form method="POST" action="" class="formedition" enctype="multipart/form-data">

		 	<label for="newImage">Image:</label>
			<input type="file" name="newImage" id="newImage"> </br> </br>

		 	<label for="newReference_fournisseur"> Référence fournisseur: </label>
			<input type="text"  name="newReference_fournisseur"  id="newReference_fournisseur" class="inputbasic" value="<?php echo $vente['Reference_fournisseur']; ?>"/></br></br>

			<label for="newReference_fabricant"> Référence fabricant: </label>
			<input type="text"  name="newReference_fabricant"  id="newReference_fabricant" class="inputbasic" value="<?php echo $vente['Reference_fabricant']; ?>"/></br></br>

			<label for="newMarque"> Marque: </label>
			<input type="text" id="newMarque" name="newMarque" class="inputbasic" value="<?php echo $vente['Marque']; ?>"/></br></br>

			<label for="newDesignation"> Désignation: </label>
			<textarea rows="3" cols="50" name="newDesignation" id="newDesignation" class="inputbasic"> <?php echo $vente['Designation']; ?> </textarea></br></br>

			<label for="newUnite_mesure"> Unité de mesure du conditionnement: </label>
			<input type="text" name="newUnite_mesure" id="newUnite_mesure" class="inputbasic" value="<?php echo $vente['Unite_mesure']; ?>"/></br></br>

			<label for="newNombre_pieces"> Nombre de pièces contenues dans l'unité de mesure: </label>
			<input type="text" name="newNombre_pieces" id="newNombre_pieces" class="inputbasic" size="1" value="<?php echo $vente['Nombre_pieces']; ?>"/></br></br>

			<label for="newMinimum_commande"> Minimum de commande d'unité de conditionnement: </label>
			<input type="text" name="newMinimum_commande" id="newMinimum_commande" size="1" class="inputbasic" value="<?php echo $vente['Minimum_commande']; ?>"/></br></br>

			<label for="newPrix_PU_HT"> Prix(en €): </label> 
			<input type="text" size="3" name="newPrix" id="newPrix_PU_HT" class="inputbasic" value="<?php echo $vente['Prix_PU_HT']; ?>"/></br></br>

			<label for="categorie-select">Sélectionner la catégorie: </label>
			<script type="text/javascript">
				function gensel2()
				{
				s2.length=0;
				for ( var n=0; n<liste[s1.selectedIndex].length; n++ )
				{
				s2.length++; 
				s2.options[s2.length-1].text=liste[s1.selectedIndex][n];
				}
				}
			</script> 

			<select name="newCategorie" id="categorie-select" onchange="gensel2();" >
				<optgroup label="newCategorie" > 
					<option value="null">Veuillez choisir une catégorie</option>	
			
				<?php
				while($row = $stmt->fetch(PDO::FETCH_ASSOC))
				{
					if($row['categorie'] == $vente['Categorie'])
					{
						$contenu .= "<option selected='selected' value='" . $row['categorie'] . "'>" . $row['categorie'] . "</option>";
					}
					else{
						$contenu .= "<option value='" . $row['categorie'] . "'>" . $row['categorie'] . "</option>";
					}
					
				}
				echo $contenu;
				?>
				</optgroup>
			</select>
			</br> </br>

			<label for="newSousCategorie">Sélectionner la sous-catégorie: </label>
			<select name="newSousCategorie" id="newSousCategorie">
				<optgroup label="Sous Catégorie">
				</optgroup>
			</select>

			<script type="text/javascript">
				var s1=document.getElementById("categorie-select");
				var s2=document.getElementById("newSousCategorie"); 
				var liste=new Array(
									new Array(),
									new Array("ordinateurs portables", "ordinateurs de bureau", "serveurs", "tablettes"),
									new Array("moniteurs", "vidéo-projecteurs", "imprimantes", "scanners", "onduleurs", "claviers _ souris", "stockage externe"),
									new Array("systèmes d'exploitation", "logiciels de sauvegarde", "logiciels de sécurité _ antivirus", "applications bureautique"),
									new Array("RACKS et armoires", "réseaux wi-fi", "réseaux filaires", "accessoires réseaux"),
									new Array("cartes-mères _ processeurs", "cartes-vidéo _ cartes-son", "boîtiers _ alimentation", "stockage interne"),
									new Array()
									);
			</script>

			<input type="hidden" type='submit' name="modif" value="true">

			<input type="hidden" type='submit' name="id" value="<?php echo($_POST['id']) ?>">
								</br> </br>
			<input type="submit" value="Mettre à jour" name="modifier" class="inputbasic">

		</form>

		<?php if(isset($msg)) { echo $msg; } ?>

		</center>

		</div>



	<!--Zone du footer-->

	<?php include("../../../include/footeracc.php"); ?>

</body>

<script type="text/javascript" src="../../../js/jquery.min.js"></script>





</html>