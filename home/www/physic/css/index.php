<?php


$host = 'localhost';

$dbname = 'physic';

$username = 'admin';

$password = 'root';


$dsn = "mysql:host=$host;dbname=$dbname"; 

$sql = "SELECT * FROM catalogue WHERE Categorie ='destockage'";

try{

 $pdo = new PDO($dsn, $username, $password);

 $stmt = $pdo->query($sql);

 if($stmt === false){

  die("Erreur");

 }

 

}catch (PDOException $e){

  echo $e->getMessage();

}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8"/>
	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>
	<link rel="icon" type="image/png" href="favicon.png" />
	<link type="text/css" rel="stylesheet" href="css/style.css" />
	<link type="image/jpg" rel="icon" href="img/favicon.jpg"/>
</head>
	<body>
		<?php include("include/page_upindex.php"); ?>

	<!--Logo-->

	<!--Images défilantes-->
		<div class="zone2">
			<img href="">
		</div>

		<div class="zones_accueil">
		<!--Zone "Qui sommes-nous ?"-->
			<div class="zone3">
				<center> <h2> Qui sommes-nous ? </h2> </center>
					<center>
				<text id="quisommesnous">Créée en 1993, PhYsic est une société qui emploie aujourd'hui des techniciens hautement qualifiés afin de répondre à la demande de service et de matériel sans cesse croissante des entreprises et des utilisateurs dans le domaine informatique.
				La spécialisation de nos techniciens (possédant de nombreuses certifications constructeurs) nous permet de répondre à tous vos besoins : serveurs (Mac & PC), conception et maintenance de réseaux, stations de travail (Mac & PC), etc...
				N'hésitez pas à prendre <a href="modules/contact.php">contact</a> avec nous pour étudier vos besoins.
				</text>
					<br>
						<b>Tel : 03 22 670 670
							<br>
						Fax : 03 22 670 671</b>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2568.18054735346!2d2.28283931583683!3d49.932953432397596!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e786b9993d4c63%3A0x158f6c425b2e1678!2sPhysic!5e0!3m2!1sfr!2sfr!4v1526971856302" width="250" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
						</center>
			</div>

		<!--Zone "Nouveaux produits"-->
			<div class="zone4">
				<center> <h2> Nouveaux produits </h2><!--suppression espace vide
						--><img src="img/nouveauxproduits2.png" width="500px" height="460px" /></center>
			</div>

		<!--Zone "Derniers produits destockés"-->
			<div class="zone5">
				<h2> Derniers produits destockés </h2>
				<div class="container">
					<?php $emplacement_row=0;
					while($row = $stmt->fetch(PDO::FETCH_ASSOC))
					{
						if($emplacement_row==0)
						{
							$contenu .= "<div class=\"row\">";
						}
							$contenu .= "<div class=\"col-6\">";

							$titre = stristr($row['Designation'], ' - ', true);
							$id = htmlspecialchars($row['id']);
							if(file_exists("modules/upload/img_cata/" . $id . ".png"))
							{
								$image = "modules/upload/img_cata/" . $id . ".png";
							}  

							elseif(file_exists("modules/upload/img_cata/" . $id . ".jpg"))
							{
								$image = "modules/upload/img_cata/" . $id . ".jpg";
							}  

							elseif(file_exists("modules/upload/img_cata/" . $id . ".jpeg"))
							{
								$image = "modules/upload/img_cata/" . $id . ".jpeg";
							}  

							else{
								$image = "modules/upload/img_cata/null.png";
							}
							$contenu .= '<div class="boutique-produit">';
							$contenu .= "<div class=\"visible_boutique\"><h2>$titre</h2> </div>";
							$contenu .= "<div class=\"invisible_boutique\"><p>$row[Designation]</p> </div>";
							$contenu .= "<div class=\"photos_boutique\"><img src=$image width=\"130\" height=\"100\"> </div>";
							$contenu .= "<p>$row[Prix] €</p>";
							$contenu .= "<p>Référence du produit : $row[Reference] </p>";
							$contenu .= "<a href=\"modules/client/panier.php?action=ajout&amp;l=" . $row['Designation'] . "&amp;q=1&amp;p=" . $row['Prix'] . "&amp;r=" . $row['Reference'] ." \"onclick=\"window.open(this.href, '';\">Ajouter au panier</a>";
							$contenu .= "</div>";
							$contenu .= "</div>";

						if($emplacement_row==0)
						{
							$contenu .= "</div>";
						}

						$emplacement_row += 1;
					}
					?>
				</div>

			</div>
		</div>

	<!--Zone du footer-->
	<?php include("include/footerindex.php"); ?>
</body>
</html>
