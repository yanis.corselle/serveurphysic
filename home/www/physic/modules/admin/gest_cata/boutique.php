﻿<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set("display_startup_errors", 1);

$host = 'localhost';

$dbname = 'physic';

$username = 'admin';

$password = 'root';


$dsn = "mysql:host=$host;dbname=$dbname"; 

$page = (!empty($_GET['page']) ? $_GET['page'] : 1);
$limite = 24;

$debut = ($page - 1) * $limite;

if(!empty($_GET['categorie'])) 
{
	$categorie = $_GET['categorie'];
}

if(!empty($_GET['sous_categorie'])) 
{
	$sous_categorie = $_GET['sous_categorie'];
}



if(isset($_POST['tri']))
{
	if($_POST['tri'] == 'prix_croissant') 
	{
		$sql = "SELECT * FROM catalogue ASC ORDER BY Prix_PU_HT";
	}
	elseif($_POST['tri'] == 'prix_decroissant') 
	{
		$sql = "SELECT * FROM catalogue DESC ORDER BY Prix_PU_HT";
	}
	elseif($_POST['tri'] == 're ference') 
	{
		$sql = "SELECT * FROM catalogue ORDER BY Reference_fournisseur";
	}
	elseif($_POST['tri'] == 'titre') 
	{
		$sql = "SELECT * FROM catalogue ORDER BY Designation";
	}
	elseif($_POST['tri'] == 'marque') 
	{
		$sql = "SELECT * FROM catalogue ORDER BY Marque";
	}

	else{
		$sql = "SELECT * FROM catalogue LIMIT :debut, :limite ";
	}
}
else
{
	$sql = "SELECT * FROM catalogue";
}

if(!empty($_GET['categorie']) OR !empty($_GET['sous_categorie'] )) 
{
	if(!empty($_GET['categorie']) AND empty($_GET['sous_categorie']))
	{
		$sql .= " WHERE Categorie = '$categorie'";
	}

	else if(!empty($_GET['sous_categorie']) AND empty($_GET['categorie']))
	{
		$sql .= " WHERE Sous_Categorie = '$sous_categorie'";
	}

	else{
		$sql .= " WHERE Categorie = '$categorie' AND Sous_Categorie = '$sous_categorie'";
	}
}

$sql .=  " LIMIT :debut, :limite";

$sql2 = "SELECT * FROM categories_boutique";




try{

 $pdo = new PDO($dsn, $username, $password);
 $stmt = $pdo->prepare($sql);
 $stmt->bindParam(':debut', $debut, PDO::PARAM_INT);
 $stmt->bindValue(':limite', $limite, PDO::PARAM_INT);
 $stmt->execute();
 $stmt2 = $pdo->query($sql2);
 if($stmt === false || $stmt2 == false){

  die("Erreur");

 }

}catch (PDOException $e){

  echo $e->getMessage();

}



function tronquer_texte($texte, $limite){
	$tab = explode(' ', $texte, ($limite+1));
	if(count($tab) > $limite){ array_pop($tab); }
	return implode(' ', $tab);
}

/*?>
if( ($_SESSION['attribut']) ==0 OR ($_SESSION['attribut']) ==1)
{ 

	<script type="text/javascript">
		var msg="Si vous avez droit à des réductions dans notre boutique, veuillez vous connecter pour qu'elles s'appliquent";
		alert(msg);
	</script>
<?php
}*/

?>

<html lang="fr">

<head>

	<meta charset="utf-8"/>

	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>

	<link rel="icon" type="image/png" href="../img/favicon.png" />

	<link type="text/css" rel="stylesheet" href="../css/style.css" />
	<link type="text/css" rel="stylesheet" href="../css/simple-grid.css" />

	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>



</head>

	<body>

	<!--Entete-->

		<?php include("../include/page_up.php"); ?>



	<!--Image logo-->

<?php
$contenu = "<center> <h1> Nos produits : </h1> </center>";
	//--------------------------------- TRAITEMENTS PHP ---------------------------------//
//--- AFFICHAGE DES CATEGORIES ---//

$contenu .= '<div class="boutique-gauche">';
$contenu .= "<ul id='menu-boutique'>";
while($row = $stmt2->fetch(PDO::FETCH_ASSOC))
		{
			$contenu .= '<li>';
			if(file_exists("../img/icones_boutique/" . $row['categorie'] . ".png"))
			{
				$image = "../img/icones_boutique/" . $row['categorie'] . ".png";
			}  

			elseif(file_exists("../img/icones_boutique/" . $row['categorie'] . ".jpg"))
			{
				$image = "../img/icones_boutique/" . $row['categorie'] . ".jpg";
			}  

			elseif(file_exists("../img/icones_boutique/" . $row['categorie'] . ".jpeg"))
			{
				$image = "../img/icones_boutique/" . $row['categorie'] . ".jpeg";
			}  

			else{
				$image = "null.png";
			}

			if(empty($row['sous_categorie1']) == FALSE)
			{
				$contenu .= "<div class=\"categories\"> <a href='?categorie=" . $row['categorie'] . "'> <div class=\"img_cat_boutique\"> <img src=$image width=\"80\" height=\"60\"> </div> <div class=\"texte_cat_boutique\">" . str_replace('_', ' ', $row['categorie']) . " </div></a> </div>";
			}
			else{
				$contenu .= "<div class=\"categories\"> <div class=\"categorie_vide\"><a href='?categorie=" . $row['categorie'] . "'> <div class=\"img_cat_boutique\"> <img src=$image width=\"80\" height=\"60\"> </div> <div class=\"texte_cat_boutique\">" . str_replace('_', ' ', $row['categorie']) . " </div></a> </div></div>";
			}
			$contenu .= "<ul>";
			if(empty($row['sous_categorie1']) == FALSE)
			{
				$contenu .= "<li><a href='?sous_categorie=" . $row['sous_categorie1'] . "'> - " . str_replace('_', ' ', $row['sous_categorie1']) . "</a> </li>" ;
			} 

			if(empty($row['sous_categorie2']) == FALSE)
			{
				$contenu .= "<li><a href='?sous_categorie=" . $row['sous_categorie2'] . "'> - " . str_replace('_', ' ', $row['sous_categorie2']) . "</a> </li>" ;
			} 

			if(empty($row['sous_categorie3']) == FALSE)
			{
				$contenu .= "<li><a href='?sous_categorie=" . $row['sous_categorie3'] . "'> - " . str_replace('_', ' ', $row['sous_categorie3']) . "</a> </li>" ;
			} 
			
			if(empty($row['sous_categorie4']) == FALSE)
			{
				$contenu .= "<li><a href='?sous_categorie=" . $row['sous_categorie4'] . "'> - " . str_replace('_', ' ', $row['sous_categorie4']) . "</a> </li>" ;
			} 

			if(empty($row['sous_categorie5']) == FALSE)
			{
				$contenu .= "<li><a href='?sous_categorie=" . $row['sous_categorie5'] . "'> - " . str_replace('_', ' ', $row['sous_categorie5']) . "</a> </li>" ;
			} 

			if(empty($row['sous_categorie6']) ==FALSE)
			{
				$contenu .= "<li><a href='?sous_categorie=" . $row['sous_categorie6'] . "'> - " . str_replace('_', ' ', $row['sous_categorie6']) . "</a> </li>" ;
			} 

			if(empty($row['sous_categorie7']) == FALSE)
			{
				$contenu .= "<li><a href='?sous_categorie=" . $row['sous_categorie7'] . "'> - " . str_replace('_', ' ', $row['sous_categorie7']) . "</a> </li>" ;
			} 


			$contenu .= "</ul>";
			
			$contenu .= "</li>";
		}
$contenu .= "</ul>";
$contenu .= "</div>";

//--- AFFICHAGE DES PRODUITS ---//
$contenu .= '<div class="boutique-droite">';	

?>
<div class="zones_boutique">
	<div class="gauche">
		<form action = "" method = "POST" >
		<input type = "text" name = "terme">
		<input type = "submit" name = "recherche" value = "Rechercher">
		</form>
	</div>
	
	<div class="droite">
		<form method="POST" action="" id="zone5">
			<label for="tri-select">Trier par:</label>

			<select name="tri" id="tri-select">
				<option value="">Affichage par défaut</option>
				<option value="prix_croissant">Prix croissant</option>
				<option value="prix_decroissant">Prix décroissant</option>
				<option value="reference">Référence</option>
				<option value="titre">Nom</option>
				<option value="marque">Marque</option>
			</select>
			<input type="submit" value="Trier">

		</form>
	</div>
</div>

<?php
if(isset($_GET['terme']))
{
	$_POST['terme'] = $_GET['terme'];
}
if(isset($_POST['terme']))
{
	if(empty($_POST['terme']) == false)
	{
		$_POST['terme'] = htmlspecialchars($_POST['terme']);
		
		$contenu .= '<div class="container">';
		$emplacement_row=0;
		while($row = $stmt->fetch(PDO::FETCH_ASSOC))
		{
				
			if ((stristr($row['Designation'], $_POST['terme']) != FALSE) || (stristr($row['Reference_fournisseur'], $_POST['terme']) != FALSE))
			{
				if($emplacement_row == 4)
				{
					$emplacement_row = 0;
				}

				if($emplacement_row == 0)
				{
					$contenu .= '<div class="row">';
				}

				$contenu .= '<div class="col-3">';
				$titre = tronquer_texte($row['Designation'], 3);
				if(stristr($titre, 'K/'))
				{
					$titre = str_replace('K/', ' ', $titre);
				}
				elseif(stristr($titre, 'CS/'))
				{
					$titre = str_replace('CS/', ' ', $titre);
				}
				
				$id = htmlspecialchars($row['id']);
				if(file_exists("upload/img_cata/" . $id . ".png"))
				{
					$image = "upload/img_cata/" . $id . ".png";
				}  

				elseif(file_exists("upload/img_cata/" . $id . ".jpg"))
				{
					$image = "upload/img_cata/" . $id . ".jpg";
				}  

				elseif(file_exists("upload/img_cata/" . $id . ".jpeg"))
				{
					$image = "upload/img_cata/" . $id . ".jpeg";
				}  

				else
				{
					if(file_exists("../img/icones_boutique/" . $row['Categorie'] . ".png"))
					{
						$image = "../img/icones_boutique/" . $row['Categorie'] . ".png";
					}  

					elseif(file_exists("../img/icones_boutique/" . $row['Categorie'] . ".jpg"))
					{
						$image = "../img/icones_boutique/" . $row['Categorie'] . ".jpg";
					}  

					elseif(file_exists("../img/icones_boutique/" . $row['Categorie'] . ".jpeg"))
					{
						$image = "../img/icones_boutique/" . $row['Categorie'] . ".jpeg";
					}  

					else{
						$image = "null.png";
					}
				}
				$contenu .= '<div class="boutique-produit">';
				$contenu .= "<div class=\"visible_boutique\"><h3>$titre</h3> </div>";
				$contenu .= "<div class=\"invisible_boutique\"><p>$row[Designation]</p> </div>";
				$contenu .= "<div class=\"photos_boutique\"><img src=$image width=\"130\" height=\"100\"> </div>";
				$contenu .= "<p>$row[Prix_PU_HT] €</p>";
				$contenu .= "<p>Référence du produit : $row[Reference_fournisseur] </p>";
				$contenu .= "<a href=\"client/panier.php?action=ajout&amp;q=1&amp;id=" . $row['id'] ." \"onclick=\"window.open(this.href, '';\">Ajouter au panier</a>";
				$contenu .= "<br> <a href=\"fiche_produit.php?id=" . $row['id'] . "\">Voir la fiche produit</a>";
				$contenu .= '</div>';
				$contenu .= '</div>';
				if($emplacement_row == 3)
				{
					$contenu .= '</div>';
				}
				$emplacement_row += 1;
				
				
			}
			
			
		}
		$contenu .= '</div>';
		
	}
	
}
else
{
	$emplacement_row = 0;
	$contenu .= '<div class="container">';
	while($row = $stmt->fetch(PDO::FETCH_ASSOC))
	{
		
		if($emplacement_row == 4)
		{
			$emplacement_row = 0;
		}

		if($emplacement_row == 0)
		{
			$contenu .= '<div class="row">';
		}

		$contenu .= '<div class="col-3">';
		$titre = tronquer_texte($row['Designation'], 3);
		if(stristr($titre, 'K/'))
		{
			$titre = str_replace('K/', ' ', $titre);
		}
		elseif(stristr($titre, 'CS/'))
		{
			$titre = str_replace('CS/', ' ', $titre);
		}
					
		$id = htmlspecialchars($row['id']);
		if(file_exists("upload/img_cata/" . $id . ".png"))
		{
			$image = "upload/img_cata/" . $id . ".png";
		}  

		elseif(file_exists("upload/img_cata/" . $id . ".jpg"))
		{
			$image = "upload/img_cata/" . $id . ".jpg";
		}  

		elseif(file_exists("upload/img_cata/" . $id . ".jpeg"))
		{
			$image = "upload/img_cata/" . $id . ".jpeg";
		}  

		else
		{
			if(file_exists("../img/icones_boutique/" . $row['Categorie'] . ".png"))
			{
				$image = "../img/icones_boutique/" . $row['Categorie'] . ".png";
			}  

			elseif(file_exists("../img/icones_boutique/" . $row['Categorie'] . ".jpg"))
			{
				$image = "../img/icones_boutique/" . $row['Categorie'] . ".jpg";
			}  

			elseif(file_exists("../img/icones_boutique/" . $row['Categorie'] . ".jpeg"))
			{
				$image = "../img/icones_boutique/" . $row['Categorie'] . ".jpeg";
			}  

			else{
				$image = "null.png";
			}
		}
		$contenu .= '<div class="boutique-produit">';
		$contenu .= "<div class=\"visible_boutique\"><h3>$titre</h3> </div>";
		$contenu .= "<div class=\"invisible_boutique\"><p>$row[Designation]</p> </div>";
		$contenu .= "<div class=\"photos_boutique\"><img src=$image width=\"130\" height=\"100\"> </div>";
		$contenu .= "<p>$row[Prix_PU_HT] €</p>";
		$contenu .= "<p>Référence fournisseur : $row[Reference_fournisseur] </p>";
		$contenu .= "<a href=\"client/panier.php?action=ajout&amp;q=1&amp;id=" . $row['id'] ." \"onclick=\"window.open(this.href, '';\">Ajouter au panier</a>";
		$contenu .= "<br> <a href=\"fiche_produit.php?id=" . $row['id'] . "\">Voir la fiche produit</a>";
		$contenu .= '</div>';
		$contenu .= '</div>';
		
		if($emplacement_row == 3)
		{
			$contenu .= '</div>';
		}
		$emplacement_row += 1;
				
					
		
	}
		
	$contenu .= '</div>';
	$contenu .= '</div>';
}

//--------------------------------- AFFICHAGE HTML ---------------------------------//
$contenu .= '</div>';
?> 
<div class="boutique"> <?php echo $contenu; ?> 
	<center>
		<a href="?page=<?php echo $page - 1; ?>">Page précédente</a>
		<?php
		
		for ($i = $page - 5; $i <= $page + 5; $i++) 
		{
			if($i >= 1 AND $page != $i)
			{
				?> <a href="?page=<?php echo $i; ?>"> <?php echo $i ?> </a> <?php 
			}
			elseif($page == $i)
			{
				echo $i;
			}
		}?>

		<a href="?page=<?php echo $page + 1; ?>">Page suivante</a>
	</center>
</div>
		<!--Zone du footer-->

		<?php include("../include/footer.php"); ?>

		<!--<script type="text/javascript" src="../../js/Remise.js" charset="utf-8"></script>-->

</body>

</html>

