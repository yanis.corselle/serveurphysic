<?php
	try { $bdd = new PDO('mysql:host=127.0.0.1;dbname=physic;charset=utf8', 'admin', 'root'); } catch (Exception $e) { die('Erreur : ' . $e->getMessage()); }
	if(isset($_POST['article_titre'], $_POST['article_contenu'])){
	if(!empty($_POST['article_titre']) AND !empty($_POST['article_contenu'])){
		$article_titre = htmlspecialchars($_POST['article_titre']);
		$article_contenu = htmlspecialchars($_POST['article_contenu']);

		$ins = $bdd->prepare('INSERT INTO articles(titre, contenu, date_time_publication) VALUES (?, ?, NOW())');
		$ins->execute(array($article_titre, $article_contenu));

		$message = '<font color="green">Votre article a bien été posté !</font>';
	}else{
		$message = '<font color="red">Veuillez remplir tous les champs</font>';
	}
}
?>
<html lang="fr">
<head>
	<meta charset="utf-8"/>
	<title>PhYsic | Maintenance, Logiciels, Réseaux, Informatique</title>
	<link rel="icon" type="image/png" href="favicon.png" />
	<link type="text/css" rel="stylesheet" href="../css/style.css" />
	<link type="image/jpg" rel="icon" href="../img/favicon.jpg"/>
	<link rel="stylesheet" href="../css/jquery.mCustomScrollbar.css" />
</head>

	<body>
	<!--Entete-->
		<?php include("../include/page_up.php"); ?>

	<!--Zone "Formulaire article"-->
		<div class="zone4">
		<center>
		<form action="" method="POST" class="formarticles">
			<input type="text" name="article_titre" placeholder="Titre" class="inputbasic"/><br/>
			<textarea cols="33" rows="10" name="article_contenu" placeholder="Contenu de l'article"></textarea></br/>
			<input type="submit" value="Envoyer l'article"/><br/>
			<?php if(isset($message)) { echo $message; } ?>
		</form>
		</center>
		</div>

		<?php
			$bdd = new PDO("mysql:host=127.0.0.1;dbname=physic;charset=utf8", "root", "");
			if(isset($_POST['article_titre'], $_POST['article_contenu'])) {
			   if(!empty($_POST['article_titre']) AND !empty($_POST['article_contenu'])) {

				  $article_titre = htmlspecialchars($_POST['article_titre']);
				  $article_contenu = htmlspecialchars($_POST['article_contenu']);
				  $ins = $bdd->prepare('INSERT INTO articles (titre, contenu, date_time_publication) VALUES (?, ?, NOW())');
				  $ins->execute(array($article_titre, $article_contenu));
				  $message = 'Votre article a bien été posté';
			   } else {
				  $message = 'Veuillez remplir tous les champs';
			   }
			}
			?>
<!--Zone de footer-->
	<?php include("../include/footer.php"); ?>
</body>
</html>
